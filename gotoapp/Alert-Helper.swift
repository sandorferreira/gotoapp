//
//  FunctionHelpers.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 08/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit
import Foundation

class Alert: UIAlertController {
    static func showAlertWithMessage(_ title: String, withMessage message: String, actions: [UIAlertAction]?, atController controller: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if let arrayActions = actions {
            for action in arrayActions {
                alert.addAction(action)
            }
        } else {
            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(ok)
        }
        
        controller.present(alert, animated: true, completion: nil)
        //alert.view.tintColor = color
    }
}
