//
//  TTBar.swift
//  PrimeMarket
//
//  Created by Sandor ferreira da silva on 24/03/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit
import GoogleMaps


open class TTTabBar: UIViewController, CLLocationManagerDelegate{
    
    fileprivate var detailView: UIView! //View that will show controllers
    var activeTabBar: TTTabBarItem? //Active showing view Controller
    fileprivate var activeView: UIView? //Active showing view Controller
    fileprivate var tabBarView: UIView! //View of tabBar
    fileprivate var contentTabBarView : UIView! //Content, where background is render
    
    fileprivate var tabBarHidden = false
    
    //TabBar Items, which include VC
    var tabBarItems: [TTTabBarItem] = []
    
    fileprivate let defaultTabBarHeight: CGFloat = 54
    fileprivate var initialTabBarHeight: CGFloat = 0
    
    //TabBar Custom
    var tabBarHeight: CGFloat = 0 //Height of the TabBar, if a TTTabBarItem is bigger, will be over the tabBar
    var defaultTabBarItem: TTTabBarItem!
    var spaceBetweenTabs: CGFloat = 5
    var tabBackgroundColor = UIColor(white: 1.0, alpha: 0.0)
    
    var goButton = TTTabBarItem(image: UIImage(named:"goButtonBorder"), selected: UIImage(named:"goButtonBorder"))
    //var store: DataStore<Evento> = DataStore<Evento>.collection() //KCSCollection(from: "Events", of: Evento.self)
    var event: Evento?
    
    // --------- Variables --------- //
    
    var angleEvent: Double?
    var presentedVC: UIViewController?
    //var goToEvent = false
    var locationManager = CLLocationManager()
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        // -- Location -- //
        UIApplication.shared.isStatusBarHidden = false
        self.setNeedsStatusBarAppearanceUpdate()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        // -- Notifications -- //
        NotificationCenter.default.addObserver(self, selector: #selector(TTTabBar.changeMainButtonToEdit), name: NSNotification.Name(rawValue: "GoButtonEdit"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TTTabBar.rotateAndChangeFunction(_:)), name: NSNotification.Name(rawValue: "IrAoEvento"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TTTabBar.atualizarCompass), name: NSNotification.Name(rawValue: "AtualizarCompass"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TTTabBar.changeToInsideEvent(_:)), name: NSNotification.Name(rawValue: NotificationFuncaoDentroDoEvento), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TTTabBar.hideOrShow(_:)), name: NSNotification.Name(rawValue: EsconderBarra), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(TTTabBar.presentBehindBar(_:)), name: NSNotification.Name(rawValue: "PresentBehindBar"), object: nil)
        
        //Defaults
        if (tabBarHeight == 0) {
            tabBarHeight = defaultTabBarHeight
        }
        
        initialTabBarHeight = tabBarHeight
        
        
        //Create the detailView
        detailView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height-tabBarHeight))
        
        //Creat TabBar view
        tabBarView = UIView(frame: CGRect(x: 0, y: self.view.frame.height-tabBarHeight, width: self.view.frame.width, height: tabBarHeight))
        contentTabBarView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: tabBarHeight))
        
        //defaults
        tabBarView.backgroundColor = tabBackgroundColor //UIColor.clearColor()
        
        //Add subviews to mainView
        tabBarView.addSubview(contentTabBarView)
        self.view.addSubview(detailView)
        self.view.addSubview(tabBarView)
        
        // -- Adding Tab Bar Buttons
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "HomeScreen")
        let controllerPopular = self.storyboard?.instantiateViewController(withIdentifier: "Popular")
        let controllerDiscover = self.storyboard?.instantiateViewController(withIdentifier: "Discover")
        let controllerSettings = self.storyboard?.instantiateViewController(withIdentifier: "navigationPacotes")
        
        let item = TTTabBarItem(viewController: controller)
        item.image = UIImage(named: "bt_home")
        item.selectedImage = UIImage(named: "bt_home")
        
        let item2 = TTTabBarItem(viewController: controllerPopular!)
        item2.image = UIImage(named: "bt_popular")
        item2.selectedImage = UIImage(named: "bt_popular")
        
        
        goButton.isButton = true
        goButton.frame = CGRect(x: 0, y: 0, width: tabBarHeight + 35 , height: tabBarHeight + 35)
        
//        let creatorId = KCSUser.active().userId!
//        print(creatorId)
//        let query = Query(format: "creatorId == %@", creatorId)
//        storeEvents.find(query) { (events, error) in
//            if error != nil {
//                print(error)
//                //completionHandler(false, nil)
//            }
//            else {
//                print("??? ueeee")
//                if events?.count != 0 {
//                    let evento = events![0] //as! Evento
//                    //completionHandler(true, evento)
//                } else {
//                    //completionHandler(false, nil)
//                }
//            }
//        }
//        Evento.checkUserHasEventRunning { (success, evento) in
//            if success {
//                self.event = evento
//                let tap_edit_event = UITapGestureRecognizer(target: self, action: #selector(TTTabBar.openEditEvent(_:)))
//                print(evento!.desc!)
//                self.goButton.gestureRecognizers?.removeAll()
//                self.goButton.addGestureRecognizer(tap_edit_event)
//            } else if !success && evento == nil {
//                print("nao tem evento criado???")
                let tap_createEvent = UITapGestureRecognizer(target: self, action: #selector(TTTabBar.openCreateEvent(_:)))
                self.goButton.addGestureRecognizer(tap_createEvent)
//            }
//        }
        
        let item3 = TTTabBarItem(viewController: controllerDiscover!)
        item3.image = UIImage(named: "bt_discover")
        item3.selectedImage = UIImage(named: "bt_discover")
        
        let item4 = TTTabBarItem(viewController: controllerSettings!)
        item4.image = UIImage(named: "bt_settings")
        item4.selectedImage = UIImage(named: "bt_settings")
        
        
        self.tabBarItems = [item, item2, goButton , item3, item4]
        
        //contentTabBarView.backgroundColor = //.clearColor()
        let frameGoToBar = CGRect(x: contentTabBarView.frame.origin.x, y: contentTabBarView.frame.origin.y, width: self.view.frame.width, height: contentTabBarView.frame.height)
        let imageView = UIImageView(frame: frameGoToBar)
        imageView.image = UIImage(named: "gotoTabBar")
        imageView.contentMode = .scaleAspectFill
        self.contentTabBarView.addSubview(imageView)
        
        
        self.updateTabBarView()
        
    }
    
    func hideOrShow(_ n: Notification) {
        let hide = (n as NSNotification).userInfo!["hide"] as! Bool
        if hide {
            self.hideTabBar(true)
        } else {
            self.showTabBar(true)
        }
    }
    
    open override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    open override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
        return .fade
    }

    override open var prefersStatusBarHidden : Bool {
        if activeTabBar!.viewController!.restorationIdentifier == "Discover" {
            return true
        } else {
            return false
        }
    }
    
    //Modify tabbar with custom options
    func updateTabBarView() {
        
        //Update the detailView
        detailView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height-tabBarHeight)
        
        //Update TabBar view
        tabBarView.frame = CGRect(x: 0, y: self.view.frame.height-tabBarHeight, width: self.view.frame.width, height: tabBarHeight)
        
        //Verify that has defaultTabBar
        if let tabBar = defaultTabBarItem {
            
        }else{
            if tabBarItems.count > 0 {
                defaultTabBarItem = tabBarItems[0]
            }
        }
        
        self.updateBackgroundColor(tabBackgroundColor)
        self.updateTabBarHeight()
        self.renderButtons()
    }
    
    fileprivate func updateBackgroundColor (_ color: UIColor) {
        contentTabBarView.backgroundColor = color
        tabBarView.backgroundColor = color
    }
    
    func updateTabBarHeight() {
        for tabBarItem in tabBarItems {
            
            //if one button is greather default. Set the tabBarView greather
            if tabBarItem.frame.height + tabBarItem.offsetBottom > tabBarView.frame.height {
                tabBarHeight = tabBarItem.frame.height + tabBarItem.offsetBottom
                tabBarView.frame = CGRect(x: 0, y: self.view.frame.height-tabBarHeight, width: self.view.frame.width, height: tabBarHeight)
                contentTabBarView.frame = CGRect(x: 0, y: tabBarHeight-initialTabBarHeight, width: self.view.frame.width, height: initialTabBarHeight)
                
                
            }
        }
    }
    
    fileprivate func renderButtons() {
        
        //Get all TabBarItems and divide the width between
        let widthPerButton = (self.view.frame.width - spaceBetweenTabs*CGFloat(tabBarItems.count))/CGFloat(tabBarItems.count)
        
        //Add subview buttons to tabBar
        var tempX = spaceBetweenTabs //Start position of buttons, logical position
        for tabBarItem in tabBarItems {
            //Set images if has
            if let image = tabBarItem.image {
                tabBarItem.setImage(image, for: UIControlState())
            }
            
            if defaultTabBarItem == tabBarItem {
                //Load the default VC
                self.loadViewControllerFrom(tabBarItem)
            }
            
            //Call action on touchUpInside
            tabBarItem.addTarget(self, action: #selector(TTTabBar.tabBarItemClicked(_:)), for: UIControlEvents.touchUpInside)
            
            //if the height of the tabBarItem, is default (40), change to tabBarView height
            var newHeight = tabBarItem.frame.height
            if tabBarItem.frame.height == defaultTabBarHeight {
                newHeight = defaultTabBarHeight
            }
            
            //if width of tanBarItem is not 0, set the custom size
            var customWidth = widthPerButton
            var positionX = tempX
            if tabBarItem.frame.width > 0 {
                customWidth = tabBarItem.frame.width
                
                //New position of X
                //Center button on distance between buttons + custom width
                positionX = tempX + widthPerButton/2 - customWidth/2
            }
            
            //Modify frame of TabBarItems
            //- tabBarItem.offsetBottom
            let off = (tabBarView.frame.height - tabBarItem.frame.height) - tabBarItem.offsetBottom
            tabBarItem.frame = CGRect(x: positionX, y: tabBarItem.offsetY + off , width: customWidth, height: newHeight)
            tempX += widthPerButton+spaceBetweenTabs
            
            tabBarView.addSubview(tabBarItem)
        }
    }
    
    func tabBarItemClicked(_ sender: AnyObject) {
        if let tabBar = sender as? TTTabBarItem {
            //self.prefersStatusBarHidden()
            self.loadViewControllerFrom(tabBar)
        }
    }
    
    func loadViewControllerFrom(_ tabBarItem: TTTabBarItem?) {
        if let item = tabBarItem {
            if !self.ttTabBar(self, shouldChangeTab: item) {
                return
            }
            
            //if users click on the same tab that is active, return
            if let tabBar = activeTabBar {
                if item == activeTabBar {
                    return
                }
            }
        }
        
        //Change image to the old tabBarItem to no selected
        if let tabBar = activeTabBar {
            ttTabBar(self, tabWillDisappear: tabBar)
            if let image = tabBar.image {
                tabBar.setImage(image, for: UIControlState())
            }
            
            //Remove actual View
            if let vc = tabBar.viewController {
                vc.view.removeFromSuperview()
            }
            ttTabBar(self, tabDidDisappear: tabBar)
        }
        
        if let view = activeView {
            view.removeFromSuperview()
        }
        
        if let item = tabBarItem {
            //Change image to the new tabBarItem to selected
            if let image = item.selectedImage {
                item.setImage(image, for: UIControlState())
            }
            
            
            //add VC to detailView
            if let vc = item.viewController {
                ttTabBar(self, tabWillAppear: item)
                //set active bar
                activeTabBar = item
                self.prefersStatusBarHidden
                vc.view.frame = self.detailView.bounds
                detailView.addSubview(vc.view)
                self.addChildViewController(vc)
                vc.didMove(toParentViewController: self)
                
                ttTabBar(self, tabDidAppear: item)
            }
        }
    }
    
    //IF you need to load an external view controller, that is not on the tab menu
    func loadViewController(_ vc: UIViewController) {
        //Change image to the old tabBarItem to no selected
        if let tabBar = activeTabBar {
            ttTabBar(self, tabWillDisappear: tabBar)
            if let image = tabBar.image {
                tabBar.setImage(image, for: UIControlState())
            }
            
            //Remove actual View
            if let vc = tabBar.viewController {
                vc.view.removeFromSuperview()
            }
            ttTabBar(self, tabDidDisappear: tabBar)
        }
        
        if let view = activeView {
            view.removeFromSuperview()
        }
        
        //add VC to detailView
        //set active bar
        //activeTabBar = nil
        activeView = vc.view
        
        vc.view.frame = self.detailView.bounds
        detailView.addSubview(vc.view)
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: self)
    }
    
    // MARK: - Get Functions
    func getActualController() -> UIViewController? {
        if let activeTab = activeTabBar {
            return activeTab.viewController
        }
        return nil
    }
    
    //MARK: hide/show tabBar
    func hideTabBar(_ animated: Bool) {
        if !tabBarHidden  {
            UIView.animate(withDuration: 0.8, animations: {
                self.tabBarView.frame.origin.y += self.tabBarView.frame.width
                self.detailView.frame.size.height = self.view.frame.height
            }) 
            tabBarHidden = true
        }
    }
    
    func showTabBar(_ animated: Bool) {
        if tabBarHidden  {
            UIView.animate(withDuration: 0.8, animations: {
                self.tabBarView.frame.origin.y -= self.tabBarView.frame.width
                //self.detailView.frame.size.height -= self.contentTabBarView.frame.height
            }) 
            
            tabBarHidden = false
        }
        
    }
    
    // MARK: - Active TabBar
    func isTabSelected(_ tab: TTTabBarItem) {
        if activeTabBar == tab {
            
        }
    }
    
    
    //MARK: overridable Func
    internal func ttTabBar(_ tabBar: TTTabBar, shouldChangeTab tabBarItem: TTTabBarItem) -> Bool {
        if tabBarItem.isButton {
            self.ttTabBar(tabBar, buttonHasBeenClicked: tabBarItem)
            return false
        }
        
        return true
    }
    
    internal func ttTabBar(_ tabBar: TTTabBar, tabWillDisappear tabBarItem: TTTabBarItem) {
        
        if tabBarItem.viewController?.restorationIdentifier == "Discover" {
            print("clicou no f / esconder status bar e barra")
            //hideStatusBar = false
            self.showTabBar(true) //(true)
            self.prefersStatusBarHidden
        }
        
    }
    
    internal func ttTabBar(_ tabBar: TTTabBar, tabDidDisappear tabBarItem: TTTabBarItem) {
        
    }
    
    internal func ttTabBar(_ tabBar: TTTabBar, tabWillAppear tabBarItem: TTTabBarItem) {
        if tabBarItem.viewController?.restorationIdentifier == "Discover" {
            //print("clicou no discover / esconder status bar e barra")
            //hideStatusBar = true
            self.hideTabBar(true)
            UIApplication.shared.isStatusBarHidden = true
            self.prefersStatusBarHidden
        } else {
            print("nao é")
        }
        
    }
    
    internal func ttTabBar(_ tabBar: TTTabBar, tabDidAppear tabBarItem: TTTabBarItem) {
        if tabBarItem.viewController?.restorationIdentifier == "Discover" {
            //print("clicou no discover / esconder status bar e barra")
            //hideStatusBar = true
            //self.hideTabBar(true)
            UIApplication.shared.isStatusBarHidden = true
            //self.prefersStatusBarHidden()
        } else {
            print("nao é")
        }
    }
    
    internal func ttTabBar(_ tabBar: TTTabBar, buttonHasBeenClicked tabBarItem: TTTabBarItem) {
        
    }
    
    // GOTO MAIN BUTTON FUNCTIONS //
    
    func openCreateEvent(_ recognizer: UITapGestureRecognizer) {
        let controllerEvent = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventUserNC") as! UINavigationController
        self.present(controllerEvent, animated: true, completion: nil)
    }
    
    func changeMainButtonToEdit(_ notification: Notification) {
        let evento = (notification as NSNotification).userInfo!["event"] as! Evento
        //globalEventId = evento.entityId
        self.event = evento
        let tap_edit_event = UITapGestureRecognizer(target: self, action: #selector(TTTabBar.openEditEvent(_:)))
        self.goButton.gestureRecognizers?.removeAll()
        self.goButton.addGestureRecognizer(tap_edit_event)
    }
    
    func openEditEvent(_ recognizer: UITapGestureRecognizer) {
        let controllerEditEvent = self.storyboard?.instantiateViewController(withIdentifier: "EditEventVC") as! EditEventViewController
        if self.event != nil {
            controllerEditEvent.evento = self.event
            //controllerEditEvent.eventId = self.event?.entityId
            controllerEditEvent.eventoBuscado = self.event
            print(self.event!.desc)
        } else {
            print("ta nulo <<<")
        }
        //controllerEditEvent.evento = self.event
        self.present(controllerEditEvent, animated: true, completion: nil)
    }
    
    func rotateAndChangeFunction(_ notification: Notification) {
        let angleRadians = (notification as NSNotification).userInfo!["angle"] as! Double
        //let evento = notification.userInfo!["event"] as! Evento
        let eventId = (notification as NSNotification).userInfo!["eventId"] as! String
        globalEventId = eventId
        let locationEvento = (notification as NSNotification).userInfo!["location"] as! CLLocation
        //currentEvent = evento
        currentEventLocation = locationEvento
        //currentEventEndDate = evento.endDate
        self.angleEvent = angleRadians
        UIView.animate(withDuration: 1.5, animations: {
            self.goButton.transform = CGAffineTransform(rotationAngle: CGFloat(angleRadians))
            
            }, completion: { (success) in
                if success {
                    print("uhul << change to IR function")
                    //self.goButton.setImage(UIImage(named: "GoButtonGo"), forState: UIControlState.Normal)
                    self.goButton.gestureRecognizers?.removeAll()
                    let tap_gotoevent = UITapGestureRecognizer(target: self, action: #selector(TTTabBar.gotoEvent(_:)))
                    self.goButton.addGestureRecognizer(tap_gotoevent)
                    let tap_cancelgoto = UILongPressGestureRecognizer(target: self, action: #selector(TTTabBar.cancelGoToEvent(_:)))
                    self.goButton.addGestureRecognizer(tap_cancelgoto)
            }
        }) 
    }
    
    func gotoEvent(_ r: UITapGestureRecognizer) {
        // animar o botao carregando para depois funcionar como bússola
//        Evento.reloadEvent(globalEventId!) { (success, event) in
//            if !success {
//                print("error")
//            } else {
//                print("going to event")
//                currentEvent = event
//                currentEventEndDate = event?.endDate
//                goToEvent = true
//                startMonitoringEventDuration()
//                self.setupNotification()
//            }
//        }
    }
    
    func setupNotification() {
        let locattionnotification = UILocalNotification()
        locattionnotification.alertBody = "Você já chegou?"
        locattionnotification.regionTriggersOnce = true
        let region = CLCircularRegion(center: currentEventLocation!.coordinate, radius: 100.0, identifier: "CurrentEvent")
        let regionOut = CLCircularRegion(center: currentEventLocation!.coordinate, radius: 300.0, identifier: "CurrentEventExitRegion")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MonitoraRegiao"), object: nil, userInfo: ["region" : region, "regionExit" : regionOut])
        locattionnotification.region = region
        locattionnotification.category = "EVENT_CATEGORY"
        UIApplication.shared.scheduleLocalNotification(locattionnotification)
    }
    
    func cancelGoToEvent(_ r: UILongPressGestureRecognizer) {
        if goToEvent == true {
            let alert = UIAlertController(title: "Cancelar", message: "Deseja mesmo cancelar Ir ao evento?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Não", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Sim", style: .default, handler: { (action) in
                self.goButton.gestureRecognizers?.removeAll()
                goToEvent = false
//                Evento.checkUserHasEventRunning { (hasEvent, event) in
//                    if hasEvent {
//                        self.event = event
//                        let tap_edit_event = UITapGestureRecognizer(target: self, action: #selector(TTTabBar.openEditEvent(_:)))
//                        print(event!.desc!)
//                        self.goButton.addGestureRecognizer(tap_edit_event)
//                    } else {
//                        let tap_createEvent = UITapGestureRecognizer(target: self, action: #selector(TTTabBar.openCreateEvent(_:)))
//                        self.goButton.addGestureRecognizer(tap_createEvent)
//                    }
//                }
                UIView.animate(withDuration: 2.0, animations: {
                    self.goButton.transform = CGAffineTransform(rotationAngle: CGFloat(0.0))
                    }, completion: { (sucess) in
                        print("changed to Default")
                        self.goButton.setImage(UIImage(named: "GoButton"), for: UIControlState())
                })
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func changeToInsideEvent(_ n: Notification) {
        let isInside = (n as NSNotification).userInfo!["inside"] as! Bool
        let event = (n as NSNotification).userInfo!["event"] as! Evento
        self.event = event
        if isInside {
            UIView.animate(withDuration: 1.5, animations: {
                self.goButton.transform = CGAffineTransform(rotationAngle: CGFloat(0.0))
                }, completion: { (sucess) in
                    print("changed to Default")
                    self.goButton.setImage(UIImage(named: "GoButton"), for: UIControlState())
                    self.goButton.gestureRecognizers?.removeAll()
                    let tap_insideEvent = UITapGestureRecognizer(target: self, action: #selector(TTTabBar.gotoInsideEvent))
                    self.goButton.addGestureRecognizer(tap_insideEvent)
            })
        } else {
            UIView.animate(withDuration: 1.5, animations: {
                self.goButton.transform = CGAffineTransform(rotationAngle: CGFloat(0.0))
                }, completion: { (sucess) in
                    print("changed to Default")
                    self.goButton.setImage(UIImage(named: "GoButton"), for: UIControlState())
                    self.goButton.gestureRecognizers?.removeAll()
                    let tap_createEvent = UITapGestureRecognizer(target: self, action: #selector(TTTabBar.openCreateEvent(_:)))
                    self.goButton.addGestureRecognizer(tap_createEvent)
            })
        }
    }
    
    func gotoInsideEvent() {
        let controllerInside = self.storyboard?.instantiateViewController(withIdentifier: "InsideEvent") as! InsideEventViewController
        controllerInside.evento = self.event
        self.present(controllerInside, animated: true, completion: nil)
    }
    
    func atualizarCompass() {
        if self.angleEvent != nil && compass != nil {
            let totalAngle = self.angleEvent! - compass!
            UIView.animate(withDuration: 0.1, animations: {
                self.goButton.transform = CGAffineTransform(rotationAngle: CGFloat(totalAngle))
                
            }, completion: { (success) in
                if success {
                    
                }
            }) 
        }
    }
    
    func presentBehindBar(_ n: Notification) {
        let present = (n as NSNotification).userInfo!["present"] as! Bool
        if present {
            print("entrou no behind bar")
            let controller = (n as NSNotification).userInfo!["controller"] as! UIViewController
            self.presentedVC = controller
            controller.modalPresentationStyle = .currentContext
            self.loadViewController(controller)
        } else {
            self.getActualController()!.dismiss(animated: true, completion: {
//            let controller = self.storyboard?.instantiateViewControllerWithIdentifier("TTTabBar") as! TTTabBar
//            self.presentViewController(controller, animated: false, completion: nil)
            })
        }
    }
}

extension TTTabBar {
    
    static func presentViewUnderTabBar(_ controller: UIViewController) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "PresentBehindBar"), object: nil, userInfo: ["controller" : controller, "present" : true])
    }
    
    static func dismissViewUnderTabBar() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "PresentBehindBar"), object: nil, userInfo: ["present" : false])
    }
}
