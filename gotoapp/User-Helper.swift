////
////  User-Helper.swift
////  gotoapp
////
////  Created by Sandor ferreira da silva on 11/07/16.
////  Copyright © 2016 AppsCat. All rights reserved.
////
//
//import Foundation
//import UIKit
//
//extension KCSUser {
//    static func isUserPremium() -> Bool {
//        let isPremium = KCSUser.active().getValueForAttribute("isPremium") as! Int
//        if isPremium == 1 {
//            return true
//        } else {
//            return false
//        }
//    }
//    
//    static func returnRemainingEvents() -> Int {
//        let remainingEvents = KCSUser.active().getValueForAttribute("remainingEvents") as! Int
//        return remainingEvents
//    }
//}
