//
//  FunPoint.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 17/06/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit
import Foundation
import MapKit

class FunPoint {
    var entityId: String?
    var premium: NSNumber?
    //var endDate: Date?
    var place: CLLocation?
    var currentEventName: String?
    var currentEventAddress: String?
    var currentEventImage: String?
    
    // premium properties // 
    var image: String?
    var desc: String?
    var backgroundColor: String?

//    override class func collectionName() -> String {
//        return "FunPoint"
//    }
    
    init(premium: NSNumber, place: CLLocation, event: Evento, currentEventName: String, currentEventAddress: String) {
        //super.init()
        self.premium = premium
        self.place = place
        self.currentEventName = currentEventName
        self.currentEventAddress = currentEventAddress
        //self.endDate = self.event!.endDate as Date?
    }

    
    
    
//    required init(realm: RLMRealm, schema: RLMObjectSchema) {
//        super.init(realm: realm, schema: schema)
//        //fatalError("init(realm:schema:) has not been implemented")
//    }
//    
//    required init?(map: Map) {
//        super.init(map: map)
//        //fatalError("init(map:) has not been implemented")
//    }
//    
//    required init() {
//        super.init()
//        //fatalError("init() has not been implemented")
//    }
//    
//    required init(value: Any, schema: RLMSchema) {
//        super.init(value: value, schema: schema)
//        //fatalError("init(value:schema:) has not been implemented")
//    }
//    
//    internal override func hostToKinveyPropertyMapping() -> [AnyHashable: Any]! {
//        return [
//        "entityId": KCSEntityKeyId,
//        "premium": "premium",
//        "place": KCSEntityKeyGeolocation,
//        "event": "event",
//        "endDate" : "endDate",
//        "currentEventName":"currentEventName",
//        "currentEventAddress":"currentEventAddress",
//        "currentEventImage" : "currentEventImage",
//        "image":"image",
//        "desc":"desc"
//        ]
//    }
//    
//    internal static override func kinveyPropertyToCollectionMapping() -> [AnyHashable: Any]! {
//        return [
//            "event" /* backend field name */ : "Events" /* collection name for invitations */
//        ]
//    }
//    
//    internal static override func referenceKinveyPropertiesOfObjectsToSave() -> [Any]! {
//        return [ "event" as AnyObject ]
//    }
}
