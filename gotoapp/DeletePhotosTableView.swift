//
//  DeletePhotosTableView.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 24/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

class DeletePhotosTableView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableViewDelete: UITableView!
    
    
    var base64EncodedImages: [String]?
    var eventId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewDelete.delegate = self
        tableViewDelete.dataSource = self
        
        print("AAAAAAAA")
        NotificationCenter.default.addObserver(self, selector: #selector(DeletePhotosTableView.getInfoFromEvent(_:)), name: NSNotification.Name(rawValue: "PegarInformacoesDoEvento"), object: nil)
    }
    
    func getInfoFromEvent(_ n: Notification) {
        let eventId = (n as NSNotification).userInfo!["eventId"] as! String
        let photosArray = (n as NSNotification).userInfo!["photosArray"] as! [String]
        
        self.base64EncodedImages = photosArray
        self.eventId = eventId
        
        tableViewDelete.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return base64EncodedImages?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let base64EncodedString = base64EncodedImages![(indexPath as NSIndexPath).row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoEventCell") as! DeletePhotoCell
        cell.photoTaken.image = returnPhotoFromBase64EncodedString(base64EncodedString)
        cell.deleteButton.tag = (indexPath as NSIndexPath).row
        
        return cell
    }
    
    
    @IBAction func deletePhoto(_ sender: AnyObject) {
        // add activity indicator
        let index = sender.tag
        
        let alert = UIAlertController(title: "Deletar", message: "Você tem certeza que deseja deletar essa foto do seu evento? Essa ação não pode ser desfeita.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Sim", style: .cancel, handler: { (action) in
            let base64EncodedStringToDelete = self.base64EncodedImages![index!]
//            Evento.deletePhotoFromEvent(self.eventId!, base64EncodedString: base64EncodedStringToDelete) { (success) in
//                if success {
//                    //stop activity indicator
//                    print("deletou com sucesso")
//                    self.dismiss(animated: false, completion: nil)
//                } else {
//                    // ocorreu um erro ao deletar a foto
//                }
//            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)

    }
    
    
    @IBAction func dismissSelf(_ sender: AnyObject) {
        self.dismiss(animated: false, completion: nil)
    }
    
//    @IBAction func dismissSelf(sender: AnyObject) {
//
//    }
    
}
