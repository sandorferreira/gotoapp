//
//  HomeScreenViewController.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 18/05/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit
import GoogleMaps
import FirebaseDatabase

class HomeScreenViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var homeScreenNavigationBar: UINavigationBar!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var eventListContainerView: UIView!
    
    @IBOutlet weak var subAuxView: UIView!
    
    @IBOutlet weak var navigationBarMapButton: UIBarButtonItem!
    
    @IBOutlet weak var titleGoToView: UIView!
    
    var locationManager = CLLocationManager()
    var funPoints = [FunPoint]()
    var auxIDS = [String]()
    var auxMarkers = [FunPointMarker]()
    
    // aux variables
    var auxEvent: Evento?
    var auxFunPoint: FunPoint?
    var keys = [String]()
    
    // geoFire
    //let geoFireReference =
    let geoFire = GeoFire(firebaseRef: ref.reference(withPath: "Locations"))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("entrou?")
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingHeading()
        locationManager.allowsBackgroundLocationUpdates = true
        mapView.delegate = self
        
        setupNavigationBarColorToTransparent()
        self.view.backgroundColor = gotoColor
        //retrieveEventsForTest()
        
        // -- Notifications -- //
        NotificationCenter.default.addObserver(self, selector: #selector(HomeScreenViewController.monitorRegion(_:)), name: NSNotification.Name(rawValue: "MonitoraRegiao"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeScreenViewController.createEventMarker(_:)), name: NSNotification.Name(rawValue: "CriarMarker"), object: nil)
        
        let controllerEventDetail = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailVC") as! EventDetailViewController
        NotificationCenter.default.addObserver(controllerEventDetail, selector: #selector(EventDetailViewController.enteredEventRegion(_:)), name: NSNotification.Name(rawValue: NotificationEstaNoEvento), object: nil)

        // container view
        
        let controllerListContainer = self.storyboard?.instantiateViewController(withIdentifier: "ListaEventosHomeScreen") as! EventListViewController
        controllerListContainer.loadView()
        //controllerListContainer.viewDidLoad()
        //controllerListContainer.loadView()
        self.eventListContainerView.isHidden = true
        self.mapView.isHidden = false
        subAuxView.addSubview(mapView)
        
        let tap_gotoLocation = UITapGestureRecognizer(target: self, action: #selector(HomeScreenViewController.gotoMyLocation))
        titleGoToView.addGestureRecognizer(tap_gotoLocation)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // mapView.padding = UIEdgeInsetsMake(0, 0, 20, 0)
//        if goToEvent == true {
//            clearAllLocalNotifications()
//        }
    }
    
    func gotoMyLocation() {
        mapView.animate(toLocation: usersLocation!.coordinate)
    }
    
    func monitorRegion(_ notification: Notification) {
        let region = (notification as NSNotification).userInfo!["region"] as! CLRegion
        let regionOut = (notification as NSNotification).userInfo!["regionExit"] as! CLRegion
        locationManager.startMonitoring(for: regionOut)
        locationManager.startMonitoring(for: region)
    }
    
    func createEventMarker(_ notification: Notification) {
        
        let circleQuery = geoFire?.query(at: usersLocation, withRadius: 4.0)
        circleQuery?.observe(.keyEntered, with: { (key, location) in
            if key != nil {
                if !self.isKeyLoaded(key: key!) {
                    self.keys.append(key!)
                    let reference = FIRDatabase.database().reference(withPath: "Events").child(key!)
                    reference.observe(.value, with: { (snapshot) in
                        let event = snapshot.value as! [String: AnyObject]
                        let name = event["name"] as! String
                        let address = event["address"] as! String
                        let desc = event["desc"] as! String
                        let image = event["image"] as! String
                        let startDate = event["startDate"] as! String
                        let endDate = event["endDate"] as! String
                        let usersTokens = event["usersTokens"] as! [String]
                        let creatorId = event["creatorId"] as! String
                        let linkPurchase = event["linkPurchase"] as! String
                        let auxEvent = Evento(name: name, creatorId: creatorId, desc: desc, image: image, linkPurchase: linkPurchase, startDate: startDate, endDate: endDate, customBackgroundColor: "", type: "", address: address)
                        auxEvent.usersTokens = usersTokens
                        if let friendsTokens = event["friendsTokens"] as? [String] {
                            auxEvent.friendsTokens = friendsTokens
                        }
                        if let photosArray = event["photosArray"] as? [String] {
                            auxEvent.photosArray = photosArray
                        }
                        let marker = FunPointMarker(place: location!, evento: auxEvent)
                        marker.map = self.mapView
                    })
                }
                
            }
        })

        
//        let latitude = usersLocation!.coordinate.latitude
//        let longitude = usersLocation!.coordinate.longitude
//        let point = [latitude, longitude]
//        
////        let q1 = Query(
////            onField: KCSEntityKeyGeolocation,
////            using: KCSQueryConditional.kcsNearSphere,
////            forValue: point as NSObject!
////        )
//        
//        //let querya = Query(form)
//        
//        storeFunPoint?.query(withQuery: q1, withCompletionBlock: { (results, error) in
//                if error != nil {
//                    print(error)
//                } else {
//                    //var auxEventInfo = [NSDictionary]()
//                    for result in results! {
//                        let funPointAux = result as! FunPoint
//                        if !self.containsFunPoints(funPointAux.entityId!, arrayIds: self.auxIDS) {
//                            self.auxIDS.append(funPointAux.entityId!)
//                            let marker = FunPointMarker(place: funPointAux.place!, funPoint: funPointAux)
//                            if let eventInfo = (result as AnyObject).value(forKey: "event") as? NSDictionary {
//                                if let eventId = eventInfo["_id"] as? String {
//                                    marker.eventId = eventId
//                                    let eventInfo: NSDictionary = ["eventName" : funPointAux.currentEventName!,
//                                        "eventImage" : funPointAux.currentEventImage!, "eventLocation" : funPointAux.place!, "eventId" : eventId]
//                                    //auxEventInfo.append(eventInfo)
//                                NotificationCenter.default.post(name: Notification.Name(rawValue: "AtualizarMarkersLista"), object: nil, userInfo: ["eventInfo" : eventInfo])
//                                    print("inseriu event id!")
//                                }
//                            }
//                            self.auxMarkers.append(marker)
//                            marker.map = self.mapView
//                        }
//                    }
//                    
////                    if auxEventInfo.count != 0 {
////                        if let orderedEvents = self.sortStoreByDistance(auxEventInfo) {
////                            print("inseriu")
////                            
////                        } else {
////                            print("ordered vazio!")
////                        }
////                    } else {
////                        print("auxEventInfo count == 0")
////                    }
//                    
////                    if let orderedEvents = self.sortStoreByDistance(auxEventInfo) {
////                        NSNotificationCenter.defaultCenter().postNotificationName("AtualizarMarkersLista", object: nil, userInfo: ["eventInfo" : orderedEvents])
////                    }
//                }
//            }, withProgressBlock: nil)
    }
    
    func isKeyLoaded(key: String) -> Bool{
        for k in self.keys {
            if k == key {
                return true
            }
        }
        return false
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        // 3
        if status == .authorizedAlways {
            
            // 4
            locationManager.startUpdatingLocation()
            
            //5
            mapView.isMyLocationEnabled = true
            //mapView.settings.myLocationButton = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            usersLocation = location
            print("entrou no location?")
            mapView.camera = GMSCameraPosition(target: usersLocation!.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            
            if currentEvent != nil && usersLocation != nil {
            }
            
            // 8
            locationManager.stopUpdatingLocation()
        }else{
            print("AHOY")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        if region.identifier == "CurrentEvent" {
            print("requesting....")
            self.locationManager.requestState(for: region)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        if state == .inside {
            print("está dentro")
            isInsideEvent = true
            NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationEstaNoEvento), object: nil, userInfo: ["inside" : true])
        } else {
            isInsideEvent = false
            print("desconhecida")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if region.identifier == "CurrentEvent" {
            NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationEstaNoEvento), object: nil, userInfo: ["inside" : true])
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        if region.identifier == "CurrentEventExitRegion" {
            didExitCurrentEvent()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        compass = self.degreesToRadians(newHeading.magneticHeading)
        if goToEvent == true {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "AtualizarCompass"), object: nil)
        }
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let location = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
        let circleQuery = geoFire?.query(at: location, withRadius: 1.0)
        
        circleQuery?.observe(.keyEntered, with: { (key, location) in
            if key != nil {
                if !self.isKeyLoaded(key: key!) {
                    self.keys.append(key!)
                    let reference = FIRDatabase.database().reference(withPath: "Events").child(key!)
                    reference.observe(.value, with: { (snapshot) in
                        let event = snapshot.value as! [String: AnyObject]
                        let name = event["name"] as! String
                        let address = event["address"] as! String
                        let desc = event["desc"] as! String
                        let image = event["image"] as! String
                        let startDate = event["startDate"] as! String
                        let endDate = event["endDate"] as! String
                        let usersTokens = event["usersTokens"] as! [String]
                        let creatorId = event["creatorId"] as! String
                        let linkPurchase = event["linkPurchase"] as! String
                        let auxEvent = Evento(name: name, creatorId: creatorId, desc: desc, image: image, linkPurchase: linkPurchase, startDate: startDate, endDate: endDate, customBackgroundColor: "", type: "", address: address)
                        auxEvent.eventId = key
                        auxEvent.usersTokens = usersTokens
                        if let friendsTokens = event["friendsTokens"] as? [String] {
                            auxEvent.friendsTokens = friendsTokens
                        }
                        if let photosArray = event["photosArray"] as? [String] {
                            auxEvent.photosArray = photosArray
                        }
                        let marker = FunPointMarker(place: location!, evento: auxEvent)
                        marker.map = self.mapView
                    })
                }
                
            }
        })
//        let latitude = position.target.latitude
//        let longitude = position.target.longitude
//        let point = [latitude, longitude]
//
//        print(point)
//        let q2 = KCSQuery(
//            onField: KCSEntityKeyGeolocation,
//            using: KCSQueryConditional.kcsNearSphere,
//            forValue: point as NSObject!
//        )
        
//        storeUnlinkedFP?.query(withQuery: q2, withCompletionBlock: { (results, error) in
//            if error != nil {
//                print(error)
//            } else {
//                //var auxEventInfo = [NSDictionary]()
//                for result in results! {
//                    let funPointAux = result as! FunPoint
//                    if !self.containsFunPoints(funPointAux.entityId!, arrayIds: self.auxIDS) {
//                        self.auxIDS.append(funPointAux.entityId!)
//                        let marker = FunPointMarker(place: funPointAux.place!, funPoint: funPointAux)
//                        if let eventInfo = (result as AnyObject).value(forKey: "event") as? NSDictionary {
//                            print("inseriu event id! 1")
//                            if let eventId = eventInfo["_id"] as? String {
//                                print("inseriu event id! 2")
//                                marker.eventId = eventId
//                                let eventInfo: NSDictionary = ["eventName" : funPointAux.currentEventName!,
//                                    "eventImage" : funPointAux.currentEventImage!, "eventLocation" : funPointAux.place!, "eventId" : eventId]
//                                nearEventIds.append(eventId)
//                                NotificationCenter.default.post(name: Notification.Name(rawValue: "AtualizarMarkersLista"), object: nil, userInfo: ["eventInfo" : eventInfo])
//                            }
//                        }
//                        
//                        self.auxMarkers.append(marker)
//                        marker.map = self.mapView
//                    }
//                }
//                
////                if auxEventInfo.count != 0 {
////                    if let orderedEvents = self.sortStoreByDistance(auxEventInfo) {
////                        print("inseriu")
////                        
////                    } else {
////                        print("ordered vazio!")
////                    }
////                } else {
////                    print("auxEventInfo count == 0")
////                }
//                
//                
//            }
//            }, withProgressBlock: nil)
    }
    
    func setupNavigationBarColorToTransparent() {
        self.homeScreenNavigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.homeScreenNavigationBar.shadowImage = UIImage()
        self.homeScreenNavigationBar.backgroundColor = UIColor.clear
        
        self.homeScreenNavigationBar.tintColor = UIColor.white
        self.homeScreenNavigationBar.titleTextAttributes = ([NSFontAttributeName: UIFont(name: "Helvetica Neue", size: 17)!,
            NSForegroundColorAttributeName: UIColor.white])
    }
    
    func containsFunPoints(_ id: String, arrayIds: [String]) -> Bool {
        for aux in arrayIds {
            if id == aux {
                return true
            }
        }
        return false
    }
    
    func checkEventTimestamp(_ start: String, end: String) -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss ZZZZ"
        let dateInicio = dateFormatter.date(from: start)
        let dataFinal = dateFormatter.date(from: end)
        
        // comparing with current date value
        let currentDate = Date()
        let compareInicio = currentDate.compare(dateInicio!)
        let compareFim = currentDate.compare(dataFinal!)
        
        if compareInicio == ComparisonResult.orderedDescending && compareFim == ComparisonResult.orderedAscending {
            return true
        } else {
            return false
        }
    }
    
    
    // -- MARKERS FUNCTIONS -- //
    

    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        let eventMarker = marker as! FunPointMarker
        let event = eventMarker.event
        
        //let controller = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailVC") as! EventDetailViewController
        //controller.evento = event
        //controller.eventId = event.eventId!
        let navController = self.storyboard?.instantiateViewController(withIdentifier: "navigationEventDetail") as! UINavigationController
        let controller = navController.viewControllers.first as! EventDetailViewController
        controller.evento = event
        controller.eventId = event.eventId!
        self.present(navController, animated: true, completion: nil)
    
//
//        let funPointMarker = marker as! FunPointMarker
//        if let eventId = funPointMarker.eventId {
//            print("recebeu event Id")
//            let controller = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailVC") as! EventDetailViewController
//            controller.eventId = eventId
//            self.present(controller, animated: true, completion: nil)
//            //TTTabBar.presentViewUnderTabBar(controller)
//        } else {
//            let controller = self.storyboard!.instantiateViewController(withIdentifier: "NoFutureEvents") as! PremiumNoEventViewController
//            controller.funPointPremium = funPointMarker.funPoint
//            self.present(controller, animated: true, completion: nil)
//            //TTTabBar.presentViewUnderTabBar(controller)
//        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
//        Evento.checkUserHasEventRunning { (hasEvent, event) in
//            if !hasEvent {
//                let auxFunPoint = marker as! FunPointMarker
//                let location = auxFunPoint.place
//                let angleRadians = self.getBearingBetweenTwoPoints1(usersLocation!, point2: auxFunPoint.place)
//        
//                if let eventId = auxFunPoint.eventId {
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: "IrAoEvento"), object: nil, userInfo: ["angle" : angleRadians, "location" : location, "eventId" : eventId])
//                }
//            }
//        }
//        
        return false
    }
    
    func degreesToRadians(_ degrees: Double) -> Double { return degrees * M_PI / 180.0 }
    
    func getBearingBetweenTwoPoints1(_ point1 : CLLocation, point2 : CLLocation) -> Double {
        
        let lat1 = degreesToRadians(point1.coordinate.latitude)
        let lon1 = degreesToRadians(point1.coordinate.longitude)
        
        let lat2 = degreesToRadians(point2.coordinate.latitude);
        let lon2 = degreesToRadians(point2.coordinate.longitude);
        
        let dLon = lon2 - lon1;
        
        let y = sin(dLon) * cos(lat2);
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
        let radiansBearing = atan2(y, x);
        
        return radiansBearing
    }
    
    @IBAction func changeMapToList(_ sender: AnyObject) {
        if self.subAuxView.subviews.last == self.mapView {
            
            self.eventListContainerView.isHidden = false
//            let controllerListContainer = self.storyboard?.instantiateViewControllerWithIdentifier("ListaEventosHomeScreen") as! EventListViewController
            UIView.transition(with: self.subAuxView, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromLeft, animations: { () -> Void in
                self.subAuxView.addSubview(self.eventListContainerView)
                
                }, completion: nil)
            self.navigationBarMapButton.image = UIImage(named: "ic_mapa")
            self.subAuxView.reloadInputViews()
            
        } else {
            
            self.eventListContainerView.isHidden = true
            UIView.transition(with: self.subAuxView, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromRight, animations: { () -> Void in
                
                self.subAuxView.addSubview(self.mapView)
                
                }, completion: nil)
            self.navigationBarMapButton.image = UIImage(named: "ic_list")
            self.subAuxView.reloadInputViews()
        }
    }
    
    func sortStoreByDistance(_ arrayEvents: [NSDictionary]) -> [NSDictionary]?{
        var auxArrayEvents = [NSDictionary]()
        var auxArrayDistance = [Double]()
        for event in arrayEvents {
            let eventLatitude = (event["eventLocation"] as! CLLocation).coordinate.latitude
            let eventLongitude = (event["eventLocation"] as! CLLocation).coordinate.longitude
            
            let userLatitude = usersLocation!.coordinate.latitude
            let userLongitude = usersLocation!.coordinate.longitude
            
            //Euclidian Distance between points
            
            let euclidianDistance = self.measureDistance(userLatitude, lon1: userLongitude, lat2: eventLatitude, lon2: eventLongitude)
            let distance = euclidianDistance
            event.setValue(distance, forKey: "distance") //distance
            print(distance)
            auxArrayEvents.append(event)
            auxArrayDistance.append(distance)
        }
        //arrayLojas.removeAll()
        var returnedOrder = [NSDictionary]()
        var i = auxArrayDistance.count
        while i > 0 {
            let minDistance = auxArrayDistance.min()
            let auxIndex: Int = auxArrayDistance.index(of: minDistance!)!
            auxArrayDistance.remove(at: auxIndex)
            
            for event in auxArrayEvents {
                if event["distance"] as? CLLocationDegrees == minDistance {
                    returnedOrder.append(event)
                }
            }
            i -= 1
        }
        
        return returnedOrder
    }
    
    func measureDistance(_ lat1: Double, lon1: Double, lat2: Double, lon2: Double) -> Double{  // generally used geo measurement function
        let R = 6378.137; // Radius of earth in KM
        let dLat = (lat2 - lat1) * 3.14 / 180;
        let dLon = (lon2 - lon1) * 3.14 / 180;
        let aux = sin(dLat/2) * sin(dLat/2)
        let aux1 = cos(lat1 * 3.14 / 180) * cos(lat2 * 3.14 / 180) * sin(dLon/2) * sin(dLon/2);
        let a = aux + aux1
        let c = 2 * atan2(sqrt(a), sqrt(1-a));
        let d = R * c;
        return d * 1000; // meters
    }
    
     //   NSNotificationCenter.defaultCenter().postNotificationName("ExibirLojas", object: nil, userInfo:
    
}
