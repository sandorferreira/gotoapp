//
//  CustomOverlayView.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 06/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

class CustomOverlayView: UIView {
    
    var delegate : CustomOverlayDelegate! = nil
    
    @IBOutlet weak var imgrectview: UIView!
    @IBOutlet weak var gotoTopBar: UIView!
    @IBOutlet weak var flashBtn: UIButton!
    
    @IBAction func shootButton(_ sender: AnyObject) {
        delegate.didShoot(self)
    }
    
    @IBAction func cancelButton(_ sender: AnyObject) {
        delegate.didCancel(self)
    }
    
    @IBAction func rotateButton(_ sender: AnyObject) {
        delegate.didRotate(self)
    }
    
    @IBAction func turnFlash(_ sender: AnyObject) {
        delegate.didTurnFlash(self)
    }
    
}

protocol CustomOverlayDelegate {
    func didCancel(_ overlayView: CustomOverlayView)
    func didShoot(_ overlayView: CustomOverlayView)
    func didRotate(_ overlayView: CustomOverlayView)
    func didTurnFlash(_ overlayView: CustomOverlayView)
}
