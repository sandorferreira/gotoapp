//
//  PurchaseTicketViewController.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 09/06/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

class PurchaseTicketViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    var link: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let ticketNSURL = URL(string: link!)! //URL(string: link!)
        let request = URLRequest(url: ticketNSURL)
        webView.loadRequest(request)
    }
}
