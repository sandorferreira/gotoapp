//
//  Evento.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 08/06/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

class Evento {
    var eventId: String?
    var creatorId: String?
    var name: String?
    var desc: String?
    var image: String?
    var linkPurchase: String?
    var address: String?
    var startDate: String?
    var endDate: String?
    var usersTokens: [String]?
    var friendsTokens: [String]?
    var photosArray: [String]?
    var customBackgroundColor: String? // Only Premium Users
    var type: String?
    var location: CLLocation?
    //var funPoint: FunPoint
    
//    override class func collectionName() -> String {
//        return "Events"
//    }
    
    
    init(name: String, creatorId: String,desc: String, image: String, linkPurchase: String, startDate: String, endDate:String, customBackgroundColor: String, type: String, address: String) {
        //super.init()
        self.name = name
        self.creatorId = creatorId
        self.desc = desc
        self.image = image
        self.linkPurchase = linkPurchase
        self.startDate = startDate
        self.endDate = endDate
        self.customBackgroundColor = customBackgroundColor
        self.type = type
        self.address = address
    }
//    
//    required init(realm: RLMRealm, schema: RLMObjectSchema) {
//        super.init(realm: realm, schema: schema)
//        //fatalError("init(realm:schema:) has not been implemented")
//    }
//    
//    required init?(map: Map) {
//        super.init(map: map)
//        //fatalError("init(map:) has not been implemented")
//    }
//    
//    required init() {
//        super.init()
//        //fatalError("init() has not been implemented")
//    }
//    
//    required init(value: Any, schema: RLMSchema) {
//        super.init(value: value, schema: schema)
//        //fatalError("init(value:schema:) has not been implemented")
//    }
//
//    override func propertyMapping(_ map: Map) {
//        super.propertyMapping(map)
//        
//        creatorId <- ("creatorId", map["creatorId"])
//        name <- ("name", map["name"])
//        startDate <- ("startDate", map["startDate"], ISO8601DateTransform())
//        endDate <- ("endDate", map["endDate"], ISO8601DateTransform())
//        desc <- ("desc", map["desc"])
//        image <- ("image", map["image"])
//        linkPurchase <- ("linkPurchase", map["linkPurchase"])
//        usersTokens <- ("usersTokens", map["usersTokens"])
//        friendsTokens <- ("friendsTokens", map["friendsTokens"])
//        customBackgroundColor <- ("customBackgroundColor", map["customBackgroundColor"])
//        address <- ("address", map["address"])
//        photosArray <- ("photosArray", map["photosArray"])
//    }
//    
//    
//    
//    internal override func hostToKinveyPropertyMapping() -> [AnyHashable: Any]! {
//        return [
//            "entityId": KCSEntityKeyId,
//            "creatorId": "creatorId",
//            "name": "name",
//            "startDate": "startDate",
//            "endDate" : "endDate",
//            "desc":"desc",
//            "image": "image",
//            "linkPurchase":"linkPurchase",
//            "usersTokens": "usersTokens",
//            "friendsTokens":"friendsTokens",
//            "customBackgroundColor":"customBackgroundColor",
//            "type":"type",
//            "address" : "address",
//            "photosArray" : "photosArray"
//            ]
//    }
    
//    internal override static func kinveyPropertyToCollectionMapping() -> [NSObject : AnyObject]! {
//        return ["usersTokens" : KCSUserCollectionName,
//                "friendsTokens" : KCSUserCollectionName]
//    }

}
