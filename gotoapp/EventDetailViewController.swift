//
//  EventDetailViewController.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 29/06/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit
import CountdownLabel
import ImageSlideshow
import FirebaseDatabase
import FirebaseAuth

class EventDetailViewController: UIViewController, CountdownLabelDelegate/*, CustomOverlayDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate*/ {
    
    @IBOutlet weak var slideShow: ImageSlideshow!
    @IBOutlet weak var comecaLabel: UILabel!
    @IBOutlet weak var eventCountDown: CountdownLabel!
    @IBOutlet weak var friendsLabel: UILabel!
    @IBOutlet weak var peopleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var purchaseButton: UIButton!
    
    @IBOutlet weak var contentViewAreyouThere: UIView!
    @IBOutlet weak var yesHereButton: UIButton!
    @IBOutlet weak var notHereButton: UIButton!
    @IBOutlet weak var contentViewAuxHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var goButtonAuxHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var noPhotoWarningLabel: UILabel!
    
    // friends
    
    @IBOutlet weak var firstFriendIV: UIImageView!
    @IBOutlet weak var secondFriendIV: UIImageView!
    @IBOutlet weak var thirdFriendIV: UIImageView!
    
    // -- aux Variables -- //
    var evento: Evento?
    var eventId: String?
    let refreshControl = UIRefreshControl()
    var transitionDelegate: ZoomAnimatedTransitioningDelegate?
    
//    var squareImgView: UIImageView!
//    var picker = UIImagePickerController()
//    var front = false
//    var refresher = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBarColorToTransparent()
        noPhotoWarningLabel.isHidden = true
        setupFriendsPreview()
        
        if isUserInsideRange && isInsideEvent {
            configureInsideUser()
        } else if isUserInsideRange {
            self.contentViewAreyouThere.isHidden = false
            self.contentViewAuxHeightConstraint.constant = 110
        }
        
        if !isInsideEvent {
        self.contentViewAreyouThere.isHidden = true
        self.contentViewAuxHeightConstraint.constant = 0
        } else {
            self.contentViewAreyouThere.isHidden = false
            self.contentViewAuxHeightConstraint.constant = 110
        }
        
        eventCountDown.countdownDelegate = self
        if self.evento != nil {
            setupLabels()
        } else if !(self.eventId!.isEmpty) {
            //self.printCoisa()
        }
        
        
        // -- Notifications -- //
        NotificationCenter.default.addObserver(self, selector: #selector(EventDetailViewController.enteredEventRegion(_:)), name: NSNotification.Name(rawValue: NotificationEstaNoEvento), object: nil)

    }
    
    func setupFriendsPreview() {
        firstFriendIV.layer.cornerRadius = 23 //firstFriendIV.frame.height / 2
        firstFriendIV.layer.borderColor = UIColor.white.cgColor
        firstFriendIV.layer.borderWidth = 2
        firstFriendIV.clipsToBounds = true
        //firstFriendIV.translatesAutoresizingMaskIntoConstraints = false
        
        secondFriendIV.layer.cornerRadius = 23 //secondFriendIV.frame.height / 2
        secondFriendIV.clipsToBounds = true
        secondFriendIV.layer.borderColor = UIColor.white.cgColor
        secondFriendIV.layer.borderWidth = 2
        //secondFriendIV.translatesAutoresizingMaskIntoConstraints = false
        
        thirdFriendIV.layer.cornerRadius = 23 //thirdFriendIV.frame.height / 2
        thirdFriendIV.clipsToBounds = true
        thirdFriendIV.layer.borderColor = UIColor.white.cgColor
        thirdFriendIV.layer.borderWidth = 2
        //thirdFriendIV.translatesAutoresizingMaskIntoConstraints = false
        
    }
    
    func configureInsideUser() {
        // colocar botao de tirar foto
        // setar ver todos os amigos + outros
        
        // esconder are you there
        self.contentViewAreyouThere.isHidden = true
        self.contentViewAuxHeightConstraint.constant = 0
    }
    
    func configureOutsideUser() {
        // tirar botao tirar foto
        // se o usuário estiver no range mostrar are you there
        // tirar função de ver todos
    }
    
    func setupNavigationBarColorToTransparent() {
        self.title = self.evento!.name!
        self.view.backgroundColor = UIColor(red: 148/255.0, green: 64/255.0, blue: 185/255.0, alpha: 1.0)
        let okButton = UIBarButtonItem(title: "OK", style: .done, target: self, action: #selector(EventDetailViewController.dismissEventDetail))
        self.navigationItem.rightBarButtonItem = okButton
        
        // Camera Button -- tirar quando nao estiver dentro do evento
        let cameraButton = UIBarButtonItem(image: UIImage(named: "ic_camera"), style: .plain, target: self, action: #selector(EventDetailViewController.takePicture))
        self.navigationItem.leftBarButtonItem = cameraButton
        
    }
    
    func setDesignables(event: Evento) {
        //eventDetailNavigationBar.topItem?.title = event.name
        
        // start Date
        let startDate = stringToDate(string: event.startDate!)
        eventCountDown.setCountDownDate(startDate)
        eventCountDown.start()
        
        // Description
        self.descriptionLabel.text = event.desc
        self.addressLabel.text = event.address
        
        // Photos 
        
        if let _ = event.photosArray {
            let pictures = getPhotosFromEvent(self.evento!)
            print(" ok ne \(pictures.count)")
            if pictures.count != 0 {
                var arrayImageSource = [ImageSource]()
                for picture in pictures {
                    let imageSource = ImageSource(image: picture)
                    arrayImageSource.append(imageSource)
                }
                slideShow.setImageInputs(arrayImageSource)
                self.slideShow.isHidden = false
                self.noPhotoWarningLabel.isHidden = true
            } else {
                //self.slideShow.isHidden = true
                self.noPhotoWarningLabel.isHidden = false
            }
        } else {
            self.noPhotoWarningLabel.isHidden = false
        }
    }
    
    func setupLabels() {
        let reference = ref.reference(withPath: "Events").child(eventId!)
        reference.observe(.value, with: { (snapshot) in
            let event = snapshot.value as! [String: AnyObject]
            let name = event["name"] as! String
            let address = event["address"] as! String
            let desc = event["desc"] as! String
            let image = event["image"] as! String
            let startDate = event["startDate"] as! String
            let endDate = event["endDate"] as! String
            let usersTokens = event["usersTokens"] as! [String]
            let creatorId = event["creatorId"] as! String
            let linkPurchase = event["linkPurchase"] as! String
            let auxEvent = Evento(name: name, creatorId: creatorId, desc: desc, image: image, linkPurchase: linkPurchase, startDate: startDate, endDate: endDate, customBackgroundColor: "", type: "", address: address)
            auxEvent.usersTokens = usersTokens
            // function to verify friends
            if let photosArray = event["photosArray"] as? [String] {
                auxEvent.photosArray = photosArray
            }
            self.setDesignables(event: auxEvent)
            
            // amigos
            returnFriendsAtEvent(usersIds: usersTokens, completion: { (friends) in
                if friends == nil {
                    // user counts == 1 or 0 
                    self.friendsLabel.text = "0 amigos estão aqui"
                    self.peopleLabel.text = "1 pessoa está aqui"
                } else {
                    if friends!.count == 1 {
                        self.friendsLabel.text = "1 amigo está aqui"
                        let firstFriend = friends!.first!
                        let picture = firstFriend["profile_picture"] as! String
                        self.firstFriendIV.image = returnProfilePicture(picture)
                    } else if friends!.count == 2 {
                        self.friendsLabel.text = "2 amigos estão aqui"
                        let firstFriend = friends!.first!
                        let picture1 = firstFriend["profile_picture"] as! String
                        self.firstFriendIV.image = returnProfilePicture(picture1)
                        
                        let secondFriend = friends![1]
                        let picture2 = secondFriend["profile_picture"] as! String
                        self.secondFriendIV.image = returnProfilePicture(picture2)
                        
                    } else {
                        self.friendsLabel.text = "\(friends!.count) amigos estão aqui"
                        let firstFriend = friends!.first!
                        let picture1 = firstFriend["profile_picture"] as! String
                        self.firstFriendIV.image = returnProfilePicture(picture1)
                        
                        let secondFriend = friends![1]
                        let picture2 = secondFriend["profile_picture"] as! String
                        self.secondFriendIV.image = returnProfilePicture(picture2)
                        
                        let thirdFriend = friends![2]
                        let picture3 = thirdFriend["profile_picture"] as! String
                        self.thirdFriendIV.image = returnProfilePicture(picture3)
                    }
                }
            })
        })
    }
    
    func enteredEventRegion(_ n: Notification) {
        let isInside = (n as NSNotification).userInfo!["inside"] as! Bool
        if isInside {
            self.contentViewAreyouThere.isHidden = false
        } else {
            self.contentViewAreyouThere.isHidden = true
        }
    }

    @IBAction func yesInsideEvent(_ sender: AnyObject) {
        let controllerInside = self.storyboard?.instantiateViewController(withIdentifier: "InsideEvent") as! InsideEventViewController
        controllerInside.evento = self.evento
        let presentingViewController = self.presentingViewController

        self.dismiss(animated: true) {
            presentingViewController?.present(controllerInside, animated: true, completion: {
//                Evento.checkUserInCurrentEvent(currentEvent!) { (success, event) in
//                    if success {
//                        NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationFuncaoDentroDoEvento), object: nil, userInfo: ["inside" : true, "event" : self.evento!])
//                        self.evento = event
//                    }
//                }
                
                //goToEvent = false
                clearAllLocalNotifications()
            })
            
        }
    }
    
    
    func setupInformation() {
        self.friendsLabel.text = "\(self.evento?.friendsTokens?.count) amigos estão aqui"
        self.peopleLabel.text = "\(self.evento?.usersTokens?.count) pessoas estão aqui"
        self.addressLabel.text = self.evento?.address
        self.descriptionLabel.text = self.evento?.desc
    }
    
    func openPurchaseLink() {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PurchaseWV") as! PurchaseTicketViewController
        controller.link = self.evento!.linkPurchase
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func countdownFinished() {
        print("finished")
        self.eventCountDown.isHidden = true
        self.comecaLabel.text = "Aberto"
        self.comecaLabel.textColor = UIColor.black
    }
    
    func dismissEventDetail() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func goToEvent(_ sender: AnyObject) {
    }
    
    @IBAction func yesInsideAction(_ sender: AnyObject) {
    }
    
    @IBAction func notInsideAction(_ sender: AnyObject) {
        let alert = UIAlertController(title: "Confirmar", message: "Você tem certeza que não está no evento? Marcando \"Não Estou\" você não poderá ver quem está dentro ou postar fotos no local", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Não Estou", style: .default) { (action) in
            self.contentViewAreyouThere.isHidden = true
            NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationFuncaoDentroDoEvento), object: nil, userInfo: ["inside" : false])
            //goToEvent = false
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        alert.addAction(yesAction)
        self.present(alert, animated: true) {
            clearAllLocalNotifications()
        }
    }
    
    // Inside Event -- Take Picture
    
    func takePicture() {
        
        let controllerCamera = self.storyboard?.instantiateViewController(withIdentifier: "cameraUIView") as! CameraGoToViewController
        self.present(controllerCamera, animated: true, completion: nil)
        
//        picker.delegate = self
//        let customViewController = CustomOverlayViewController(nibName: "CustomOverlayViewController", bundle: nil)
//        let customView: CustomOverlayView = customViewController.view as! CustomOverlayView
//        customView.delegate = self
//        self.squareImgView = UIImageView()
//        self.squareImgView.frame = customView.imgrectview.frame
//        customView.frame = picker.view.frame
//        customView.flashBtn.isHidden = true
//        picker.sourceType = .camera
//        picker.cameraDevice = .front
//        self.front = true
//        picker.allowsEditing = false
//        picker.showsCameraControls = false
//        picker.cameraCaptureMode = .photo
//        
//        // --  adicionando overlay -- /
//        var f = picker.view.bounds
//        f.size.height -= picker.navigationBar.bounds.size.height
//        let barHeight = (f.size.height - f.size.width) / 2
//        UIGraphicsBeginImageContext(f.size)
//        UIColor.gray.withAlphaComponent(0.5).setFill()
//        UIRectFillUsingBlendMode(CGRect(x: 0, y: 0, width: f.size.width, height: barHeight), .normal)
//        UIRectFillUsingBlendMode(CGRect(x: 0, y: f.size.height - barHeight, width: f.size.width, height: barHeight), .normal)
//        let overlayImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        
//        let overlayIV = UIImageView(frame: f)
//        overlayIV.image = overlayImage
//        //customView.addSubview(overlayIV)
//        //picker.cameraOverlayView?.addSubview(overlayIV)
//        
//        self.present(picker, animated: true, completion: {
//            self.picker.cameraOverlayView = customView
//            UIApplication.shared.isStatusBarHidden = true
//            //self.picker.showsCameraControls = true
//        })
    }
//    
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
//        picker.dismiss(animated: true) {
//            let controllerPresentedPicture = self.storyboard?.instantiateViewController(withIdentifier: "PictureTakenPreview") as! SharePhotoViewController
//            // squaring photo //
//            let imageSize = image.size
//            let width = imageSize.width
//            let height = imageSize.height
//            
//            if (width != height) {
//                let newDimension = min(width, height)
//                let widthOffset = (width - newDimension) / 2
//                let heightOffset = (height - newDimension) / 2
//                UIGraphicsBeginImageContextWithOptions(CGSize(width: newDimension, height: newDimension), false, 0)
//                image.draw(at: CGPoint(x: -widthOffset, y: -heightOffset), blendMode: .copy, alpha: 1)
//                let croppedImage = UIGraphicsGetImageFromCurrentImageContext()
//                UIGraphicsEndImageContext()
//                
//                controllerPresentedPicture.pictureTaken = croppedImage
//                controllerPresentedPicture.event = self.evento
//                controllerPresentedPicture.modalPresentationStyle = .overCurrentContext
//                self.present(controllerPresentedPicture, animated: true, completion: nil)
//            }
//        }
//    }
//    func didCancel(_ overlayView:CustomOverlayView) {
//        picker.dismiss(animated: true, completion: nil)
//    }
//    func didShoot(_ overlayView:CustomOverlayView) {
//        picker.takePicture()
//    }
//    func didRotate(_ overlayView: CustomOverlayView) {
//        if self.front {
//            self.front = false
//            picker.cameraDevice = .rear
//            overlayView.flashBtn.isHidden = false
//        } else {
//            self.front = true
//            picker.cameraDevice = .front
//            overlayView.flashBtn.isHidden = true
//        }
//    }
//    
//    func didTurnFlash(_ overlayView: CustomOverlayView) {
//        if picker.cameraFlashMode == .off {
//            picker.cameraFlashMode = .on
//            overlayView.flashBtn.setImage(UIImage(named: "bt_flash_on"), for: .normal)
//        } else if picker.cameraFlashMode == .on {
//            picker.cameraFlashMode = .off
//            overlayView.flashBtn.setImage(UIImage(named: "bt_flash_off"), for: .normal)
//        }
//    }
}
