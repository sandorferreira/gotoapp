//
//  InsideEventViewController.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 04/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit
import ImageSlideshow
import Social
import AVFoundation
import ImageIO

class InsideEventViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CustomOverlayDelegate {
    
    // -- outlets from storyboard -- //
    
    @IBOutlet weak var eventNavigationBar: UINavigationBar!
    @IBOutlet weak var slideShow: ImageSlideshow!
    @IBOutlet weak var takePictureBtn: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var peopleLabel: UILabel!
    @IBOutlet weak var friendsLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var evento: Evento?
    var squareImgView: UIImageView!
    var picker = UIImagePickerController()
    var front = false
    var refresher = UIRefreshControl()
    
    var friendsDictionary: [NSDictionary]?
    var usersDictionary: [NSDictionary]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.evento != nil {
            setupInfo()
        }
        
        let tap_takePicture = UITapGestureRecognizer(target: self, action: #selector(InsideEventViewController.takePicture(_:)))
        self.takePictureBtn.addGestureRecognizer(tap_takePicture)
        refresher.addTarget(self, action: #selector(InsideEventViewController.refreshEvent), for: .valueChanged)
        self.scrollView.addSubview(refresher)
        
//        Evento.checkUserInCurrentEvent(evento!) { (success, event) in
//            var isInside = success
//            while !isInside {
//                Evento.checkUserInCurrentEvent(self.evento!, completionHandler: { (success, event) in
//                    isInside = success
//                })
//            }
//        }
    }
    
    func refreshEvent() {
//        Evento.reloadEvent(self.evento!.entityId!, completionHandler: { (success, event) in
//            if !success && event == nil {
//                print("erro ao refresh")
//                self.refresher.endRefreshing()
//            } else {
//                print("refreesh")
//                self.evento = event
//                self.setupInfo()
//                self.refresher.endRefreshing()
//            }
//        })
    }
    
    func openUsersTableView() {
        let controllerPeople = self.storyboard?.instantiateViewController(withIdentifier: "TableViewUsers") as! InsideEventViewController
        controllerPeople.usersDictionary = usersDictionary
        controllerPeople.modalPresentationStyle = .popover
        controllerPeople.modalPresentationStyle = .overCurrentContext
        
        self.present(controllerPeople, animated: true, completion: nil)
    }
    
    func setupInfo() {
//        Evento.returnUserCheckedIn(self.evento!) { (friends, users) in
//            if friends == nil || friends!.count == 0 {
//                self.friendsLabel.text = "Nenhum amigo no evento"
//            } else {
//                self.friendsDictionary = friends
//                if friends!.count == 1 {
//                    self.friendsLabel.text = "1 amigo está aqui"
//                } else {
//                    self.friendsLabel.text = "\(friends!.count) amigos estão aqui"
//                }
//            }
//            if users == nil || users!.count == 0 {
//                self.peopleLabel.text = "Ainda nenhum check-in"
//            } else {
//                self.usersDictionary = users
//                if users!.count == 1 {
//                    self.peopleLabel.text = "1 pessoa está aqui"
//                } else {
//                    self.peopleLabel.text = "\(users!.count) pessoas estão aqui"
//                }
//            }
//        }

        self.descriptionLabel.text = evento!.desc
//        let pictures = Evento.getPhotosFromEvent(self.evento!)
//        print(pictures.count)
//        if pictures.count != 0 {
//            var arrayImageSource = [ImageSource]()
//            for picture in pictures {
//                let imageSource = ImageSource(image: picture)
//                arrayImageSource.append(imageSource)
//            }
//            slideShow.setImageInputs(arrayImageSource)
//        } else {
//            self.slideShow.isHidden = true
//        }
    }
    
    @IBAction func dismiss(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func takePicture(_ r: UITapGestureRecognizer) {
        picker.delegate = self
        let customViewController = CustomOverlayViewController(nibName: "CustomOverlayViewController", bundle: nil)
        let customView: CustomOverlayView = customViewController.view as! CustomOverlayView
        customView.delegate = self
        self.squareImgView = UIImageView()
        self.squareImgView.frame = customView.imgrectview.frame
        customView.frame = picker.view.frame
        customView.flashBtn.isHidden = true
        picker.sourceType = .camera
        picker.cameraDevice = .front
        self.front = true
        picker.allowsEditing = false
        picker.showsCameraControls = false
        picker.cameraCaptureMode = .photo
        
        // --  adicionando overlay -- /
        var f = picker.view.bounds
        f.size.height -= picker.navigationBar.bounds.size.height
        let barHeight = (f.size.height - f.size.width) / 2
        UIGraphicsBeginImageContext(f.size)
        UIColor.gray.withAlphaComponent(0.5).setFill()
        UIRectFillUsingBlendMode(CGRect(x: 0, y: 0, width: f.size.width, height: barHeight), .normal)
        UIRectFillUsingBlendMode(CGRect(x: 0, y: f.size.height - barHeight, width: f.size.width, height: barHeight), .normal)
        let overlayImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let overlayIV = UIImageView(frame: f)
        overlayIV.image = overlayImage
        //customView.addSubview(overlayIV)
        //picker.cameraOverlayView?.addSubview(overlayIV)
        
        self.present(picker, animated: true, completion: {
            self.picker.cameraOverlayView = customView
            //self.picker.showsCameraControls = true
        })
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        picker.dismiss(animated: true) { 
            let controllerPresentedPicture = self.storyboard?.instantiateViewController(withIdentifier: "PictureTakenPreview") as! SharePhotoViewController
            // squaring photo //
            let imageSize = image.size
            let width = imageSize.width
            let height = imageSize.height
            
            if (width != height) {
                let newDimension = min(width, height)
                let widthOffset = (width - newDimension) / 2
                let heightOffset = (height - newDimension) / 2
                UIGraphicsBeginImageContextWithOptions(CGSize(width: newDimension, height: newDimension), false, 0)
                image.draw(at: CGPoint(x: -widthOffset, y: -heightOffset), blendMode: .copy, alpha: 1)
                let croppedImage = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                controllerPresentedPicture.pictureTaken = croppedImage
                controllerPresentedPicture.event = self.evento
                controllerPresentedPicture.modalPresentationStyle = .overCurrentContext
                self.present(controllerPresentedPicture, animated: true, completion: nil)
            }
        }
    }
    func didCancel(_ overlayView:CustomOverlayView) {
        picker.dismiss(animated: true, completion: nil)
    }
    func didShoot(_ overlayView:CustomOverlayView) {
        picker.takePicture()
    }
    func didRotate(_ overlayView: CustomOverlayView) {
        if self.front {
            self.front = false
            picker.cameraDevice = .rear
            overlayView.flashBtn.isHidden = false
        } else {
            self.front = true
            picker.cameraDevice = .front
            overlayView.flashBtn.isHidden = true
        }
    }
    
    func didTurnFlash(_ overlayView: CustomOverlayView) {
        if picker.cameraFlashMode == .off {
            picker.cameraFlashMode = .on
            overlayView.flashBtn.setImage(UIImage(named: "bt_flash_on"), for: .normal)
        } else {
            picker.cameraFlashMode = .off
            overlayView.flashBtn.setImage(UIImage(named: "bt_flash_off"), for: .normal)
        }
    }
    
    func cropToBounds(_ image: UIImage, width: Double, height: Double) -> UIImage {
        
        let contextImage: UIImage = UIImage(cgImage: image.cgImage!)
        
        let contextSize: CGSize = contextImage.size
        
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRect(x: 0, y: 0, width: cgwidth, height: cgheight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImage = contextImage.cgImage!.cropping(to: rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }
    
    @IBAction func seeUsersCheckedIn(_ sender: AnyObject) {
        print("vendo todos")
        let controllerTable = self.storyboard?.instantiateViewController(withIdentifier: "TableViewUsers") as! KCSUserEventTableView
        controllerTable.users = self.usersDictionary
        controllerTable.friends = self.friendsDictionary
        let navigationController = UINavigationController(rootViewController: controllerTable)
        navigationController.isNavigationBarHidden = true
        navigationController.modalPresentationStyle = .overCurrentContext
        navigationController.modalTransitionStyle = .crossDissolve
        self.present(navigationController, animated: true, completion: {
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.view.addSubview(blurEffectView)
        })
    }
    
    
}
