//
//  ConstantHelpers.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 08/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase
//import Kinvey
//import KeychainAccess
//import Realm
//import RealmSwift
//import PromiseKit

// Firebase Supported References //

let ref = FIRDatabase.database()

// Notifications //
let NotificationEstaNoEvento = "EstaNoEvento"
let NotificationFuncaoDentroDoEvento = "ChangeToInsideEvent"
let EsconderBarra = "EsconderTabBar"

// Color //

let gotoColor = UIColor(red: 123.0/255.0, green: 66.0/255.0, blue: 146.0/255.0, alpha: 1.0)
let gotoColorPink = UIColor(red: 251.0/255.0, green: 48.0/255.0, blue: 142.0/255.0, alpha: 1.0)
let gotoDarkGray = UIColor(red: 61.0/255.0, green: 61.0/255.0, blue: 61.0/255.0, alpha: 1.0)

let facebookColor = UIColor(red: 59.0/255.0, green: 89.0/255.0, blue: 152.0/255.0, alpha: 1.0)
let twitterColor = UIColor(red: 64.0/255.0, green: 153.0/255.0, blue: 1.0, alpha: 1.0)
let shareGray = UIColor(red: 216.0/255.0, green: 216.0/255.0, blue: 216.0/255.0, alpha: 1.0)
