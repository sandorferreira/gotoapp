//
//  DiscoverViewController.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 18/08/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

class DiscoverViewController: UIViewController {
    
    @IBOutlet weak var backgroundView: UIImageView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // aux variable
    var isTabBarPresented: Bool?
    var events: [Evento]?
    var posts: [DiscoverPost]?
    var refresh = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isTabBarPresented = false
        UIApplication.shared.isStatusBarHidden = true
        collectionView.delegate = self
        collectionView.dataSource = self
    
        posts = [DiscoverPost]()
//        self.manager.startManagingWithDelegate(self)
//        self.manager.registerCellClass(DiscoverCell)
//        
//        self.manager.whenSelected(DiscoverCell.self) { (postCell, post, indePath) in
//            self.hideTabBar()
//        }
        
        //getfirstTenAleatoryImages()
        //refresh.addTarget(self, action: #selector(DiscoverViewController.getfirstTenAleatoryImages), for: .valueChanged)
        //self.collectionView!.addSubview(refresh)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        activityIndicator.startAnimating()
        //getfirstTenAleatoryImages()
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isStatusBarHidden = false
    }
    
    fileprivate struct Storyboard {
        static let CellIdentifier  = "DiscoverCell"
    }
    
//    func getfirstTenAleatoryImages() {
//        //let query = KCSQuery()
//        //let modifier = KCSQueryLimitModifier(limit: 10)
//        ///query.limitModifer = modifier
//        
//        
//        storeEvents?.query(withQuery: query, withCompletionBlock: { (results, error) in
//            if error != nil || results == nil || results?.count == 0 {
//                //completion(false, newEvents: nil)
//                Alert.showAlertWithMessage("Ops!", withMessage: "Ocorreu um erro ao buscar os eventos. Por favor, tente mais tarde.", actions: nil, atController: self)
//                self.activityIndicator.stopAnimating()
//                let tap_showTabBar = UITapGestureRecognizer(target: self, action: #selector(DiscoverViewController.hideTabBar))
//                self.view.addGestureRecognizer(tap_showTabBar)
//            } else {
//                for event in results as! [Evento] {
//                    if !(self.checkAlreadyhasEvent(event)) {
//                        self.events?.append(event)
//                        if event.photosArray != nil && event.photosArray?.count != 0 {
//                            let discoverPost = DiscoverPost(photoString: event.photosArray!.first!, localName: "", cityName: "Vitoria", eventName: event.name!)
//                            self.posts!.append(discoverPost)
//                            //self.manager.memoryStorage.addItems(self.posts!)
//                            self.activityIndicator.stopAnimating()
//                            self.collectionView!.reloadData()
//                        } else {
//                            Alert.showAlertWithMessage("Ops!", withMessage: "Ocorreu um erro ao buscar os eventos. Por favor, tente mais tarde.", actions: nil, atController: self)
//                            let tap_showTabBar = UITapGestureRecognizer(target: self, action: #selector(DiscoverViewController.hideTabBar))
//                            self.view.addGestureRecognizer(tap_showTabBar)
//                            self.activityIndicator.stopAnimating()
//                        }
//                        //let discoverPost = DiscoverPost(
//                    } else {
//                        print("mas ue??")
//                    }
//                }
////                if !(self.checkNilPhotos()) {
////                    print("check photos")
////                    Alert.showAlertWithMessage("Ops!", withMessage: "Ocorreu um erro ao buscar os eventos. Por favor, tente mais tarde.", actions: nil, atController: self)
////                    let tap_showTabBar = UITapGestureRecognizer(target: self, action: #selector(DiscoverViewController.hideTabBar))
////                    self.view.addGestureRecognizer(tap_showTabBar)
////                    self.activityIndicator.stopAnimating()
////                } else {
////                    self.activityIndicator.stopAnimating()
////                    self.collectionView!.reloadData()
////                }
//            }
//        }, withProgressBlock: nil)
//    }
    
    func checkNilPhotos() -> Bool{
        if events == nil {
            return false
        }
        
        for aux in events! {
            if aux.photosArray != nil || aux.photosArray?.count != 0 {
                return true
            }
        }
        
        return false
    }
    
    func checkAlreadyhasEvent(_ event: Evento) -> Bool {
        
        if events == nil {
            return false
        }
        
        for aux in self.events! {
            return true
//            if aux.entityId == event.entityId {
//                return true
//            }
        }
        
        return false
    }
    
    func hideTabBar() {
        if self.isTabBarPresented == false {
            NotificationCenter.default.post(name: Notification.Name(rawValue: EsconderBarra), object: nil, userInfo: ["hide" : false])
            self.isTabBarPresented = true
        } else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: EsconderBarra), object: nil, userInfo: ["hide" : true])
            self.isTabBarPresented = false
        }
    }

}

extension DiscoverViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return events?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let event = self.events![indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Storyboard.CellIdentifier, for: indexPath) as! DiscoverCell
        
        cell.photo.image =  returnPhotoFromBase64EncodedString(event.photosArray!.first!)
        cell.eventNameLabel.text = "Evento muito bacana elgal"
        cell.localName.text = "Aquizinha mesmo na parada"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        self.backgroundView.image = UIImage(named: "bg_discover")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        collectionView.cellForItem(at: indexPath)?.layoutMargins = UIEdgeInsets.zero
        return CGSize(width: collectionView.frame.size.width ,height: collectionView.frame.size.height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        hideTabBar()
        
        //collectionView.scrollIndicatorInsets = UIEdgeInsetsZero
//        if self.isTabBarPresented == false {
//            NSNotificationCenter.defaultCenter().postNotificationName(EsconderBarra, object: nil, userInfo: ["hide" : false])
//            self.isTabBarPresented = true
//        } else {
//            NSNotificationCenter.defaultCenter().postNotificationName(EsconderBarra, object: nil, userInfo: ["hide" : true])
//            self.isTabBarPresented = false
//        }
        //collectionView.layoutMargins = UIEdgeInsetsZero
        //collectionView.cellForItemAtIndexPath(indexPath)?.layoutMargins = UIEdgeInsetsZero
        //self.view.layoutIfNeeded()
    }
    
    
}

extension DiscoverViewController: UIScrollViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        //let layout = self.collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWithIncludingSpacing = collectionView!.frame.width //layout.itemSize.width - layout.minimumLineSpacing
        var offset = targetContentOffset.pointee
        
        let index = (offset.x + scrollView.contentInset.left) / cellWithIncludingSpacing
        let roundedIndex = round(index)
        offset = CGPoint(x: roundedIndex * cellWithIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
        targetContentOffset.pointee = offset
    }
}




