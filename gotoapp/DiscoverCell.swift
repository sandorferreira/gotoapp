//
//  DiscoverCell.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 18/08/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit
//import DTCollectionViewManager
//import DTModelStorage

class DiscoverCell: UICollectionViewCell {
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var eventNameLabel: UILabel!
    @IBOutlet weak var localName: UILabel!
    
    
    override func layoutMarginsDidChange() {
        self.layoutMargins = UIEdgeInsets.zero
        self.preservesSuperviewLayoutMargins = false
    }
    
    func updateWithModel(_ model: DiscoverPost) {
        photo.image = returnPhotoFromBase64EncodedString(model.photoString)
        eventNameLabel.text = model.eventName
        localName.text = "\(model.localName), \(model.cityName)"
    }
}
