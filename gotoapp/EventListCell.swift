//
//  EventListCell.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 27/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

class EventListCell: UITableViewCell {
    
    @IBOutlet weak var distanceLabel: UILabel!
  
    @IBOutlet weak var evnetNameLabel: UILabel!
    
    @IBOutlet weak var eventImage: UIImageView!
}
