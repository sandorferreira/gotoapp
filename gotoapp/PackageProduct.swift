//
//  PackageProduct.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 15/08/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import Foundation

public struct PackageProduct {
    
    fileprivate static let Prefix = "com.gotoapp.goto."
    
    public static let FunPackage = Prefix + "FunPackageFiveEvents"
    
    fileprivate static let productIdentifiers: Set = [PackageProduct.FunPackage]
}
