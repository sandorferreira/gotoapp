//
//  PreviewViewController.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 08/06/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit
import ImageSlideshow
import NVActivityIndicatorView
import FirebaseDatabase
import CountdownLabel

class PreviewViewController: UIViewController, CountdownLabelDelegate {
    
    // -- Outlets From Storyboard -- //
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var slideShow: ImageSlideshow!
    
    
    @IBOutlet weak var previewCountdown: CountdownLabel!
    
    @IBOutlet weak var comecaLabel: UILabel!
    
    // friends imageviews
    
    @IBOutlet weak var firstFriendIV: UIImageView!
    @IBOutlet weak var secondFriendIV: UIImageView!
    @IBOutlet weak var thirdFriendIV: UIImageView!
    
    // friends and people labels
    
    @IBOutlet weak var friendsLabel: UILabel!
    @IBOutlet weak var peopleLabel: UILabel!
    
    // event info
    
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var purchaseButton: UIButton!
    
    // event image
    
    @IBOutlet weak var eventNameImage: UILabel!
    @IBOutlet weak var eventImage: UIImageView!
    
    // -- Aux Variables -- //
    
    var evento: Evento?
    var eventDic: NSDictionary?
    var funPoint: FunPoint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        previewCountdown.countdownDelegate = self
        previewCountdown.setCountDownDate(stringToDate(string: evento!.startDate!))
        previewCountdown.start()
        // -- setting up Navigation bar -- //
        setupNavigationBarWithEventName()
        setupFriendsPreview()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        // -- setting up delegates -- //
        //self.view.backgroundColor = getBackgroundColor((evento?.customBackgroundColor)!)
        slideShow.setImageInputs([ImageSource(image: UIImage(named: "foto1")!),ImageSource(image: UIImage(named: "foto2")!),ImageSource(image: UIImage(named: "foto3")!),ImageSource(image: UIImage(named: "foto4")!),ImageSource(image: UIImage(named: "foto5")!)])
        slideShow.slideshowInterval = 6.0
        slideShow.contentScaleMode = .scaleAspectFill
        
        descriptionLabel.text = evento?.desc
        
        addressLabel.text = evento?.address
        eventNameImage.text = evento?.name
        eventImage.image = returnPhotoFromBase64EncodedString(evento!.image!)
    
        // -- Keyboard Setup -- //
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(PreviewViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PreviewViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    func setupNavigationBarWithEventName() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = evento?.name
        self.navigationController?.navigationBar.titleTextAttributes = ([NSFontAttributeName: UIFont(name: "Helvetica Neue", size: 17)!,
            NSForegroundColorAttributeName: UIColor.white])
        
        if evento?.linkPurchase == "" {
            purchaseButton.isHidden = true
        }
    }
    
    func setupFriendsPreview() {
        firstFriendIV.layer.cornerRadius = 23 //firstFriendIV.frame.height / 2
        firstFriendIV.layer.borderColor = UIColor.white.cgColor
        firstFriendIV.layer.borderWidth = 2
        firstFriendIV.clipsToBounds = true
        //firstFriendIV.translatesAutoresizingMaskIntoConstraints = false
        
        secondFriendIV.layer.cornerRadius = 23 //secondFriendIV.frame.height / 2
        secondFriendIV.clipsToBounds = true
        secondFriendIV.layer.borderColor = UIColor.white.cgColor
        secondFriendIV.layer.borderWidth = 2
        //secondFriendIV.translatesAutoresizingMaskIntoConstraints = false
        
        thirdFriendIV.layer.cornerRadius = 23 //thirdFriendIV.frame.height / 2
        thirdFriendIV.clipsToBounds = true
        thirdFriendIV.layer.borderColor = UIColor.white.cgColor
        thirdFriendIV.layer.borderWidth = 2
        //thirdFriendIV.translatesAutoresizingMaskIntoConstraints = false
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style: .plain, target:nil, action:nil)
    }
    
    func openLinkPurchaseWebView(_ r: UITapGestureRecognizer) {
//        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PurchaseWV") as! PurchaseTicketViewController
//        controller.link = evento?.linkPurchase
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func keyboardWillShow(_ notification:Notification){
        
        var userInfo = (notification as NSNotification).userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
    }
    
    func keyboardWillHide(_ notification:Notification){
        let contentInset: UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        scrollView.endEditing(true)
    }
    
    func saveEventToTheBackend(_ r: UITapGestureRecognizer) {
//        UIApplication.shared.beginIgnoringInteractionEvents()
//        
//        let activitySize = CGSize(width: 30.0, height: 30.0)
//        let activityFrame = CGRect(origin: self.view.center, size: activitySize)
//        let activityIndicator = NVActivityIndicatorView(frame: activityFrame, type: .ballScaleRipple, color: UIColor.black, padding: 2.0)
//        activityIndicator.center = self.view.center
//        self.view.alpha = 0.5
//        self.view.addSubview(activityIndicator)
//
//        activityIndicator.startAnimating()
//        let key = ref.reference(withPath: "Events").childByAutoId().key //.setValue(eventDic!)
//        ref.reference(withPath: "Events").child(key).setValue(eventDic!) { (error, reference) in
//            if error != nil {
//                UIApplication.shared.endIgnoringInteractionEvents()
//                activityIndicator.stopAnimating()
//                Alert.showAlertWithMessage("Erro", withMessage: "Ocorreu um erro ao salvar o evento no banco de dados. Nenhum valor foi cobrado.", actions: nil, atController: self)
//            } else {
//                UIApplication.shared.endIgnoringInteractionEvents()
//                activityIndicator.stopAnimating()
//                // fechar janela
//                print("Successfully saved to background")
//            }
//        }
//        let geofireRef = FIRDatabase.database().reference(withPath: "Events").child(key)
//        let geoFire = GeoFire(firebaseRef: geofireRef)
//        geoFire?.setLocation(self.evento?.location, forKey: "firebase-hq", withCompletionBlock: { (error) in
//            if error != nil {
//                UIApplication.shared.endIgnoringInteractionEvents()
//                activityIndicator.stopAnimating()
//                Alert.showAlertWithMessage("Erro", withMessage: "Ocorreu um erro ao salvar o evento no banco de dados. Nenhum valor foi cobrado. Tente novamente mais tarde", actions: nil, atController: self)
//                print("error while trying to set location \(error)")
//            }
//        })
        
//        UIApplication.shared.beginIgnoringInteractionEvents()
//        let activitySize = CGSize(width: 30.0, height: 30.0)
//        let activityFrame = CGRect(origin: self.view.center, size: activitySize)
//        let activityIndicator = NVActivityIndicatorView(frame: activityFrame, type: .ballScaleRipple, color: UIColor.black, padding: 2.0)
//        activityIndicator.center = self.view.center
//        self.view.alpha = 0.5
//        self.view.addSubview(activityIndicator)
//
//        
//        if  KCSUser.isUserPremium() {
//            Evento.getUserFunPoint({ (funPoint) in
//                if funPoint != nil {
//                    funPoint!.event = self.evento
//                    activityIndicator.startAnimating()
//                    storeFunPoint.save(funPoint!, completionHandler: { (funPoint, error) in
//                        if error != nil {
//                            print(error)
//                        } else {
//                            print("Successfully saved")
//                            self.view.alpha = 1.0
//                            activityIndicator.stopAnimating()
//                            UIApplication.shared.endIgnoringInteractionEvents()
//                            NotificationCenter.default.post(name: Notification.Name(rawValue: "QueryEvents"), object: nil, userInfo: ["funPoint" : self.funPoint!])
//                            NotificationCenter.default.post(name: Notification.Name(rawValue: "GoButtonEdit"), object: nil, userInfo: ["event" : self.funPoint!.event!])
//                            NotificationCenter.default.post(name: Notification.Name(rawValue: "CriarMarker"), object: nil)
//                            self.dismiss(animated: true, completion: nil)
//                        }
//                    })
//                    
////                    storeFunPoint?.save(funPoint, withCompletionBlock: { (objects, error) in
////                        if error != nil {
////                            print(error)
////                        } else {
////                            print("Successfully saved")
////                            self.view.alpha = 1.0
////                            activityIndicator.stopAnimation()
////                            UIApplication.shared.endIgnoringInteractionEvents()
////                            NotificationCenter.default.post(name: Notification.Name(rawValue: "QueryEvents"), object: nil, userInfo: ["funPoint" : self.funPoint!])
////                            NotificationCenter.default.post(name: Notification.Name(rawValue: "GoButtonEdit"), object: nil, userInfo: ["event" : self.funPoint!.event!])
////                            NotificationCenter.default.post(name: Notification.Name(rawValue: "CriarMarker"), object: nil)
////                            self.dismiss(animated: true, completion: nil)
////                        }
////                        
////                        }, withProgressBlock: nil)
//                } else {
//                    activityIndicator.startAnimating()
//                    storeFunPoint.save(self.funPoint!, completionHandler: { (funPoint, error) in
//                        if error != nil {
//                            print(error)
//                        } else {
//                            print("Successfully saved")
//                            self.view.alpha = 1.0
//                            activityIndicator.stopAnimating()
//                            UIApplication.shared.endIgnoringInteractionEvents()
//                            NotificationCenter.default.post(name: Notification.Name(rawValue: "QueryEvents"), object: nil, userInfo: ["funPoint" : self.funPoint!])
//                            NotificationCenter.default.post(name: Notification.Name(rawValue: "GoButtonEdit"), object: nil, userInfo: ["event" : self.funPoint!.event!])
//                            NotificationCenter.default.post(name: Notification.Name(rawValue: "CriarMarker"), object: nil)
//                            self.dismiss(animated: true, completion: nil)
//                        }
//                    })
//                    
////                    storeFunPoint?.save(self.funPoint, withCompletionBlock: { (objects, error) in
////                        if error != nil {
////                            print(error)
////                        } else {
////                            print("Successfully saved")
////                            self.view.alpha = 1.0
////                            activityIndicator.stopAnimation()
////                            UIApplication.shared.endIgnoringInteractionEvents()
//                            NotificationCenter.default.post(name: Notification.Name(rawValue: "QueryEvents"), object: nil, userInfo: ["funPoint" : self.funPoint!])
//                            NotificationCenter.default.post(name: Notification.Name(rawValue: "GoButtonEdit"), object: nil, userInfo: ["event" : self.funPoint!.event!])
//                            NotificationCenter.default.post(name: Notification.Name(rawValue: "CriarMarker"), object: nil)
//                            self.dismiss(animated: true, completion: nil)
////                        }
////                        
////                        }, withProgressBlock: nil)
//                }
//            })
//        } else {
//            activityIndicator.startAnimating() //.startAnimation()
//            storeFunPoint.save(self.funPoint!, completionHandler: { (funPoint, error) in
//                if error != nil {
//                    print(error)
//                } else {
//                    print("Successfully saved")
//                    self.view.alpha = 1.0
//                    activityIndicator.stopAnimating()
//                    UIApplication.shared.endIgnoringInteractionEvents()
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: "QueryEvents"), object: nil, userInfo: ["funPoint" : self.funPoint!])
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: "GoButtonEdit"), object: nil, userInfo: ["event" : self.funPoint!.event!])
//                    //NotificationCenter.default.post(name: Notification.Name(rawValue: "CriarMarker"), object: nil)
//                    self.dismiss(animated: true, completion: nil)
//                }
//            })
//            
////            storeFunPoint.save(self.funPoint, withCompletionBlock: { (objects, error) in
////                if error != nil {
////                    print(error)
////                } else {
////                    print("Successfully saved")
////                    self.view.alpha = 1.0
////                    activityIndicator.stopAnimation()
////                    UIApplication.shared.endIgnoringInteractionEvents()
////                    NotificationCenter.default.post(name: Notification.Name(rawValue: "QueryEvents"), object: nil, userInfo: ["funPoint" : self.funPoint!])
////                    NotificationCenter.default.post(name: Notification.Name(rawValue: "GoButtonEdit"), object: nil, userInfo: ["event" : self.funPoint!.event!])
////                    NotificationCenter.default.post(name: Notification.Name(rawValue: "CriarMarker"), object: nil)
////                    self.dismiss(animated: true, completion: nil)
////                }
////                
////                }, withProgressBlock: nil)
//        }
//
//    }
    
    
    }
    
    // functions outlets
    @IBAction func createEventFirebase(_ sender: AnyObject) {
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        let activitySize = CGSize(width: 30.0, height: 30.0)
        let activityFrame = CGRect(origin: self.view.center, size: activitySize)
        let activityIndicator = NVActivityIndicatorView(frame: activityFrame, type: .ballScaleRipple, color: UIColor.black, padding: 2.0)
        activityIndicator.center = self.view.center
        self.view.alpha = 0.5
        self.view.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
        let key = ref.reference(withPath: "Events").childByAutoId().key //.setValue(eventDic!)
        ref.reference(withPath: "Events").child(key).setValue(eventDic!) { (error, reference) in
            if error != nil {
                UIApplication.shared.endIgnoringInteractionEvents()
                activityIndicator.stopAnimating()
                Alert.showAlertWithMessage("Erro", withMessage: "Ocorreu um erro ao salvar o evento no banco de dados. Nenhum valor foi cobrado.", actions: nil, atController: self)
            } else {
                UIApplication.shared.endIgnoringInteractionEvents()
                activityIndicator.stopAnimating()
                // fechar janela
                print("Successfully saved to background")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "CriarMarker"), object: nil)
                self.dismiss(animated: true, completion: nil)
            }
        }
        let geofireRef = FIRDatabase.database().reference(withPath: "Locations") //.child(key)
        let geoFire = GeoFire(firebaseRef: geofireRef)
        geoFire?.setLocation(self.evento?.location, forKey: key, withCompletionBlock: { (error) in
            if error != nil {
                UIApplication.shared.endIgnoringInteractionEvents()
                activityIndicator.stopAnimating()
                Alert.showAlertWithMessage("Erro", withMessage: "Ocorreu um erro ao salvar o evento no banco de dados. Nenhum valor foi cobrado. Tente novamente mais tarde", actions: nil, atController: self)
                print("error while trying to set location \(error)")
            }
        })
    }
    
    @IBAction func purchaseButton(_ sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PurchaseWV") as! PurchaseTicketViewController
        controller.link = evento?.linkPurchase
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    func countdownFinished() {
        print("ta")
        comecaLabel.text = "Aberto"
        previewCountdown.isHidden = true
    }
}
