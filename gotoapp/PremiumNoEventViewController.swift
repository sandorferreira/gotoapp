//
//  PremiumNoEventViewController.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 02/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

class PremiumNoEventViewController: UIViewController {
    
    @IBOutlet weak var premiumNavigationBar: UINavigationBar!
    
    @IBOutlet weak var locationImage: UIImageView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
    
    var funPointPremium: FunPoint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if funPointPremium != nil {
            self.setupInfo()
        }
    }
    
    func setupInfo() {
        self.locationImage.image = UIImage() //self.funPointPremium?.image
        self.descriptionLabel.text = self.funPointPremium?.desc
        self.addressLabel.text = self.funPointPremium?.currentEventAddress
    }
    
    // << Definir com Paulo >> //
    
    func setupNavigationBar(_ color: String) {
        //let auxColor = self.funPointPremium!.backgroundColor
        switch color {
            case "blue":
            self.premiumNavigationBar.barTintColor = UIColor.blue
            case "black":
            self.premiumNavigationBar.barTintColor = UIColor.black
        default:
            self.premiumNavigationBar.barTintColor = UIColor.gray
            
        }
    }
    
    @IBAction func dismiss(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
