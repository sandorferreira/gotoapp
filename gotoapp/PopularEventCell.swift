//
//  PopularEventCell.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 31/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

class PopularEventCell: UITableViewCell {
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var eventNameUIView: UIView!
    @IBOutlet weak var eventNumberUIView: UIView!
    @IBOutlet weak var eventNumberLabel: UILabel!
}