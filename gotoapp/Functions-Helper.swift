//
//  Functions-Helper.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 08/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import Foundation
import SwiftyTimer
import NVActivityIndicatorView

func containsInArray(_ array: [String], string: String) -> Bool {
    for aux in array {
        if aux == string {
            return true
        }
    }
    return false
}

func startMonitoringEventDuration() {
    if currentEventEndDate != nil {
        let calendar = Calendar.current
        let endDate = (calendar as NSCalendar).date(byAdding: .minute, value: -5, to: currentEventEndDate! as Date, options: [])!
        let elapsedTime = endDate.timeIntervalSinceNow
        Timer.after(elapsedTime, {
            didExitCurrentEvent()
        })
    }
}

func eventHasEnded() -> Bool {
    if currentEventEndDate == nil {
        return true
    } else {
        return false
    }
}

func clearAllLocalNotifications() {
    let application = UIApplication.shared
    for notification in application.scheduledLocalNotifications! {
        application.cancelLocalNotification(notification)
    }
}

func didExitCurrentEvent() {
    currentEvent = nil
    currentEventEndDate = nil
    currentEventLocation = nil
    goToEvent = false
    NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationFuncaoDentroDoEvento), object: nil, userInfo: ["inside" : false])
    NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationEstaNoEvento), object: nil, userInfo: ["inside" : false])
    clearAllLocalNotifications()
//    Evento.checkUserOutFromCurrentEvent(currentEvent!) { (success, evento) in
//        if !success && evento != nil {
//            Evento.checkUserOutFromCurrentEvent(currentEvent!, completionHandler: { (success, evento) in
//                print("user out")
//            })
//        }
//    }
}

func returnPhotoFromBase64EncodedString(_ base64EncodedString: String) -> UIImage? {
    if let imageData = Data(base64Encoded: base64EncodedString, options: .ignoreUnknownCharacters) {
        let image = UIImage(data: imageData)!
        return image
    } else {
        print("nao retornou imagem")
        return nil
    }
}

func returnProfilePicture(_ url: String) -> UIImage? {
    if let url = URL(string: url) {
        if let data = try? Data(contentsOf: url) {
            return UIImage(data: data)
        }
    }
    
    return nil
}

func dateToString(date: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
    return dateFormatter.string(from: date)
}

func stringToDate(string: String) -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
    return dateFormatter.date(from: string)!
}


extension NVActivityIndicatorView {
    static func addActivityIndicator(_ controller: UIViewController, withFrame frame: CGRect) {
        
    }
}
