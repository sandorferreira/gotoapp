//
//  EditEventViewController.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 21/06/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit
import ImageSlideshow
import NVActivityIndicatorView

class EditEventViewController: UIViewController {
    
    // -- outlets from storyboard -- //
    
    @IBAction func dismissEditVC(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var slideShow: ImageSlideshow!
    
    @IBOutlet weak var friendsLabel: UILabel!
    
    @IBOutlet weak var allPeopleLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var addressTextField: UITextField!
    
    @IBOutlet weak var deletePhotosContainerView: UIView!

    
    // -- Edit Buttons -- //
    
    @IBOutlet weak var editDescriptionButton: UIButton!
    
    @IBOutlet weak var editLinkPurchaseButton: UIButton!
    
    @IBOutlet weak var editAddressButton: UIButton!
    
    @IBOutlet weak var saveDescriptionButton: UIButton!
    
    @IBOutlet weak var saveAllButton: UIButton!
    
    // -- Event Helper -- //
    
    var evento: Evento?
    var eventId: String?
    var eventoBuscado: Evento?
    var descAux: String?
    var addressAux: String?
    var linkPurchaseAux: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.setupLabels()
        
        //self.setupLabels()
        deletePhotosContainerView.isHidden = true
        deletePhotosContainerView.reloadInputViews()
        self.saveDescriptionButton.isHidden = true
        self.editDescriptionButton.isHidden = false
        self.addressTextField.isHidden = true
        slideShow.slideshowInterval = 10.0
        slideShow.contentScaleMode = .scaleAspectFill
        
        
        let tap_save_description = UITapGestureRecognizer(target: self, action: #selector(EditEventViewController.saveDescription(_:)))
        self.saveDescriptionButton.addGestureRecognizer(tap_save_description)
        let tap_edit_description = UITapGestureRecognizer(target: self, action: #selector(EditEventViewController.editDescription(_:)))
        self.editDescriptionButton.addGestureRecognizer(tap_edit_description)
        let tap_edit_link = UITapGestureRecognizer(target: self, action: #selector(EditEventViewController.editPurchaseLink(_:)))
        self.editLinkPurchaseButton.addGestureRecognizer(tap_edit_link)
        let tap_edit_address = UITapGestureRecognizer(target: self, action: #selector(EditEventViewController.editAddress(_:)))
        self.editAddressButton.addGestureRecognizer(tap_edit_address)
        let tap_save_all = UITapGestureRecognizer(target: self, action: #selector(EditEventViewController.saveAllChanges(_:)))
        self.saveAllButton.addGestureRecognizer(tap_save_all)
        
        
        let tap_deletePhotos = UILongPressGestureRecognizer(target: self, action: #selector(EditEventViewController.deletePhotos))
        self.slideShow.addGestureRecognizer(tap_deletePhotos)
        
        // -- Keyboard Setup -- //
        
        NotificationCenter.default.addObserver(self, selector: #selector(EditEventViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EditEventViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if self.evento != nil {
            self.setupLabels()
        }
    }
    
    func deletePhotos() {
        print("entrou")
        let deleteViewController = self.storyboard?.instantiateViewController(withIdentifier: "DeleteTableView") as! DeletePhotosTableView
        deleteViewController.eventId = self.eventId!
        deleteViewController.base64EncodedImages = self.evento!.photosArray
        deleteViewController.modalPresentationStyle = .overCurrentContext
        self.present(deleteViewController, animated: false, completion: nil)
    }
    
    func setupLabels() {
//        Evento.returnUserCheckedIn(self.evento!) { (friends, users) in
//            if friends == nil || friends!.count == 0 {
//                self.friendsLabel.text = "Nenhum amigo no evento"
//            } else {
//                if friends!.count == 1 {
//                    self.friendsLabel.text = "1 amigo está aqui"
//                } else {
//                    self.friendsLabel.text = "\(friends!.count) amigos estão aqui"
//                }
//            }
//            if users == nil || users!.count == 0 {
//                self.allPeopleLabel.text = "Ainda nenhum check-in"
//            } else {
//                if users!.count == 1 {
//                    self.allPeopleLabel.text = "1 pessoa está aqui"
//                } else {
//                    self.allPeopleLabel.text = "\(users!.count) pessoas estão aqui"
//                }
//            }
//        }
        self.addressLabel.text = evento!.address
        self.descriptionLabel.text = evento!.desc
        self.addressAux = evento!.address
        self.linkPurchaseAux = evento!.linkPurchase
        self.descAux = evento!.desc
//        let pictures = Evento.getPhotosFromEvent(self.evento!)
//        print(pictures.count)
//        if pictures.count != 0 {
//            var arrayImageSource = [ImageSource]()
//            for picture in pictures {
//                let imageSource = ImageSource(image: picture)
//                arrayImageSource.append(imageSource)
//            }
//            slideShow.setImageInputs(arrayImageSource)
//        } else {
//            self.slideShow.isHidden = true
//        }
    }
    
    func editDescription(_ sender: UITapGestureRecognizer) {
        self.descriptionTextView.text = self.descriptionLabel.text
        self.descriptionLabel.isHidden = true
        self.descriptionTextView.isHidden = false
        self.editDescriptionButton.isHidden = true
        self.saveDescriptionButton.isHidden = false
    }
    
    func saveDescription(_ sender: UITapGestureRecognizer) {
        self.saveDescriptionButton.isHidden = true
        self.editDescriptionButton.isHidden = false
        self.descriptionLabel.text = self.descriptionTextView.text
        self.descriptionTextView.isHidden = true
        self.descriptionLabel.isHidden = false
    }
    
    func editPurchaseLink(_ r: UITapGestureRecognizer) {
        let alert = UIAlertController(title: "Link Compra de Ingresso", message: "Cole abaixo o link para compra de ingresso do seu evento", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Link"
        }
        alert.addAction(UIAlertAction(title: "Salvar", style: .default, handler: { (action) in
            let textField = alert.textFields![0]
            let purchaseLink = textField.text
            print(purchaseLink)
            self.evento?.linkPurchase = purchaseLink
        }))
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func editAddress(_ r: UITapGestureRecognizer) {
        self.addressTextField.text = self.addressLabel.text
        self.addressLabel.isHidden = true
        self.addressTextField.isHidden = false
        self.editAddressButton.titleLabel?.text = "Salvar"
        self.editAddressButton.gestureRecognizers?.removeAll()
        let tap_save_address = UITapGestureRecognizer(target: self, action: #selector(EditEventViewController.saveAddress(_:)))
        self.editAddressButton.addGestureRecognizer(tap_save_address)
        self.editAddressButton.titleLabel!.text = "Editar"
    }
    
    func saveAddress(_ r: UITapGestureRecognizer) {
        self.addressTextField.isHidden = true
        self.addressLabel.text = self.addressTextField.text
        self.addressLabel.isHidden = false
        self.evento?.address = self.addressLabel.text
    }
    
    func saveAllChanges(_ r: UITapGestureRecognizer) {
        
        if !(self.descriptionTextView.text.isEmpty) {
            self.evento!.desc = self.descriptionTextView.text
        } else {
            print("desc empty!")
        }
        
        if !(self.addressTextField.text!.isEmpty) {
            self.evento!.address = self.addressTextField.text
        } else {
            print("address empty")
        }
        
        if self.hasBeenChangesToEvent() {
        let activitySize = CGSize(width: 30.0, height: 30.0)
        let activityFrame = CGRect(origin: self.view.center, size: activitySize)
        let activityIndicator = NVActivityIndicatorView(frame: activityFrame, type: .ballScaleRipple, color: UIColor.black, padding: 2.0)
        activityIndicator.center = self.view.center
        self.view.alpha = 0.5
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        self.verifyPurchaseLink({ (success) in
            if success {
//                storeEvents.save(self.evento!, completionHandler: { (event, error) in
//                    if error != nil {
//                        print(error)
//                    } else {
//                        UIApplication.shared.endIgnoringInteractionEvents()
//                        activityIndicator.stopAnimating()
//                        self.view.alpha = 1.0
//                        print("successfully updated << ")
//                        self.dismiss(animated: true, completion: nil)
//                    }
//                })
//                storeEvents?.save(self.evento, withCompletionBlock: { (results, error) in
//                    if error != nil {
//                        print(error)
//                    } else {
//                        UIApplication.shared.endIgnoringInteractionEvents()
//                        activityIndicator.stopAnimation()
//                        self.view.alpha = 1.0
//                        print("successfully updated << ")
//                        self.dismiss(animated: true, completion: nil)
//                    }
//                    }, withProgressBlock: nil)
            } else {
                UIApplication.shared.endIgnoringInteractionEvents()
                activityIndicator.stopAnimating()
                self.view.alpha = 1.0
                let alert = UIAlertController(title: "Ops!", message: "Confira o link de compra de ingresso e cole novamente ou deixe em branco", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        })
        } else {
            let alert = UIAlertController(title: "Ops!", message: "Não houve modificação no evento. Nada foi alterado", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // -- Keyboard Reocgnizers -- //
    
    func keyboardWillShow(_ notification:Notification){
        
        var userInfo = (notification as NSNotification).userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
    }
    
    func keyboardWillHide(_ notification:Notification){
        let contentInset: UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        scrollView.endEditing(true)
    }
    
    func verifyPurchaseLink(_ completionHandler: (Bool) -> ()) {
        let purchaseLink = self.evento?.linkPurchase
        if (purchaseLink!.contains("http")) || (purchaseLink!.contains("www")) {
            completionHandler(true)
        } else {
            completionHandler(false)
        }
    }
    
    func hasBeenChangesToEvent() -> Bool {
        if self.evento!.desc == self.descAux && self.evento!.address == self.addressAux && self.evento!.linkPurchase == self.linkPurchaseAux {
            return false
        } else {
            return true
        }
    }
    
    
    
    
}
