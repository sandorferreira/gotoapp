//
//  KCSUserEventTableView.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 14/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

class KCSUserEventTableView: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var users: [NSDictionary]?
    var friends: [NSDictionary]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users!.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "KCSUserCell") as! KCSUserCheckInCell
        let user = users![(indexPath as NSIndexPath).row]
        
        let name = user["name"] as! String
        let profile_available = user["profile_available"] as! Bool
        let profile_picture_url = user["profile_picture"] as! String
        let profile_picture = returnProfilePicture(profile_picture_url)
        cell.userName.text = name
        cell.profilePicture.image = profile_picture
        if profile_available {
            cell.profile_availableIV.image = UIImage(named: "profile_available")
        } else {
            cell.profile_availableIV.image = UIImage(named: "profile_unavailable")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = users![(indexPath as NSIndexPath).row]
        let profile_avilable = user["profile_available"] as! Bool
        
        if profile_avilable {
            let profile_link = user["profile_link"] as! String
            let webViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileFBWV") as! PerfilFacebookWV
            webViewController.url = profile_link
            self.navigationController?.pushViewController(webViewController, animated: true)
            print(profile_link)
        } else {
            print("profile not available")
        }
    }
    
    
}
