//
//  PickStartTimeAndDurationViewController.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 02/10/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

class PickStartTimeAndDurationViewController: UIViewController {

    // UIButtons
    
    @IBOutlet weak var agoraButton: UIButton!
    @IBOutlet weak var trintaButton: UIButton!
    @IBOutlet weak var umahoraButton: UIButton!
    
    @IBOutlet weak var quatroButton: UIButton!
    @IBOutlet weak var seisButton: UIButton!
    @IBOutlet weak var oitoButton: UIButton!
    
    // views
    @IBOutlet weak var duiview: UIView!
    @IBOutlet weak var dduiview: UIView!
    @IBOutlet weak var ddduiview: UIView!
    @IBOutlet weak var dddduiview: UIView!
    
    // variables 
    var start: Int?
    var duration: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // adding gestures recognizers
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(PickStartTimeAndDurationViewController.dismissOwnController))
        duiview.addGestureRecognizer(tap) ; dduiview.addGestureRecognizer(tap) ; ddduiview.addGestureRecognizer(tap) ; dddduiview.addGestureRecognizer(tap) ;

        if start != nil || duration != nil {
            if start == 0 {
                agoraButton.setTitleColor(gotoColorPink, for: .normal)
                trintaButton.setTitleColor(gotoDarkGray, for: .normal)
                umahoraButton.setTitleColor(gotoDarkGray, for: .normal)
            } else if start == 1 {
                agoraButton.setTitleColor(gotoDarkGray, for: .normal)
                trintaButton.setTitleColor(gotoColorPink, for: .normal)
                umahoraButton.setTitleColor(gotoDarkGray, for: .normal)
            } else if start == 2 {
                agoraButton.setTitleColor(gotoDarkGray, for: .normal)
                trintaButton.setTitleColor(gotoDarkGray, for: .normal)
                umahoraButton.setTitleColor(gotoColorPink, for: .normal)
            }
            
            if duration == 0 {
                quatroButton.setTitleColor(gotoColorPink, for: .normal)
                seisButton.setTitleColor(gotoDarkGray, for: .normal)
                oitoButton.setTitleColor(gotoDarkGray, for: .normal)
            }else if duration == 1 {
                quatroButton.setTitleColor(gotoDarkGray, for: .normal)
                seisButton.setTitleColor(gotoColorPink, for: .normal)
                oitoButton.setTitleColor(gotoDarkGray, for: .normal)
            }else if duration == 2 {
                quatroButton.setTitleColor(gotoDarkGray, for: .normal)
                seisButton.setTitleColor(gotoDarkGray, for: .normal)
                oitoButton.setTitleColor(gotoColorPink, for: .normal)
            }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Dismiss Function
    
    func dismissOwnController() {
        self.dismiss(animated: false, completion: nil)
    }
    
    // Action Buttons
    
    @IBAction func selectAgora(_ sender: AnyObject) {
        agoraButton.setTitleColor(gotoColorPink, for: .normal)
        trintaButton.setTitleColor(gotoDarkGray, for: .normal)
        umahoraButton.setTitleColor(gotoDarkGray, for: .normal)
        start = 0
    }
    
    @IBAction func selectHalf(_ sender: AnyObject) {
        agoraButton.setTitleColor(gotoDarkGray, for: .normal)
        trintaButton.setTitleColor(gotoColorPink, for: .normal)
        umahoraButton.setTitleColor(gotoDarkGray, for: .normal)
        start = 1
    }
    
    @IBAction func selectOneHour(_ sender: AnyObject) {
        agoraButton.setTitleColor(gotoDarkGray, for: .normal)
        trintaButton.setTitleColor(gotoDarkGray, for: .normal)
        umahoraButton.setTitleColor(gotoColorPink, for: .normal)
        start = 2
    }

    
    @IBAction func durationFour(_ sender: AnyObject) {
        quatroButton.setTitleColor(gotoColorPink, for: .normal)
        seisButton.setTitleColor(gotoDarkGray, for: .normal)
        oitoButton.setTitleColor(gotoDarkGray, for: .normal)
        duration = 0
    }
    
    @IBAction func durationSix(_ sender: AnyObject) {
        quatroButton.setTitleColor(gotoDarkGray, for: .normal)
        seisButton.setTitleColor(gotoColorPink, for: .normal)
        oitoButton.setTitleColor(gotoDarkGray, for: .normal)
        duration = 1
    }
    
    @IBAction func durationEight(_ sender: AnyObject) {
        quatroButton.setTitleColor(gotoDarkGray, for: .normal)
        seisButton.setTitleColor(gotoDarkGray, for: .normal)
        oitoButton.setTitleColor(gotoColorPink, for: .normal)
        duration = 2
    }
    
    @IBAction func selectStartandDuration(_ sender: AnyObject) {
        if self.start == nil || self.duration == nil {
            Alert.showAlertWithMessage("Ops!", withMessage: "Você deve selecionar uma hora para começar e um tempo de duração", actions: nil, atController: self)
        } else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "EventStartTime"), object: nil, userInfo: ["start" : self.start, "duration" : self.duration])
            self.dismiss(animated: false, completion: nil)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
