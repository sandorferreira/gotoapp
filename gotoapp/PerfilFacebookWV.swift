//
//  PerfilFacebookWV.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 20/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

class PerfilFacebookWV: UIViewController {
    
    var url: String?
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        
        let ticketNSURL = URL(string: url!)
        let request = URLRequest(url: ticketNSURL!)
        webView.loadRequest(request)
        
    
        webView.scrollView.contentInset = UIEdgeInsets(top: -64, left: 0, bottom: 0, right: 0)
        
    }
}
