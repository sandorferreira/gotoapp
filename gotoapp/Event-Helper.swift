////
////  Event-Helper.swift
////  gotoapp
////
////  Created by Sandor ferreira da silva on 08/07/16.
////  Copyright © 2016 AppsCat. All rights reserved.
////
//
import Foundation
import UIKit
import FirebaseDatabase
import FirebaseAuth

//
//
//extension Evento {
//    
//    // Check user In at current event marked to go
//    static func checkUserInCurrentEvent(_ event: Evento, completionHandler: @escaping (Bool, Evento?) -> ()) {
//        
//        //let query = Query(format: PersistableIdKey, event.entityId!) //KCSQuery(onField: KCSEntityKeyId, withExactMatchForValue: event.entityId as NSObject!)
//        storeEvents.find(byId: event.entityId!) { (event, error) in
//            if error != nil || event == nil {
//                // event has ended
//                print("false e nil 1")
//                completionHandler(false, nil)
//            } else {
//                let auxEvent = event
//                currentEvent = auxEvent
//                
//                if !isUserAtCurrentEvent(auxEvent!) {
//                    var auxIds = [String]()
//                    for auxId in auxEvent!.usersTokens! {
//                        auxIds.append(auxId)
//                    }
//                    
//                    auxIds.append(KCSUser.active().userId)
//                    auxEvent?.setValue(auxIds, forKey: "usersTokens")
//                    var userRecentEvents = KCSUser.active().getValueForAttribute("recentEvents") as! [String]
//                    if userRecentEvents.count >= 3 {
//                        userRecentEvents.removeFirst()
//                        userRecentEvents.append(auxEvent!.name!)
//                    } else {
//                        userRecentEvents.append(auxEvent!.name!)
//                    }
//                    
//                    KCSUser.active().setValue(userRecentEvents, forAttribute: "recentEvents")
//                    KCSUser.active().save(completionBlock: { (results, error) in
//                        if error != nil {
//                            print(error)
//                        }
//                    })
//                    storeEvents.save(auxEvent!, completionHandler: { (event, error) in
//                        if error != nil {
//                            print(error)
//                            print("false e nil 2")
//                            // event has ended
//                            completionHandler(false, nil)
//                        } else {
//                            print("inseriu User <<")
//                            completionHandler(true, auxEvent!)
//                        }
//                    })
//                    
////                    storeEvents.save(auxEvent, withCompletionBlock: { (results, error) in
////                        if error != nil {
////                            print(error)
////                            print("false e nil 2")
////                            // event has ended
////                            completionHandler(false, nil)
////                        } else {
////                            print("inseriu User <<")
////                            completionHandler(true, auxEvent!)
////                        }
////                        }, withProgressBlock: nil)
//                } else {
//                    //print("deveria estar dentro")
//                    completionHandler(true, nil)
//                }
//
//                
//            }
//        }
////        storeEvents?.query(withQuery: query, withCompletionBlock: { (results, error) in
////            if error != nil || results == nil || results?.count == 0 {
////                // event has ended
////                print("false e nil 1")
////                completionHandler(false, nil)
////            } else {
////                let auxEvent = results?[0] as? Evento
////                currentEvent = auxEvent
////                if !isUserAtCurrentEvent(auxEvent!) {
////                    var auxIds = [String]()
////                    for auxId in auxEvent!.usersTokens! {
////                        auxIds.append(auxId)
////                    }
////                    
////                    auxIds.append(KCSUser.active().userId)
////                    auxEvent?.setValue(auxIds, forKey: "usersTokens")
////                    var userRecentEvents = KCSUser.active().getValueForAttribute("recentEvents") as! [String]
////                    if userRecentEvents.count >= 3 {
////                        userRecentEvents.removeFirst()
////                        userRecentEvents.append(auxEvent!.name!)
////                    } else {
////                        userRecentEvents.append(auxEvent!.name!)
////                    }
////                    
////                    KCSUser.active().setValue(userRecentEvents, forAttribute: "recentEvents")
////                    KCSUser.active().save(completionBlock: { (results, error) in
////                        if error != nil {
////                            print(error)
////                        }
////                    })
////                    
////                    storeEvents?.save(auxEvent, withCompletionBlock: { (results, error) in
////                        if error != nil {
////                            print(error)
////                            print("false e nil 2")
////                            // event has ended
////                            completionHandler(false, nil)
////                        } else {
////                            print("inseriu User <<")
////                            completionHandler(true, auxEvent!)
////                        }
////                        }, withProgressBlock: nil)
////                } else {
////                    //print("deveria estar dentro")
////                    completionHandler(true, nil)
////                }
////            }
////            }, withProgressBlock: nil)
//    }
//    
//    static func checkUserOutFromCurrentEvent(_ event: Evento, completionHandler: @escaping (Bool, Evento?) -> ()) {
//        //let query = KCSQuery(onField: KCSEntityKeyId, withExactMatchForValue: event.entityId as NSObject!)
//        storeEvents.find(byId: event.entityId!) { (event, error) in
//            if error != nil || event == nil {
//                // event has ended
//                completionHandler(false, nil)
//            } else {
//                // event is happening. Check User Id
//                let auxEvent = event //results?[0] as? Evento
//                if isUserAtCurrentEvent(auxEvent!) {
//                    for (index,userId) in auxEvent!.usersTokens!.enumerated() {
//                        if userId == KCSUser.active().userId {
//                            auxEvent?.usersTokens?.remove(at: index)
//                        }
//                    }
//                    storeEvents.save(auxEvent!, completionHandler: { (event, error) in
//                        if error != nil || event == nil{
//                            // event has ended
//                            completionHandler(false, nil)
//                        } else {
//                            completionHandler(true, auxEvent!)
//                        }
//                    })
//                    
////                    storeEvents.save(auxEvent, withCompletionBlock: { (results, error) in
////                        if error != nil || results == nil || results?.count == 0 {
////                            // event has ended
////                            completionHandler(false, nil)
////                        } else {
////                            completionHandler(true, auxEvent!)
////                        }
////                        }, withProgressBlock: nil)
//                } else {
//                    completionHandler(true, nil)
//                }
//            }
//        }
////        storeEvents?.query(withQuery: query, withCompletionBlock: { (results, error) in
////            if error != nil || results == nil || results?.count == 0 {
////                // event has ended
////                completionHandler(false, nil)
////            } else {
////                // event is happening. Check User Id
////                let auxEvent = results?[0] as? Evento
////                if isUserAtCurrentEvent(auxEvent!) {
////                    for (index,userId) in auxEvent!.usersTokens!.enumerated() {
////                        if userId == KCSUser.active().userId {
////                            auxEvent?.usersTokens?.remove(at: index)
////                        }
////                    }
////                    storeEvents?.save(auxEvent, withCompletionBlock: { (results, error) in
////                        if error != nil || results == nil || results?.count == 0 {
////                            // event has ended
////                            completionHandler(false, nil)
////                        } else {
////                            completionHandler(true, auxEvent!)
////                        }
////                        }, withProgressBlock: nil)
////                } else {
////                    completionHandler(true, nil)
////                }
////            }
////            }, withProgressBlock: nil)
//    }
//    
    func getPhotosFromEvent(_ event: Evento) -> [UIImage] {
        if event.photosArray!.count != 0 {
            let eventBase64StringArray = event.photosArray
            var imagesArray = [UIImage]()
            for photo in eventBase64StringArray! {
                //print(photo)
                if let imageData = Data(base64Encoded: photo, options: []) {
                    let image = UIImage(data: imageData)!
                    imagesArray.append(image)
                } else {
                    print("nao recebeu")
                }
            }
            return imagesArray
        } else {
            print("photos array = 0")
            return [UIImage()]
        }
    }

func returnFriendsAtEvent(usersIds: [String], completion: @escaping (([[String: AnyObject]]?) -> () )) {
    let userId = FIRAuth.auth()?.currentUser?.uid
    var friendsDictionary = [[String: AnyObject]]()
    
    if usersIds.count == 0 || usersIds.count == 1 {
        completion(nil)
    } else {
        ref.reference(withPath: "users").child(userId!).observeSingleEvent(of: .value, with: { (snapshot) in
            let user = snapshot.value as! [String: AnyObject] //snapshot.value as? Dictionary
            let friends = user["friends"] as! [[String: AnyObject]]
            var friendsInfo = [[String: AnyObject]]()
            for friend in friends {
                let id = friend["id"] as! String
                let name = friend["name"] as! String
                let finalFriend = ["id": id, "name": name]
                friendsInfo.append(finalFriend as [String : AnyObject])
            }
            
            for userAuxId in usersIds {
                ref.reference(withPath: "users").child(userAuxId).observeSingleEvent(of: .value, with: { (snapshot) in
                    let userInfo = snapshot.value as! [String: AnyObject]
                    let fbId = userInfo["fb-id"] as! String
                    let profilePicture = userInfo["profile_picture"] as! String
                    for friendInfo in friendsInfo {
                        let friendId = friendInfo["id"] as! String
                        let friendName = friendInfo["name"] as! String
                        if friendId == fbId {
                            let friendFinalInfo = ["id": friendId, "name": friendName, "profile_picture" : profilePicture]
                            friendsDictionary.append(friendFinalInfo as [String : AnyObject])
                        }
                    }
                    completion(friendsDictionary)
                })
            }
        })
    }
}


//
//    static func checkUserHasEventRunning(_ completionHandler: @escaping (Bool, Evento?) -> ()) {
//        let creatorId = KCSUser.active().userId! //Kinvey.sharedClient.activeUser!.userId //.acl!.creator //String(KCSUser.active().userId!)
//        print(creatorId)
//        let query = Query(format: "creatorId == %@", creatorId)
//        var promise = storeEvents.find(query) { (events, error) in
//            if error != nil {
//                print(error)
//                completionHandler(false, nil)
//            }
//            else {
//                print("??? ueeee")
//                if events?.count != 0 {
//                    let evento = events![0] //as! Evento
//                    completionHandler(true, evento)
//                } else {
//                    completionHandler(false, nil)
//                }
//            }
//        }
//        print(promise)
//        
////        storeEvents.find(query) { (events, error) -> Void in
////            if error != nil {
////                print(error)
////                completionHandler(false, nil)
////            } else {
////                if events?.count != 0 {
////                    let evento = events![0] //as! Evento
////                    completionHandler(true, evento)
////                } else {
////                    completionHandler(false, nil)
////                }
////            }
////
////        }
////
////        let query = Query(format: PersistableAclKey, creatorId)
////        storeEvents.find(query, deltaSet: true, readPolicy: nil) { (eventos, error) in
////            if error != nil {
////                print(error)
////                completionHandler(false, nil)
////            } else {
////                if eventos?.count != 0 {
////                    let evento = eventos![0] //as! Evento
////                    completionHandler(true, evento)
////                } else {
////                    completionHandler(false, nil)
////                }
////            }
////        }
////        storeEvents.pull(query, deltaSet: false) { (events, error) in
////            if error != nil {
////                print(error)
////                completionHandler(false, nil)
////            } else {
////                if events?.count != 0 {
////                    let evento = events![0] //as! Evento
////                    completionHandler(true, evento)
////                } else {
////                    completionHandler(false, nil)
////                }
////            }
////        }
//        
//        
//        //storeEvents.query(withQuery: query, withCompletionBlock: { (results, error) in
////            if error != nil {
////                print(error)
////                completionHandler(false, nil)
////            } else {
////                if results?.count != 0 {
////                    let evento = results?[0] as! Evento
////                    completionHandler(true, evento)
////                } else {
////                    completionHandler(false, nil)
////                }
////            }
////            }, withProgressBlock: nil)
//    }
//    
//    static func getUserFunPoint(_ completionHandler: @escaping (FunPoint?) -> ()) {
//        let creatorId = String(KCSUser.active().userId)
//        let query = Query(format: PersistableAclKey, creatorId)
//        storeFunPoint.find(query, deltaSet: false, readPolicy: nil) { (funPoints, error) in
//            if error != nil {
//                print(error)
//                completionHandler(nil)
//            } else {
//                if funPoints?.count != 0 {
//                    let funPoint = funPoints![0]
//                    completionHandler(funPoint)
//                } else {
//                    completionHandler(nil)
//                }
//            }
//        }
////        storeFunPoint?.query(withQuery: query, withCompletionBlock: { (results, error) in
////            if error != nil {
////                print(error)
////                completionHandler(nil)
////            } else {
////                if results?.count != 0 {
////                    let funPoint = results?[0] as! FunPoint
////                    completionHandler(funPoint)
////                } else {
////                    completionHandler(nil)
////                }
////            }
////            }, withProgressBlock: nil)
//    }
//    
//    // Saving event on current user FunPoint //
//    static func saveEventToCurrentFunPoint(_ event: Evento, funPoint: FunPoint) {
//        funPoint.event = event
//        storeFunPoint.save(funPoint) { (funPoint, error) in
//            if error != nil {
//                print("error")
//            } else {
//                print("Saved successfully")
//            }
//        }
////        storeFunPoint.save(funPoint, withCompletionBlock: { (results, error) in
////            if error != nil {
////                print("error")
////            } else {
////                print("Saved successfully")
////            }
////            })
//    }
//    
//    static func reloadEvent(_ eventId: String, completionHandler: @escaping (Bool, Evento?) -> ()) {
//        storeEvents.find(byId: eventId) { (event, error) in
//            if error != nil || event == nil {
//                print(error)
//                completionHandler(false, nil)
//            } else {
//                completionHandler(true, event!)
//            }
//        }
////        storeEvents?.loadObject(withID: eventId, withCompletionBlock: { (results, error) in
////            if error != nil || results == nil || results?.count == 0 {
////                print(error)
////                completionHandler(false, nil)
////            } else {
////                let evento = results?[0] as! Evento
////                completionHandler(true, evento)
////            }
////            }, withProgressBlock: nil)
//    }
//    
//    static func reloadAllEventsWithIds(_ eventIds: [String], completion: @escaping (Bool, [Evento]?) -> ()) {
//        //let query = Query() //KCSQuery(onField: KCSEntityKeyId, using: .kcsAll, forValue: eventIds as NSObject!)
//        storeEvents.find { (eventos, error) in
//            if error != nil || eventos == nil {
//                completion(false, nil)
//            } else {
//                completion(true, eventos)
//            }
//        }
////        storeEvents?.query(withQuery: query, withCompletionBlock: { (results, error) in
////            if error != nil || results?.count == 0 || results == nil {
////                completion(false, nil)
////            } else {
////                if let eventos = results as? [Evento] {
////                    completion(true, eventos)
////                } else {
////                    completion(false, nil)
////                }
////            }
////            }, withProgressBlock: nil)
//    }
//    
//    static func returnUserCheckedIn(_ event: Evento, completionHandler: @escaping ([NSDictionary]?, [NSDictionary]?) -> () ) {
//        let friendsArray = KCSUser.active().getValueForAttribute("friends") as! [NSDictionary]
//        let usersIds = event.value(forKey: "usersTokens") as! [String]
//        print("users Ids == \(usersIds)")
//        
//        var friendsDictionary = [NSDictionary]()
//        var usersDictionary = [NSDictionary]()
//        
//        //let usersCollection = KCSCollection.user()
//        //let storeUsers = KCSAppdataStore(collection: usersCollection, options: nil)
//        //let query = KCSQuery(onField: KCSEntityKeyId, usingConditional: .KCSAll, forValue: usersIds)
//        
//        for userId in usersIds {
//            //let query2 = UserQuery(map: userId) //KCSQuery(onField: KCSEntityKeyId, withExactMatchForValue: userId as NSObject!)
//            
//            //let user = User.get(userId: userId) as! KCSUser
//            if let user = User.get(userId: userId) as? KCSUser {
//                let attributes = user.value(forKey: "_userAttributes") as! NSDictionary
//                if let name = attributes["name"] as? String {
//                    let fb_id = attributes["fb-id"] as! String
//                    let profile_available = attributes["profile_available"] as! Bool
//                    let profile_link = attributes["profile_link"] as! String
//                    let profile_picture = attributes["profile_picture"] as! String
//                    let finalUser = ["name" : name, "profile_available" : profile_available, "profile_link" : profile_link, "profile_picture" : profile_picture] as [String : Any]
//                    usersDictionary.append(finalUser as NSDictionary)
//                    
//                    for friend in friendsArray {
//                        let friendFBId = friend["id"] as! String
//                        if friendFBId == fb_id {
//                            friendsDictionary.append(finalUser as NSDictionary)
//                        }
//                    }
//                    print(name)
//                }
//                //}
//                completionHandler(friendsDictionary, usersDictionary)
//            }
////            storeUsers?.query(withQuery: query2, withCompletionBlock: { (results, error) in
////                if error != nil {
////                    return //(nil, nil)
////                } else {
////                    print("ta")
////                    let user = results?[0] as! KCSUser
////                    //for user in users {
////                    let attributes = user.value(forKey: "_userAttributes") as! NSDictionary
////                    if let name = attributes["name"] as? String {
////                        let fb_id = attributes["fb-id"] as! String
////                        let profile_available = attributes["profile_available"] as! Bool
////                        let profile_link = attributes["profile_link"] as! String
////                        let profile_picture = attributes["profile_picture"] as! String
////                        let finalUser = ["name" : name, "profile_available" : profile_available, "profile_link" : profile_link, "profile_picture" : profile_picture] as [String : Any]
////                        usersDictionary.append(finalUser as NSDictionary)
////                        
////                        for friend in friendsArray {
////                            let friendFBId = friend["id"] as! String
////                            if friendFBId == fb_id {
////                                friendsDictionary.append(finalUser as NSDictionary)
////                            }
////                        }
////                        print(name)
////                    }
////                    //}
////                    completionHandler(friendsDictionary, usersDictionary)
////                }
////                }, withProgressBlock: nil)
//        }
//    }
//    
//    static func deletePhotoFromEvent(_ eventId: String, base64EncodedString: String, completionHandler: @escaping (Bool) -> ()) {
//        
//        //let query = //KCSQuery(onField: KCSEntityKeyId, withExactMatchForValue: eventId as NSObject!)
//            
//        storeEvents.find(byId: eventId) { (event, error) in
//            if error != nil  || event == nil {
//                print(error)
//                completionHandler(false)
//            } else {
//                //results?[0] as! Evento
//                for (index, photo) in (event?.photosArray!.enumerated())! {
//                    if photo == base64EncodedString {
//                        event!.photosArray!.remove(at: index)
//                    }
//                }
//                storeEvents.save(event!, completionHandler: { (event, error) in
//                    if error != nil {
//                        completionHandler(false)
//                        print(error)
//                    } else {
//                        completionHandler(true)
//                    }
//                })
//                
////                storeEvents?.save(event, withCompletionBlock: { (results, error) in
////                    if error != nil {
////                        completionHandler(false)
////                        print(error)
////                    } else {
////                        completionHandler(true)
////                    }
////                    }, withProgressBlock: nil)
//                
//            }
//        }
////        storeEvents?.query(withQuery: query, withCompletionBlock: { (results, error) in
////            if error != nil {
////                print(error)
////                completionHandler(false)
////            } else {
////                let event = results?[0] as! Evento
////                for (index, photo) in event.photosArray!.enumerated() {
////                    if photo == base64EncodedString {
////                        event.photosArray?.remove(at: index)
////                    }
////                }
////                
////                storeEvents?.save(event, withCompletionBlock: { (results, error) in
////                    if error != nil {
////                        completionHandler(false)
////                        print(error)
////                    } else {
////                        completionHandler(true)
////                    }
////                    }, withProgressBlock: nil)
////                
////            }
////            }, withProgressBlock: nil)
//    }
//
//}
//
//// Functions
//
//func isUserAtCurrentEvent(_ event: Evento) -> Bool {
//    let usersIds = event.value(forKey: "usersTokens") as! [String]
//    for userId in usersIds {
//        if userId == KCSUser.active().userId {
//            return true
//        }
//    }
//    return false
//}
