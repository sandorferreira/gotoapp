//
//  SettingsViewController.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 12/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import Foundation
import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import FBSDKShareKit


class SettingsViewController: UIViewController {
    
    // Outlets from Storyboard
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var premiumLabel: UILabel!
    @IBOutlet weak var friendsNumberLabel: UILabel!
    @IBOutlet weak var firstRecentEventLabel: UILabel!
    @IBOutlet weak var secondRecentEventLabel: UILabel!
    @IBOutlet weak var thirdRecentEventLabel: UILabel!
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var endingCardNumber: UILabel!
    @IBOutlet weak var packageNameLabel: UILabel!
    @IBOutlet weak var firstFriendProfilePicture: UIImageView!
    @IBOutlet weak var secondFriendProfilePicture: UIImageView!
    @IBOutlet weak var thirdFriendProfilePicture: UIImageView!
    @IBOutlet weak var profileActivityIndicator: UIActivityIndicatorView!
    
    // buttons uiviews
    @IBOutlet weak var purchasePackageUIView: UIView!
    @IBOutlet weak var switchRedirectProfile: UISwitch!
    @IBOutlet weak var reportProblemUIView: UIView!
    @IBOutlet weak var informationUIView: UIView!
    @IBOutlet weak var rateAppUIView: UIView!
    
    // aux
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // configuring designables
        configureDesignables()
        getUserProperties()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController?.navigationBarHidden = true
    }
    
    func getUserProperties() {
        self.profileNameLabel.text = "" //KCSUser.active().getValueForAttribute("name") as? String
        self.profileImageView.image = returnProfilePicture("")
        self.switchRedirectProfile.setOn(false /*(KCSUser.active().getValueForAttribute("profile_available") as! Bool)*/, animated: false)
        
        let friendsArray = /*KCSUser.active().getValueForAttribute("friends") as?*/ [[String: String]]()
//        self.friendsNumberLabel.text = "\(friendsArray!.count) amigos estão no GoTo"
//        if friendsArray?.count != 0  && friendsArray != nil {
//            firstFriendProfilePicture.isHidden = false
//            firstFriendProfilePicture.image = returnProfilePicture(friendsArray![0]["picture"]!)
//            if friendsArray!.count == 2 {
//                secondFriendProfilePicture.isHidden = false
//                secondFriendProfilePicture.image = returnProfilePicture(friendsArray![1]["picture"]!)
//            }
//            if friendsArray!.count >= 3 {
//                thirdFriendProfilePicture.isHidden = false
//                thirdFriendProfilePicture.image = returnProfilePicture(friendsArray![2]["picture"]!)
//            }
//        }
        
//        let recentEventsArray = KCSUser.active().getValueForAttribute("recentEvents") as? [String]
//        if let firstEvent = recentEventsArray?[0] {
//            if firstEvent != "" {
//                firstRecentEventLabel.text = firstEvent //"Nenhum evento recente."
//            } else {
//                firstRecentEventLabel.text = "Nenhum evento recente."
//            }
//        }
//        
//        if let secondEvent = recentEventsArray?[1] {
//            if secondEvent != "" {
//                secondRecentEventLabel.text = secondEvent
//            } else {
//                secondRecentEventLabel.isHidden = true
//            }
//        }
//        
//        if let thirdEvent = recentEventsArray?[2] {
//            if thirdEvent != "" {
//                thirdRecentEventLabel.text = thirdEvent
//            } else {
//                thirdRecentEventLabel.isHidden = true
//            }
//        }
//        
    }
    
    @IBAction func logUserOut(_ sender: AnyObject) {
        let okAction = UIAlertAction(title: "Sim", style: .cancel) { (action) in
            let loginController = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! ViewController
            //KCSUser.active().logout()
            let loginManager = FBSDKLoginManager()
            loginManager.loginBehavior = .web
            FBSDKAccessToken.setCurrent(nil)
            FBSDKProfile.setCurrent(nil)
            loginManager.logOut()
            
            self.present(loginController, animated: false, completion: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .default, handler: nil)
        let actions = [cancelAction, okAction]
        Alert.showAlertWithMessage("Sair", withMessage: "Deseja finalizar sessão atual?", actions: actions, atController: self)
        
    }
    @IBAction func logout(_ sender: AnyObject) {
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func configureDesignables() {
        self.navigationController!.navigationBar.barTintColor = gotoColor //UIColor.clearColor()
        self.navigationController?.navigationBar.isTranslucent = false
        //self.navigationController!.navigationBar.barTintColor = gotoColor //UIColor.whiteColor()
        profileActivityIndicator.stopAnimating()
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2
        firstFriendProfilePicture.layer.cornerRadius = firstFriendProfilePicture.frame.size.height / 2
        secondFriendProfilePicture.layer.cornerRadius = secondFriendProfilePicture.frame.size.height / 2
        thirdFriendProfilePicture.layer.cornerRadius = thirdFriendProfilePicture.frame.size.height / 2
        profileImageView.clipsToBounds = true
        firstFriendProfilePicture.clipsToBounds = true
        secondFriendProfilePicture.clipsToBounds = true
        thirdFriendProfilePicture.clipsToBounds = true
        
        firstFriendProfilePicture.layer.borderColor = UIColor.white.cgColor
        firstFriendProfilePicture.layer.borderWidth = 2
        secondFriendProfilePicture.layer.borderColor = UIColor.white.cgColor
        secondFriendProfilePicture.layer.borderWidth = 2
        thirdFriendProfilePicture.layer.borderColor = UIColor.white.cgColor
        thirdFriendProfilePicture.layer.borderWidth = 2
        
        firstFriendProfilePicture.isHidden = true; secondFriendProfilePicture.isHidden = true; thirdFriendProfilePicture.isHidden = true;
    }
    
    @IBAction func setProfileAvailable(_ sender: AnyObject) {
//        profileActivityIndicator.startAnimating()
//        if switchRedirectProfile.isOn {
//            KCSUser.active().setValue(NSNumber(value: true as Bool), forAttribute: "profile_available")
//            KCSUser.active().save(completionBlock: { (results, error) in
//                self.profileActivityIndicator.stopAnimating()
//                if error != nil {
//                    Alert.showAlertWithMessage("Ops!", withMessage: "Ocorreu um erro ao mudar as configurações e as alterações não foram salvas. Por favor, tente mais tarde", actions: nil, atController: self)
//                }
//            })
//        } else {
//            KCSUser.active().setValue(NSNumber(value: false as Bool), forAttribute: "profile_available")
//            KCSUser.active().save(completionBlock: { (results, error) in
//                self.profileActivityIndicator.stopAnimating()
//                if error != nil {
//                    Alert.showAlertWithMessage("Ops!", withMessage: "Ocorreu um erro ao mudar as configurações e as alterações não foram salvas. Por favor, tente mais tarde", actions: nil, atController: self)
//                }
//            })
//        }
    }
    
    @IBAction func verPacotes(_ sender: AnyObject) {
        
        let controllerPacotes = self.storyboard?.instantiateViewController(withIdentifier: "PacotesViewController") as! PacoteTableView
        self.navigationController?.pushViewController(controllerPacotes, animated: true)
        
    }
    
    @IBAction func inviteFriends(_ sender: AnyObject) {
        
        let actionInvite = UIAlertAction(title: "Convidar", style: .default) { (action) in
            let content = FBSDKAppInviteContent()
            content.appLinkURL = URL(string: "http://appstore.com/pokemon-go")
            FBSDKAppInviteDialog.show(from: self, with: content, delegate: self)
            
        }
        let actionCancel = UIAlertAction(title: "Não, obrigado.", style: .cancel, handler: nil)
        
        let actions = [actionInvite, actionCancel]
        
        Alert.showAlertWithMessage("Convidar Amigos", withMessage: "Ao convidar 15 amigos para o GoTo na primeira vez, você ganha 3 eventos para criar quando quiser. Deseja convidar seus amigos?", actions: actions, atController: self)
    }
    
}

extension SettingsViewController: FBSDKAppInviteDialogDelegate{
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [AnyHashable: Any]!) {
        //TODO
        if results != nil {
            print(results)
        } else {
            Alert.showAlertWithMessage("Ops!", withMessage: "Parece que o convite não foi concluído. Tente novamente.", actions: nil, atController: self)
        }
    }
    
    
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: Swift.Error!) {
        print("Ocorreu um erro ao coisar seus convites")
    }
    
//    override func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: FBSDKErrorCode!) {
//        print("ocorreu um erro ao coisar seus convites")
//    }
}
