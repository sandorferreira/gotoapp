//
//  FunPointMarker.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 17/06/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import GoogleMaps

class FunPointMarker: GMSMarker {
    
    let place: CLLocation
    var event: Evento
    //var eventId: String?
    
    init(place: CLLocation, evento: Evento) {
        self.place = place
        self.event = evento
        super.init()
        title = self.event.name
        snippet = self.event.address
        position = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude)
        //if self.funPoint.premium == 1 {
        icon = UIImage(named: "ic_simple_event")
        //} else {
         //   icon = UIImage(named: "ic_premium_event")
        //}
        groundAnchor = CGPoint(x: 0.5, y: 1.0)
        appearAnimation = kGMSMarkerAnimationPop
    }
    
}
