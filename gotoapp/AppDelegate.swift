//
//  AppDelegate.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 17/05/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import GoogleMaps
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Kinvey
        
        //Kinvey.sharedClient.initialize(appKey: "kid_Zk8m2kX2--", appSecret: "2c0a695fb8254a938e39bf9d81ecb354")
        //Kinvey.sharedClient.initialize(appKey: "kid_Zk8m2kX2--", appSecret: "2c0a695fb8254a938e39bf9d81ecb354", encrypted: false)
        
//        KCSClient.shared().initializeKinveyService(
//            forAppKey: "kid_Zk8m2kX2--",
//            withAppSecret: "2c0a695fb8254a938e39bf9d81ecb354",
//            usingOptions: nil
//        )
        
        // Firebase
        
        FIRApp.configure()
        
        // Google Maps
        
        GMSServices.provideAPIKey("AIzaSyDWtJPlGvaR9vqAWw1gNf_EGNM41avAWRA")
        
        // -- Status Bar
        
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        //UIApplication.sharedApplication().statusBarHidden = false
        
        // -- Navigation Bar
        //UINavigationBar.appearance().setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        //UINavigationBar.appearance().barTintColor = UIColor.clearColor()
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = ([NSFontAttributeName: UIFont(name: "Helvetica Neue", size: 17)!, NSForegroundColorAttributeName: UIColor.white])
        
        // -- Location Notification -- //
        
        let completeAction = UIMutableUserNotificationAction()
        completeAction.identifier = "EVENT_NOT_IN" // the unique identifier for this action
        completeAction.title = "Não" // title for the action button
        completeAction.activationMode = .background
        completeAction.isAuthenticationRequired = false // don't require unlocking before performing action
        completeAction.isDestructive = true // display action in red
        
        let remindAction = UIMutableUserNotificationAction()
        remindAction.identifier = "EVENT_YES_IN"
        remindAction.title = "Sim!"
        remindAction.activationMode = .background
        remindAction.isDestructive = false
        
        let todoCategory = UIMutableUserNotificationCategory() // notification categories allow us to create groups of actions that we can associate with a notification
        todoCategory.identifier = "EVENT_CATEGORY"
        todoCategory.setActions([remindAction, completeAction], for: .default) // UIUserNotificationActionContext.Default (4 actions max)
        todoCategory.setActions([completeAction, remindAction], for: .minimal)
        
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: Set([todoCategory])) //UIUserNotificationSettings(forTypes: [.Alert], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url,sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, for notification: UILocalNotification, completionHandler: @escaping () -> Void) {
        switch (identifier!) {
        case "EVENT_YES_IN":
            if currentEvent != nil {
//            Evento.checkUserInCurrentEvent(currentEvent!, completionHandler: { (sucess, event) in
//                if sucess {
//                    print("Entrou")
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationFuncaoDentroDoEvento), object: nil, userInfo: ["inside" : true, "event" : event!])
//                    goToEvent = false
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationEstaNoEvento), object: nil, userInfo: ["inside" : true])
//                }
//            })
            }
            clearAllLocalNotifications()
        case "EVENT_NOT_IN":
            NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationFuncaoDentroDoEvento), object: nil, userInfo: ["inside" : false])
            goToEvent = false
            NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationEstaNoEvento), object: nil, userInfo: ["inside" : false])
            print("ta")
            clearAllLocalNotifications()
        default: // switch statements must be exhaustive - this condition should never be met
            print("Error: unexpected notification action identifier!")
            clearAllLocalNotifications()
        }
        completionHandler() // per developer documentation, app will terminate if we fail to call this
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        clearAllLocalNotifications()
    }

}

