//
//  DiscoverPost.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 9/11/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

class DiscoverPost {
    var photoString: String
    var localName: String
    var eventName: String
    var cityName: String
    
    init(photoString: String, localName: String, cityName: String, eventName: String) {
        self.photoString = photoString
        self.localName = localName
        self.cityName = cityName
        self.eventName = eventName
    }
}
