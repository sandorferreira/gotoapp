//
//  EventListViewController.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 27/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit
import MapKit

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


class EventListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var nearEventIds: [String]?
    var nearEvents = [NSDictionary]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(EventListViewController.setupEvents(_:)), name: NSNotification.Name(rawValue: "AtualizarMarkersLista"), object: nil)
        print("entrou no viewdidload")
    }
    
    func setupEvents(_ n: Notification) {
        let eventInfo = (n as NSNotification).userInfo!["eventInfo"] as! NSDictionary
        print(eventInfo["eventName"])
        self.nearEvents.append(eventInfo)
        self.nearEvents = self.sortStoreByDistance(self.nearEvents)!
        print(self.nearEvents.count)
        self.tableView.layoutIfNeeded()
        self.tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.nearEvents.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventListCell") as! EventListCell
        let event = self.nearEvents[(indexPath as NSIndexPath).row]
        cell.eventImage.image = returnPhotoFromBase64EncodedString(event["eventImage"] as! String)
        cell.evnetNameLabel.text = event["eventName"] as? String
        
        if event["distance"] as? Double >= 1000.0 {
            cell.distanceLabel.text = "< \(Int(event["distance"] as! Double / 1000) + 1) km"
        } else {
            cell.distanceLabel.text = "< \(Int(event["distance"] as! Double))m"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let event = self.nearEvents[(indexPath as NSIndexPath).row]
        let controllerEventDetail = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailVC") as! EventDetailViewController
        controllerEventDetail.eventId = event["eventId"] as? String
        self.present(controllerEventDetail, animated: true, completion: nil)
    }
    
    func sortStoreByDistance(_ arrayEvents: [NSDictionary]) -> [NSDictionary]? {
        var auxArrayEvents = [NSDictionary]()
        var auxArrayDistance = [Double]()
        for event in arrayEvents {
            let eventLatitude = (event["eventLocation"] as! CLLocation).coordinate.latitude
            let eventLongitude = (event["eventLocation"] as! CLLocation).coordinate.longitude
            
            let userLatitude = usersLocation!.coordinate.latitude
            let userLongitude = usersLocation!.coordinate.longitude
            
            //Euclidian Distance between points
            
            let euclidianDistance = self.measureDistance(userLatitude, lon1: userLongitude, lat2: eventLatitude, lon2: eventLongitude)
            let distance = euclidianDistance
            let auxEvent: NSDictionary = ["eventName" : event["eventName"]!, "eventLocation" : event["eventLocation"]!, "eventImage" : event["eventImage"]!, "eventId" : event["eventId"]!, "distance" : distance]
            //event.setValue(distance, forKey: "distance") //distance
            //print(event["distance"])
            auxArrayEvents.append(auxEvent)
            auxArrayDistance.append(distance)
        }
        //arrayLojas.removeAll()
        var returnedOrder = [NSDictionary]()
        var i = auxArrayDistance.count
        while i > 0 {
            let minDistance = auxArrayDistance.min()
            let auxIndex: Int = auxArrayDistance.index(of: minDistance!)!
            auxArrayDistance.remove(at: auxIndex)
            
            for event in auxArrayEvents {
                if event["distance"] as? Double == minDistance {
                    returnedOrder.append(event)
                }
            }
            i -= 1
        }
        
        return returnedOrder
    }
    
    func measureDistance(_ lat1: Double, lon1: Double, lat2: Double, lon2: Double) -> Double{  // generally used geo measurement function
        let R = 6378.137; // Radius of earth in KM
        let dLat = (lat2 - lat1) * 3.14 / 180;
        let dLon = (lon2 - lon1) * 3.14 / 180;
        let aux = sin(dLat/2) * sin(dLat/2)
        let aux1 = cos(lat1 * 3.14 / 180) * cos(lat2 * 3.14 / 180) * sin(dLon/2) * sin(dLon/2);
        let a = aux + aux1
        let c = 2 * atan2(sqrt(a), sqrt(1-a));
        let d = R * c;
        return d * 1000; // meters
    }

}
