//
//  VariablesHelpers.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 08/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import Foundation
import MapKit

// User location
var usersLocation: CLLocation?

// Compass angle in radians
var compass: Double?

// Variable to check what event the user is going to
var currentEvent: Evento?

// Variable to check if the user is going to an even
var goToEvent = false

// Current event location
var currentEventLocation: CLLocation?

// Current event EndDate to check if event has ended
var currentEventEndDate: Date?

// Variable to check if user checked-in at the event
var isInsideEvent = false

// Variable to check if user is inside range of event
var isUserInsideRange = false

// Event Id to download the content
var globalEventId: String?

// Near event id's
var nearEventIds = [String]()

// show status bar
var hideStatusBar = false
