//
//  SharePhotoViewController.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 04/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit
import Social
import Accounts
import FBSDKLoginKit
import NVActivityIndicatorView
import FirebaseDatabase


fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


class SharePhotoViewController: UIViewController, FBSDKLoginButtonDelegate, UIDocumentInteractionControllerDelegate {
    
    
    @IBOutlet weak var imagePicked: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var shareFacebookBtn: UIButton!
    @IBOutlet weak var shareTwitterBtn: UIButton!
    
//    @IBOutlet weak var imagePicked: UIImageView!
//    @IBOutlet weak var textView: UITextView!
//    @IBOutlet weak var addressLabel: UILabel!
//    @IBOutlet weak var shareTwitter: UILabel!
//    @IBOutlet weak var shareInstagram: UILabel!
//    @IBOutlet weak var shareFacebook: UILabel!
//    @IBOutlet weak var shareWithGoTo: UIView!
    
    var pictureTaken: UIImage?
    var event: Evento?
    
    // -- share bools -- //
    var shareOnTwitter = false
    var shareOnFacebook = false
    
    var documentController: UIDocumentInteractionController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationController?.isNavigationBarHidden = false
        self.imagePicked.image = pictureTaken
        
        let tap_shareTwitter = UITapGestureRecognizer(target: self, action: #selector(SharePhotoViewController.shareWithTwitter))
        //self.shareTwitter.addGestureRecognizer(tap_shareTwitter)
        
        let tap_shareFacebook = UITapGestureRecognizer(target: self, action: #selector(SharePhotoViewController.shareWithFacebook))
        //self.shareFacebook.addGestureRecognizer(tap_shareFacebook)
        
        //let tap_shareWithGoTo = UITapGestureRecognizer(target: self, action: #selector(SharePhotoViewController.shareWithGoto))
        //self.shareWithGoTo.addGestureRecognizer(tap_shareWithGoTo)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
        //self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isStatusBarHidden = false
    }
    
    func shareWithTwitter() {
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter) {
            //self.shareTwitter.textColor = UIColor.red
            self.shareOnTwitter = true
        } else {
            print("Access denied email")
            let alert = UIAlertController(title: "Ops!", message: "Você precisa ter sua conta do Twitter validada nos Ajustes do seu iPhone", preferredStyle: .alert)
            let settingsAction = UIAlertAction(title: "Ajustes", style: .default, handler: { (action) in
                let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                if let url  = settingsUrl {
                    UIApplication.shared.openURL(url)
                }
            })
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            alert.addAction(cancelAction)
            alert.addAction(settingsAction)
            
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func sharePhotoOnTwitter () {
        let account = ACAccountStore()
        let accountType = account.accountType(
            withAccountTypeIdentifier: ACAccountTypeIdentifierTwitter)
        
        account.requestAccessToAccounts(with: accountType, options: nil, completion: { (success, error) in
            if success {
                let arrayOfAccounts =
                    account.accounts(with: accountType)
                
                if (arrayOfAccounts?.count)! > 0 {
                    let twitterAccount = arrayOfAccounts?.first as! ACAccount
                    let uploadURL = URL(string: "https://upload.twitter.com/1.1/media/upload.json")
                    let imageData = UIImageJPEGRepresentation(UIImage(named: "GoButton")!, 100)
                    guard (imageData != nil) else {print("error: There is no imageData"); return}
                    
                    let uploadRequest = SLRequest(forServiceType:SLServiceTypeTwitter, requestMethod: .POST, url: uploadURL, parameters: nil)
                    
                    uploadRequest?.account = twitterAccount
                    uploadRequest?.addMultipartData(imageData, withName: "media", type: nil, filename: nil)
                    
                    uploadRequest?.perform(handler: { (responseData, urlResponse, error ) in
                        // Get the media_id_string from response
                        let mediaIDString = self.stringForKey("media_id_string", fromJSONData:responseData)
                        guard (mediaIDString != nil) else {print("error: no media id in response \(urlResponse?.statusCode)"); return}
                        
                        let statusKey = "status" as NSString
                        let mediaIDKey = "media_ids" as NSString
                        // Use statuses/update.json for the tweet
                        let statusURL = URL(string: "https://api.twitter.com/1.1/statuses/update.json")
                        var message = "#GoTo"
                        if let textTyped = self.textView.text {
                            message = textTyped + "#GoTo"
                        }
                        
                        // Separate request to post the tweet
                        let statusRequest = SLRequest(forServiceType:SLServiceTypeTwitter, requestMethod: .POST, url: statusURL, parameters: [statusKey : message, mediaIDKey : mediaIDString!])
                        
                        statusRequest?.account = twitterAccount
                        
                        statusRequest?.perform(handler: {  (responseData, urlResponse, error) in
                                if let err = error {
                                    print("error : \(err.localizedDescription)")
                                }
                                print("Twitter HTTP response \(urlResponse?.statusCode)")
                        })
                    })
                    
//                    uploadRequest.perform()
//                        {   (responseData: Data!, urlResponse: HTTPURLResponse!, error: NSError!) -> Void in
//                            // Get the media_id_string from response
//                            let mediaIDString = self.stringForKey("media_id_string", fromJSONData:responseData)
//                            guard (mediaIDString != nil) else {print("error: no media id in response \(urlResponse.statusCode)"); return}
//                            
//                            let statusKey = "status" as NSString
//                            let mediaIDKey = "media_ids" as NSString
//                            // Use statuses/update.json for the tweet
//                            let statusURL = URL(string: "https://api.twitter.com/1.1/statuses/update.json")
//                            var message = "#GoTo"
//                            if let textTyped = self.textView.text {
//                                message = textTyped + "#GoTo"
//                            }
//                            
//                            // Separate request to post the tweet
//                            let statusRequest = SLRequest(forServiceType:SLServiceTypeTwitter, requestMethod: .POST, url: statusURL, parameters: [statusKey : message, mediaIDKey : mediaIDString!])
//                            
//                            statusRequest.account = twitterAccount
//                            
//                            statusRequest.perform()
//                                {   (responseData: Data!, urlResponse: HTTPURLResponse!, error: NSError!) -> Void in
//                                    
//                                    if let err = error {
//                                        print("error : \(err.localizedDescription)")
//                                    }
//                                    print("Twitter HTTP response \(urlResponse.statusCode)")
//                            }
//                    }
                }
            }
            else
            {
                
            }
        })
    }
    
    func shareWithFacebook() {
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
            //self.shareFacebook.textColor = UIColor.red
            self.shareOnFacebook = true
        } else {
            print("Access denied email")
            let alert = UIAlertController(title: "Ops!", message: "Você precisa ter sua conta do Facebook validada nos Ajustes do seu iPhone", preferredStyle: .alert)
            let settingsAction = UIAlertAction(title: "Ajustes", style: .default, handler: { (action) in
                let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                if let url  = settingsUrl {
                    UIApplication.shared.openURL(url)
                }
            })
            
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            alert.addAction(cancelAction)
            alert.addAction(settingsAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func sharePhotoOnFacebook() {
        let accountStore = ACAccountStore()
        let accountType = accountStore.accountType(withAccountTypeIdentifier: ACAccountTypeIdentifierFacebook)
        
        let postingOptions = [ACFacebookAppIdKey: "620896491400472", ACFacebookPermissionsKey: ["email"], ACFacebookAudienceKey: ACFacebookAudienceFriends] as [String : Any]
            
            accountStore.requestAccessToAccounts(with: accountType,options: postingOptions as [AnyHashable: Any]) {
                success, error in
                if success {
                    //self.shareFacebook.textColor = UIColor.red
                    let options = [ACFacebookAppIdKey: "620896491400472", ACFacebookPermissionsKey: ["publish_actions"], ACFacebookAudienceKey: ACFacebookAudienceFriends] as [String : Any]
                    
                    accountStore.requestAccessToAccounts(with: accountType, options: options as [AnyHashable: Any]) {
                        success, error in
                        if success {
                            var accountsArray = accountStore.accounts(with: accountType)
                            
                            if (accountsArray?.count)! > 0 {
                                let facebookAccount = accountsArray?[0] as! ACAccount
                                
                                var parameters = Dictionary<String, AnyObject>()
                                parameters["access_token"] =
                                    facebookAccount.credential.oauthToken as AnyObject?
                                if let textTyped = self.textView.text {
                                    parameters["caption"] = textTyped as AnyObject?
                                } else {
                                    parameters["caption"] = "#GoTo" as AnyObject?
                                }
                                
                                
                                let imageData = UIImagePNGRepresentation(UIImage(named: "GoButton")!)
                                guard (imageData != nil) else {print("error: There is no imageData"); return}
                                
                                let feedURL = URL(string:
                                    "https://graph.facebook.com/me/photos")
                                
                                let postRequest = SLRequest(forServiceType:
                                    SLServiceTypeFacebook, requestMethod: SLRequestMethod.POST, url: feedURL, parameters: parameters)
                                postRequest?.addMultipartData(imageData, withName: "source", type: "multipart/form-data", filename: "photo")
                                
                                postRequest?.perform(handler: { (responseData, urlResponse, error) in
                                    print("Facebook HTTP response \(urlResponse?.statusCode)")
                                })
                                
                                
//                                postRequest.perform(handler: {(responseData: Data!, urlResponse: HTTPURLResponse!, error: NSError!) -> Void in
//
//                                    print("Facebook HTTP response \(urlResponse.statusCode)")
//                                })
                            }
                        } else {
                            print("Access denied << publish permissions")
                            print(error?.localizedDescription)
                        }
                    }
                } else {
                }
            }

    }
    
    func shareWithGoto() {
        
//        let activitySize = CGSize(width: 50.0, height: 50.0)
//        let activityFrame = CGRect(origin: self.view.center, size: activitySize)
//        let activityIndicator = NVActivityIndicatorView(frame: activityFrame, type: .ballScaleRipple, color: UIColor.black, padding: 2.0)
//        activityIndicator.center = self.view.center
//        self.view.alpha = 0.5
//        self.view.addSubview(activityIndicator)
//        activityIndicator.startAnimating()
//        if self.shareOnFacebook == true {
//            self.sharePhotoOnFacebook()
//        }
//        
//        if self.shareOnTwitter == true {
//            self.sharePhotoOnTwitter()
//        }
//
//        let imageData = UIImageJPEGRepresentation(self.pictureTaken!, 100)
//        let base64StringPicture = imageData?.base64EncodedString(options: NSData.Base64EncodingOptions())
//        
//        ref.reference(withPath: "Events").child(self.event!.eventId!).observeSingleEvent(of: .value, with: { (snapshot) in
//            let event = snapshot.value as? [String: AnyObject]
//            if var photosArray = event!["photosArray"] as? [String] {
//                if photosArray.count >= 10 {
//                    photosArray.removeFirst()
//                    photosArray.append(base64StringPicture!)
//                }
//                //photosArray.append(base64StringPicture!)
//                ref.reference(withPath: "Events").child(self.event!.eventId!).child("photosArray").setValue(photosArray, withCompletionBlock: { (error, reference) in
//                    if error != nil {
//                        Alert.showAlertWithMessage("Erro", withMessage: "Ocorreu um erro ao enviar sua foto para o GoTo. Verifique sua conexão e tente novamente.", actions: nil, atController: self)
//                        self.view.alpha = 1.0
//                        activityIndicator.stopAnimating()
//                    } else {
//                        // done
//                        self.view.alpha = 1.0
//                        activityIndicator.stopAnimating()
//                    }
//                })
//            } else {
//                var photosArray = [String]()
//                photosArray.append(base64StringPicture!)
//                ref.reference(withPath: "Events").child(self.event!.eventId!).child("photosArray").setValue(photosArray)
//                self.view.alpha = 1.0
//                activityIndicator.stopAnimating()
//            }
//        })
        
        //let store = KCSCollection(from: "Events", of: Evento.self)
        //let storeToUpdate = KCSAppdataStore(collection: store, options: nil)
//        storeEvents.find(byId: self.event!.entityId!) { (event, error) in
//            if error != nil || event == nil {
//                print(error)
//                self.view.alpha = 1.0
//                activityIndicator.stopAnimating()
//                print(error)
//            } else {
//                print("Updated successfully")
//                let imageData = UIImageJPEGRepresentation(self.pictureTaken!, 100)
//                let base64StringPicture = imageData?.base64EncodedString(options: NSData.Base64EncodingOptions())
//                if self.event!.photosArray?.count >= 10 {
//                    self.event!.photosArray?.removeFirst()
//                    self.event!.photosArray?.append(base64StringPicture!)
//                } else {
//                    self.event!.photosArray?.append(base64StringPicture!)
//                }
//                storeEvents.save(self.event!, completionHandler: { (event, error) in
//                    if error != nil {
//                        self.view.alpha = 1.0
//                        activityIndicator.stopAnimating()
//                        print(error)
//                    } else {
//                        self.view.alpha = 1.0
//                        activityIndicator.stopAnimating()
//                        self.dismiss(animated: true, completion: nil)
//                        ("Sucessfully inserted photo in event")
//                    }
//                })
////                storeToUpdate?.save(self.event, withCompletionBlock: { (results, error) in
////                    if error != nil {
////                        self.view.alpha = 1.0
////                        activityIndicator.stopAnimation()
////                        print(error)
////                    } else {
////                        self.view.alpha = 1.0
////                        activityIndicator.stopAnimation()
////                        self.dismiss(animated: true, completion: nil)
////                        ("Sucessfully inserted photo in event")
////                    }
////                    }, withProgressBlock: nil)
//            }
//        }
        
        
//        storeToUpdate?.loadObject(withID: self.event!.entityId, withCompletionBlock: { (results, error) in
//            if error != nil || results == nil || results?.count == 0 {
//                self.view.alpha = 1.0
//                activityIndicator.stopAnimation()
//                print(error)
//            } else {
//                print("Updated successfully")
//                let imageData = UIImageJPEGRepresentation(self.pictureTaken!, 100)
//                let base64StringPicture = imageData?.base64EncodedString(options: NSData.Base64EncodingOptions())
//                if self.event!.photosArray?.count >= 10 {
//                    self.event!.photosArray?.removeFirst()
//                    self.event!.photosArray?.append(base64StringPicture!)
//                } else {
//                    self.event!.photosArray?.append(base64StringPicture!)
//                    }
//                storeToUpdate?.save(self.event, withCompletionBlock: { (results, error) in
//                    if error != nil {
//                        self.view.alpha = 1.0
//                        activityIndicator.stopAnimation()
//                        print(error)
//                    } else {
//                        self.view.alpha = 1.0
//                        activityIndicator.stopAnimation()
//                        self.dismiss(animated: true, completion: nil)
//                        ("Sucessfully inserted photo in event")
//                    }
//                    }, withProgressBlock: nil)
//            }
//            }, withProgressBlock: nil)
    }
    
    fileprivate func stringForKey(_ key: String, fromJSONData data: Data?) -> String?
    {
        guard let inData = data else {return nil}
        do
        {
            let response = try JSONSerialization.jsonObject(with: inData, options: []) as? NSDictionary
            let result = response?.object(forKey: key) as? String
            return result
        }
        catch {return nil}
    }
    
    @IBAction func shareWithGoto(_ sender: AnyObject) {
        let activitySize = CGSize(width: 50.0, height: 50.0)
        let activityFrame = CGRect(origin: self.view.center, size: activitySize)
        let activityIndicator = NVActivityIndicatorView(frame: activityFrame, type: .ballScaleRipple, color: UIColor.black, padding: 2.0)
        activityIndicator.center = self.view.center
        self.view.alpha = 0.5
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        if self.shareOnFacebook == true {
            self.sharePhotoOnFacebook()
        }
        
        if self.shareOnTwitter == true {
            self.sharePhotoOnTwitter()
        }
        
        let imageData = UIImageJPEGRepresentation(self.pictureTaken!, 100)
        let base64StringPicture = imageData?.base64EncodedString(options: NSData.Base64EncodingOptions())
        
        ref.reference(withPath: "Events").child(self.event!.eventId!).observeSingleEvent(of: .value, with: { (snapshot) in
            let event = snapshot.value as? [String: AnyObject]
            if var photosArray = event!["photosArray"] as? [String] {
                if photosArray.count >= 10 {
                    photosArray.removeFirst()
                    photosArray.append(base64StringPicture!)
                }
                //photosArray.append(base64StringPicture!)
                ref.reference(withPath: "Events").child(self.event!.eventId!).child("photosArray").setValue(photosArray, withCompletionBlock: { (error, reference) in
                    if error != nil {
                        Alert.showAlertWithMessage("Erro", withMessage: "Ocorreu um erro ao enviar sua foto para o GoTo. Verifique sua conexão e tente novamente.", actions: nil, atController: self)
                        self.view.alpha = 1.0
                        activityIndicator.stopAnimating()
                    } else {
                        // done
                        self.view.alpha = 1.0
                        activityIndicator.stopAnimating()
                    }
                })
            } else {
                var photosArray = [String]()
                photosArray.append(base64StringPicture!)
                ref.reference(withPath: "Events").child(self.event!.eventId!).child("photosArray").setValue(photosArray)
                self.view.alpha = 1.0
                activityIndicator.stopAnimating()
            }
        })
    }
    
    @IBAction func markShareFacebook(_ sender: AnyObject) {
        if shareOnFacebook == false {
        shareFacebookBtn.setImage(UIImage(named: "fb_share_on"), for: .normal)
        shareFacebookBtn.setTitleColor(facebookColor, for: .normal)
            shareOnFacebook = true
        } else {
            shareFacebookBtn.setImage(UIImage(named: "fb_share_off"), for: .normal)
            shareFacebookBtn.setTitleColor(shareGray, for: .normal)
            shareOnFacebook = false
        }
    }
    
    @IBAction func markShareTwitter(_ sender: AnyObject) {
        if shareOnTwitter == false {
            shareTwitterBtn.setImage(UIImage(named: "twitter_share_on"), for: .normal)
            shareTwitterBtn.setTitleColor(twitterColor, for: .normal)
            shareOnTwitter = true
        } else {
            shareTwitterBtn.setImage(UIImage(named: "twitter_share_off"), for: .normal)
            shareTwitterBtn.setTitleColor(shareGray, for: .normal)
            shareOnTwitter = false
        }
    }
    
    @IBAction func getBack(_ sender: AnyObject) {
        self.dismiss(animated: false, completion: nil)
        //_ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    // Facebook Granting Permissions
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        print("User Logged In")
        
        if ((error) != nil)
        {
            // Process error
        }
        else if result.isCancelled {
            // Handle cancellations
        }
        else {
            self.shareWithFacebook()
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }
}
