//
//  PopularViewController.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 30/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

class PopularViewController: UIViewController {
    
    // Outlets from Storyboard
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var amigosUIView: UIView!
    @IBOutlet weak var amigosLabel: UILabel!
    @IBOutlet weak var pessoasUIView: UIView!
    @IBOutlet weak var pessoasLabel: UILabel!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    var usersArray: [Evento]?
    var friendsArray: [Evento]?
    var friends = true
    var refresher = UIRefreshControl()
    
    let colorPurple = UIColor(red: 142.0/255.0, green: 0.0, blue: 146.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingActivityIndicator.stopAnimating()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        print("entrou no viewDidLoad Popular !!")
        if nearEventIds.count != 0 {
            print("tem evento ai einnnn")
        }
        
        refresher.addTarget(self, action: #selector(PopularViewController.refreshingEvents), for: .valueChanged)
        self.tableView.addSubview(refresher)
        
        getEvents()
    }
    
    func getEvents() {
        loadingActivityIndicator.startAnimating()
//        Evento.reloadAllEventsWithIds(nearEventIds) { (success, events) in
//            if success && events != nil {
//                (self.friendsArray, self.usersArray) = self.orderEventsByUsers(events!)
//                self.loadingActivityIndicator.stopAnimating()
//                self.tableView.reloadData()
//                self.refresher.endRefreshing()
//            } else {
//                print("deu ruim")
//                self.loadingActivityIndicator.stopAnimating()
//                self.refresher.endRefreshing()
//            }
//        }
    }
    
    func refreshingEvents() {
//        Evento.reloadAllEventsWithIds(nearEventIds) { (success, events) in
//            if success && events != nil {
//                (self.friendsArray, self.usersArray) = self.orderEventsByUsers(events!)
//                self.tableView.reloadData()
//                self.refresher.endRefreshing()
//            } else {
//                Alert.showAlertWithMessage("Nenhum evento ):", withMessage: "Parece que não há eventos próximos a você.", actions: nil, atController: self)
//    
//                print("deu ruim")
//                self.refresher.endRefreshing()
//            }
//        }
    }
    
    
    func orderEventsByUsers(_ events: [Evento]) -> (friends: [Evento]?, users: [Evento]?) {
        
        // Friends
        
        var friendsArrayCount = [Int]()
        for event in events {
            let friendsCount = event.friendsTokens!.count
            friendsArrayCount.append(friendsCount)
        }
        friendsArrayCount.sort()
        let descendingFriends = [Int](friendsArrayCount.reversed())
        
        var friendsArray = [Evento]()
//        for descending in descendingFriends {
//            for event in events {
//                if descending == event.friendsTokens!.count && !(friendsArray.contains(where: event)) {
//                    friendsArray.append(event)
//                }
//            }
//        }
        
        // Users
        
        var usersArrayCount = [Int]()
//        for event in events {
//            let usersCount = event.usersTokens!.count
//            usersArrayCount.append(usersCount)
//        }
//        usersArrayCount.sort()
//        let descendingUsers = [Int](usersArrayCount.reversed())
//        
//        var usersArray = [Evento]()
//        for descending in descendingUsers {
//            for event in events {
//                if descending == event.usersTokens!.count && !(usersArray.contains(where: event)) {
//                    usersArray.append(event)
//                }
//            }
//        }
        
        return (friendsArray, usersArray)
    }
    
    @IBAction func selectAmigos(_ sender: AnyObject) {
        // change colors
        // reloadTableView
        self.friends = true
        self.amigosUIView.backgroundColor = colorPurple
        self.amigosLabel.textColor = UIColor.white
        self.pessoasUIView.backgroundColor = UIColor.white
        self.pessoasLabel.textColor = colorPurple
        self.tableView.reloadData()
    }
    
    @IBAction func selectPessoas(_ sender: AnyObject) {
        self.amigosUIView.backgroundColor = UIColor.white
        self.amigosLabel.textColor = colorPurple //UIColor.whiteColor()
        self.pessoasUIView.backgroundColor = colorPurple
        self.pessoasLabel.textColor = UIColor.white
        self.friends = false
        self.tableView.reloadData()
    }
    
}

extension PopularViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if friends == true {
            return friendsArray?.count ?? 0
        } else {
            return usersArray?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PopularEventCell") as! PopularEventCell
        
        if friends == true {
            let event = friendsArray![(indexPath as NSIndexPath).row]
            cell.eventImage.image = returnPhotoFromBase64EncodedString(event.image!)
            cell.eventName.text = event.name
            cell.eventNumberLabel.text = "\(event.friendsTokens!.count) amigos aqui"
        } else {
            let event = usersArray![(indexPath as NSIndexPath).row]
            cell.eventImage.image = returnPhotoFromBase64EncodedString(event.image!)
            cell.eventName.text = event.name
            cell.eventNumberLabel.text = "\(event.usersTokens!.count) pessoas aqui"
        }
        
        return cell
    }
    
    
}
