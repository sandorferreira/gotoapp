//
//  CustomOverlayViewController.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 06/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

class CustomOverlayViewController: UIViewController {

}

// -- Cropping Image Utils -- //

struct xy {
    var x: CGFloat!
    var y: CGFloat!
    
    mutating func xy(_ _x: CGFloat, _y: CGFloat) {
        self.x = _x
        self.y = _y
    }
}

enum CROP_TYPE {
    case square
    static let divs = [square: 1]
    static let muls = [square: xy(x: 0.5, y: 0.5)]
    static let names = [square: "1:1"]
    
    func Div() -> Int {
        return 1
    }
    
    func Muls() -> xy {
        return xy(x: 0.5, y: 0.5)
    }
}

struct CROP_OPTIONS {
    var Height: CGFloat
    var Width: CGFloat
    var Center: CGPoint
}

struct FRAME {
    var Height: CGFloat
    var Width: CGFloat
}

var _cropoptions: CROP_OPTIONS!
var _frame : FRAME!
var croptype: CROP_TYPE!

