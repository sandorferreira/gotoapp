//
//  CreateEventUser.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 21/05/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit
import ImagePicker
import FirebaseAuth

class CreateEventUser: UIViewController, ImagePickerDelegate {
    
    // Delegates from Storyboard
    
    @IBOutlet weak var scrollView: UIScrollView!
    // Event inputs //
    
    @IBOutlet weak var nomeEventoTF: UITextField!
    
    @IBOutlet weak var localEventTF: UITextField!
    
    @IBOutlet weak var linkPurchaseTF: UITextField!
    
    @IBOutlet weak var descriptionTV: UITextView!
    
    @IBOutlet weak var eventImage: UIImageView!
    
    @IBOutlet weak var nextButton: UIView!
    
    @IBOutlet weak var pickStartTime: UIButton!
    
    @IBOutlet weak var placeHolderImage: UIView!
    
    @IBOutlet weak var activityIndicatorImage: UIActivityIndicatorView!
    
    @IBOutlet weak var startanddurationlabel: UILabel!
    
    //var startTimeDelay: Int?
    var start: Int?
    var duration: Int?
    let imagePickerController = ImagePickerController()
    // OVERRIDEN FUNCTIONS //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBarColorToTransparent()
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 1
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style: .plain, target:nil, action:nil)
        
        // -- Tap Gesture Recognizers -- //
        //placeHolderImage.isUserInteractionEnabled = true
        activityIndicatorImage.stopAnimating()
        let tap_choosePhoto = UITapGestureRecognizer(target: self, action: #selector(CreateEventUser.getEventImage(_:)))
        self.placeHolderImage.addGestureRecognizer(tap_choosePhoto)
        //eventImage.addGestureRecognizer(tap_choosePhoto)
        //eventImage.isHidden = true
        let tap_create_event = UITapGestureRecognizer(target: self, action: #selector(CreateEventUser.createEvent(_:)))
        nextButton.addGestureRecognizer(tap_create_event)
        
        let tap_pick_startDate = UITapGestureRecognizer(target: self, action: #selector(CreateEventUser.pickStartDate(_:)))
        self.pickStartTime.addGestureRecognizer(tap_pick_startDate)
        //buttonCriar.addGestureRecognizer(tap_create_event)
        
        // -- Keyboard Setup -- //
        
        NotificationCenter.default.addObserver(self, selector: #selector(CreateEventUser.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateEventUser.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateEventUser.setStartTimeDelay(_:)), name: NSNotification.Name(rawValue: "EventStartTime"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateEventUser.dismissImagePicker), name: NSNotification.Name(rawValue: "NotificationDismissImagePicker"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateEventUser.setandoEventImage(_:)), name: NSNotification.Name(rawValue: "NotificationCroppedImage"), object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CreateEventUser.dismissKeyboard))
        self.scrollView.addGestureRecognizer(tap)
    }
    
    func dismissImagePicker() {
        imagePickerController.dismiss(animated: false, completion: nil)
    }
    
    func setandoEventImage(_ n: Notification) {
        //imagePickerController.dismiss(animated: false, completion: nil)
        self.placeHolderImage.isHidden = true
        let tap_choosePhoto = UITapGestureRecognizer(target: self, action: #selector(CreateEventUser.getEventImage(_:)))
        self.eventImage.addGestureRecognizer(tap_choosePhoto)
        let imageCropped = n.userInfo!["image"] as! UIImage
        eventImage.image = imageCropped
    }
    
    func getUsersLocation(_ notification: Notification) {
        
    }
    
    func pickStartDate(_ r: UITapGestureRecognizer) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PickStartTimeAndDurationViewController") as! PickStartTimeAndDurationViewController
        controller.modalPresentationStyle = .overCurrentContext
        if start != nil && duration != nil {
            controller.start = start
            controller.duration = duration
        }
        self.present(controller, animated: false, completion: nil)
    }
    
    func setStartTimeDelay(_ notification: Notification) {
        let start = (notification as NSNotification).userInfo!["start"] as! Int
        let duration = (notification as NSNotification).userInfo!["duration"] as! Int
        self.start = start
        self.duration = duration
        var startString = String()
        var durationString = String()
        if start == 0 {
            startString = "agora"
        } else if start == 1 {
            startString = "em 30min"
        } else if start == 2 {
            startString = "em 1 hora"
        }
        if duration == 0 {
            durationString = "4 horas"
        } else if duration == 1 {
            durationString = "6 horas"
        } else if duration == 2 {
            durationString = "8 horas"
        }
        
        startanddurationlabel.text = "Seu evento começará \(startString) com duração de \(durationString)"
        
        //self.startTimeDelay = delay
    }
    
    func setupNavigationBarColorToTransparent() {
        self.title = "Criar Evento"
        self.view.backgroundColor = UIColor(red: 148/255.0, green: 64/255.0, blue: 185/255.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(CreateEventUser.dismissViewControllerFromDoneButton))
        self.navigationItem.rightBarButtonItem = doneButton
        self.descriptionTV.layer.borderWidth = 0.5
        self.descriptionTV.layer.cornerRadius = 5.0
        self.descriptionTV.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func dismissViewControllerFromDoneButton() {
        //self.navigationController?.popViewControllerAnimated(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func createEvent(_ r: UITapGestureRecognizer) {
        if (nomeEventoTF.text?.isEmpty)! || descriptionTV.text.isEmpty || (localEventTF.text?.isEmpty)! || start == nil || duration == nil {
            Alert.showAlertWithMessage("Ops!", withMessage: "Todos os campos, exceto compra de ingresso são obrigatórios", actions: nil, atController: self)
        } else {
            let name = nomeEventoTF.text!
            let description = descriptionTV.text!
            var linkPurchase = String() //linkPurchaseTF.text!
            if (linkPurchaseTF.text?.isEmpty)! {
                linkPurchase = ""
            } else if checkPurchaseLink(link: linkPurchaseTF.text!) {
                linkPurchase = linkPurchaseTF.text!
            } else {
                linkPurchase = ""
                Alert.showAlertWithMessage("Ops!", withMessage: "Link de compra de ingresso inválido. Verifique o link e tente novamente", actions: nil, atController: self)
            }
            
            let locationName = localEventTF.text!
            
            //let isPremium: NSNumber = NSNumber(value: false)
            
            var data = Data()
            var base64String = String()
            if let auxImage = UIImageJPEGRepresentation(eventImage.image!, 0.8) {
                data = auxImage
                base64String = data.base64EncodedString(options: .lineLength64Characters)
            } else {
                Alert.showAlertWithMessage("Ops!", withMessage: "Você precisa escolher uma imagem para seu evento.", actions: nil, atController: self)
            }
            //data = UIImageJPEGRepresentation(eventImage.image!, 0.8)!
            //let base64String = data.base64EncodedString(options: .lineLength64Characters)
            
            // -- Location Dictionary -- /
            
            //        let latitude = usersLocation?.coordinate.latitude
            //        let longitude = usersLocation?.coordinate.longitude
            //        let localNSDictionary: NSDictionary = ["latitude" : latitude!, "longitude" : longitude!]
            //
            // -- Date Values -- //
            
            var startDate = Date()
            var endDate = Date()
            let currentDate = Date()
            let calendar = Calendar.current
            if self.start == 1 {
                startDate = (calendar as NSCalendar).date(byAdding: .minute, value: 30, to: currentDate, options: [])!
            } else if self.start == 2 {
                startDate = (calendar as NSCalendar).date(byAdding: .hour, value: 1, to: currentDate, options: [])!
            }
            if self.duration == 0 {
                endDate = (calendar as NSCalendar).date(byAdding: .hour, value: 4, to: startDate, options: [])!
            } else if self.duration == 1 {
                endDate = (calendar as NSCalendar).date(byAdding: .hour, value: 6, to: startDate, options: [])!
            } else if self.duration == 2 {
                endDate = (calendar as NSCalendar).date(byAdding: .hour, value: 8, to: startDate, options: [])!
            }
            //let endDate = (calendar as NSCalendar).date(byAdding: .hour, value: 8, to: startDate, options: [])
            let creatorId = (FIRAuth.auth()?.currentUser?.uid)!
            let event = Evento(name: name, creatorId: creatorId,desc: description, image: base64String, linkPurchase: linkPurchase, startDate: dateToString(date: startDate), endDate: dateToString(date: endDate), customBackgroundColor: "", type: "Party", address: locationName)
            event.usersTokens = [String]()
            //event.usersTokens?.append(KCSUser.active().userId)
            event.friendsTokens = [String]()
            event.photosArray = [String]()
            event.location = usersLocation!
            //let funPoint = FunPoint(premium: isPremium, place: usersLocation!, event: event, currentEventName: name, currentEventAddress: locationName)
            //funPoint.currentEventImage = base64String
            let eventDic = ["name": name, "creatorId" : (FIRAuth.auth()?.currentUser?.uid)!, "desc" : description, "image" : base64String, "linkPurchase" : linkPurchase, "startDate" : dateToString(date:startDate), "endDate": dateToString(date: endDate), "address" : locationName, "friendsTokens": [String](), "photosArray" : [String](), "usersTokens": [creatorId]] as [String : Any]
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "PreviewVC") as! PreviewViewController
            controller.evento = event
            controller.eventDic = eventDic as NSDictionary?
            //controller.funPoint = funPoint
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func checkPurchaseLink(link: String?) -> Bool {
        if let urlString = link {
            if let url = NSURL(string: urlString) {
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    func getEventImage(_ r: UITapGestureRecognizer) {
        print("cagaremos??")
        activityIndicatorImage.startAnimating()
        self.present(imagePickerController, animated: true, completion: {
            self.activityIndicatorImage.stopAnimating()
        })
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        eventImage.contentMode = .scaleAspectFit
        eventImage.image = images[0]
        let imageSelected = images[0]
        let cropController = self.storyboard?.instantiateViewController(withIdentifier: "CropViewEvent") as! CropViewEvent
        cropController.imageToCrop = imageSelected
        imagePicker.present(cropController, animated: true, completion: nil)
        //self.dismiss(animated: false, completion: nil)
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        
    }
    
    // -- Keyboard Funcs -- //
    
    
    func keyboardWillShow(_ notification:Notification){
        
        var userInfo = (notification as NSNotification).userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
    }
    
    func keyboardWillHide(_ notification:Notification){
        let contentInset: UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        scrollView.endEditing(true)
    }
    
    
}
