//
//  ViewController.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 17/05/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

/*
 
 KCSUser.activeUser().setValue(num, forAttribute: "PostCount")
 KCSUser.activeUser.saveWithCompletionBlock { (error) -> Void in
 print(error)
 }
 When setting isPremuim: Bool and remainingEvents: NSNumber(int: 5)
 This attributes can only be retrieved
*/

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import FirebaseAuth
import FirebaseDatabase

class ViewController: UIViewController {
    /*!
     @abstract Sent to the delegate when the button was used to login.
     @param loginButton the sender
     @param result The results of the login
     @param error The error (if any) from the login
     */


    //let loginView : FBSDKLoginButton = FBSDKLoginButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.isStatusBarHidden = true
//        let loginManager = FBSDKLoginManager()
//        loginManager.loginBehavior = .web
//        FBSDKAccessToken.setCurrent(nil)
//        FBSDKProfile.setCurrent(nil)
//        loginManager.logOut()
        
        if (FBSDKAccessToken.current() != nil
            && FIRAuth.auth()?.currentUser != nil)
        {
            self.setFirstLoginUserData()
            print("gente que bacana olha")
            let navigationHomeController = self.storyboard?.instantiateViewController(withIdentifier: "TTTabBar") as! TTTabBar
            //l//et aaaaa = UIViewController()
            self.present(navigationHomeController, animated: false, completion: nil)
        }
        else
        {
//            print("deu ruim")
////            print(FBSDKAccessToken.current())
//            loginView.loginBehavior = .web
//            self.view.addSubview(loginView)
//            loginView.center = self.view.center
//            loginView.readPermissions = ["public_profile", "email", "user_friends"]
//            loginView.delegate = self
        }

        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    @IBAction func loginFacebook(_ sender: AnyObject) {
        let fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.loginBehavior = .web
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: self) { (result, error) in
            
            if error != nil {
                print(error)
            }
            else if (result?.isCancelled)! {
                print("cancelled")
            }
            else {
                let accessToken = FBSDKAccessToken.current().tokenString
                print(accessToken)
                let credentials = FIRFacebookAuthProvider.credential(withAccessToken: accessToken!)
                FIRAuth.auth()?.signIn(with: credentials, completion: { (user, error) in
                    if error != nil {
                        print(error)
                    } else {
                        self.setFirstLoginUserData()
                        //user?.setValue("gente?", forKey: "teste")
                        // call TTTabBar
                        let navigationHomeController = self.storyboard?.instantiateViewController(withIdentifier: "TTTabBar") as! TTTabBar
                        self.present(navigationHomeController, animated: false, completion: nil)
                    }
                })
            }
        }
    }
    

    
//    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Swift.Error!) {
//        print("User Logged In")
//        //print(result)
//        if ((error) != nil)
//        {
//            print(error)
//            // Process error
//        }
//        else if result.isCancelled {
//            print("cancelled")
//            // Handle cancellations
//        }
//        else {
//            print("logou???")
//            let accessToken = FBSDKAccessToken.current().tokenString
//            //let facebookDictionary = FBSDKAccessToken.current()
//            
//            
//            ///let event = Evento(name: "", desc: "", image: "", linkPurchase: "", startDate: NSDate() as Date, endDate: NSDate() as Date, isPrivate: NSNumber(value: true), customBackgroundColor: "", type: "", address: "")
//            
//            
////            storeEvents.save(event, completionHandler: { (event, error) in
////                if error != nil {
////                    print(error)
////                } else {
////                    print("foi ne gente")
////                }
////            })
//            
////            Kinvey.User.login(authSource: .facebook, ["accessToken" : accessToken], client: .sharedClient, completionHandler: { (user, error) in
////                print("???????")
////                if let user = user {
////                    print("logou")
////                    if user.value(forKey: "remainingEvents") == nil && user.value(forKey:"isPremium") == nil {
////                        print("entrou no first login")
////                        self.setFirstLoginUserData()
////                    } else {
////                        print("entrou no login sem ser primeira vez")
////                        //self.loginView.hidden = true
////                        let navigationHomeController = self.storyboard?.instantiateViewController(withIdentifier: "TTTabBar") as! TTTabBar
////                        self.present(navigationHomeController, animated: false, completion: nil)
////                    }
////                } else {
////                    print("failed login")
////                }
////            })
//            
//            
////            KCSUser.login(withSocialIdentity: .socialIDFacebook, accessDictionary: [KCSUserAccessTokenKey : accessToken], withCompletionBlock: { (user, error, result) in
////                print("gente entrou???")
////            })
//            
//            
////            User.login(authSource: .facebook, facebookDictionary as! [String: Any]) { user, error in
////                print("???????")
////                if error != nil {
////                    print(error)
////                }
////                if let user = user {
////                    print("logou")
////                    if user.value(forKey: "remainingEvents") == nil && user.value(forKey:"isPremium") == nil {
////                        print("entrou no first login")
////                        self.setFirstLoginUserData()
////                    } else {
////                        print("entrou no login sem ser primeira vez")
////                        //self.loginView.hidden = true
////                        let navigationHomeController = self.storyboard?.instantiateViewController(withIdentifier: "TTTabBar") as! TTTabBar
////                        self.present(navigationHomeController, animated: false, completion: nil)
////                    }
////                } else {
////                    print("failed login")
////                }
////            }
//        
//            
//            
////            KCSUser.login(withSocialIdentity: .socialIDFacebook, accessDictionary: [KCSUserAccessTokenKey : accessToken!], withCompletionBlock: { (user, error, result) in
////                print("entrou???????")
////                if error != nil {
////                    print("error while trying to login via Kinvey")
////                } else {
////                    print("entrou aqui neam")
////                    if user?.getValueForAttribute("remainingEvents") == nil && user?.getValueForAttribute("isPremium") == nil {
////                        print("entrou no first login")
////                        self.setFirstLoginUserData()
////                    } else {
////                        print("entrou no login sem ser primeira vez")
////                        //self.loginView.hidden = true
////                        let navigationHomeController = self.storyboard?.instantiateViewController(withIdentifier: "TTTabBar") as! TTTabBar
////                        self.present(navigationHomeController, animated: false, completion: nil)
////                    }
////                }
////            })
//            
//            // CADASTRAR AQUI
//            
//            
//            // If you ask for multiple permissions at once, you
//            // should check if specific permissions missing
//            
//            
//            if result.grantedPermissions.contains("email")
//            {
//                
//                print("TEM EMAIL TA")
//                
//                //setFirstLoginUserData()
//                
////                User.login(authSource: .facebook, [KCSUserAccessTokenKey: accessToken!]) { user, error in
////                    print("TA NE??")
////                    if error != nil {
////                        print(error)
////                    } else {
////                        if let premium = KCSUser.active().getValueForAttribute("isPremium") as? Bool {
////                            let navigationHomeController = self.storyboard?.instantiateViewController(withIdentifier: "TTTabBar") as! TTTabBar
////                            self.present(navigationHomeController, animated: false, completion: nil)
////                            
////                        } else {
////                            self.setFirstLoginUserData()
////                        }
////                    }
////                }
//            }
//        }
//    }
    
    
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }

    func setFirstLoginUserData()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "email, name, picture, friends, link"])
        graphRequest.start(completionHandler: { (connection, result , error) -> Void in
            
            if ((error) != nil)
            {
                // Process error
                print("Error: \(error)")
            }
            else
            {
                let result = result as! NSDictionary
                print(result)
                var friendsInfo = [[String: String]]()
                let id = result.value(forKey: "id") as! NSString
                let profileIMGURL = self.returnUserProfileImage(id)
                let userName : NSString = result.value(forKey: "name") as! NSString
                let userEmail : NSString = result.value(forKey: "email") as! NSString
                let userFriends = result.value(forKey: "friends") as! [String : AnyObject]
                let friendsIds = userFriends["data"] as! NSArray
                for friendId in friendsIds {
                    let friend = friendId as! [String : AnyObject]
                    let idF = friend["id"] as! String
                    let nameF = friend["name"] as! String
                    let friendInfo = ["id" : idF, "name" : nameF]
                    friendsInfo.append(friendInfo)
                }
                
                if let user = FIRAuth.auth()?.currentUser {
                    print("received user!")
                    ref.reference(withPath: "users").child((FIRAuth.auth()?.currentUser?.uid)!).observeSingleEvent(of: .value, with: { (snapshot) in
                        let currentUser = snapshot.value as! [String: AnyObject]
                        var auxremainingEvents = NSNumber(value: 0 as Int32)
                        var auxrecentEvents = ["","",""]
                        var auxpackageName = "Nenhum"
                        var auxisPremium = NSNumber(value: false as Bool)
                        var auxprofile_available = NSNumber(value: false as Bool)
                        if let remainingEvents = currentUser["remainingEvents"] as? NSNumber {
                            auxremainingEvents = remainingEvents
                        }
                        if let recentEvents = currentUser["recentEvents"] as? [String] {
                            auxrecentEvents = recentEvents
                        }
                        if let packageName = currentUser["packageName"] as? String {
                            auxpackageName = packageName
                        }
                        if let profile_available = currentUser["profile_available"] as? NSNumber {
                            auxprofile_available = profile_available
                        }
                        if let isPremium = currentUser["isPremium"] as? NSNumber {
                            auxisPremium = isPremium
                        }
                        
                        let personalLink = result.value(forKey: "link") as! NSString
                        let info = ["fb-id" : id, "profile_picture": profileIMGURL, "friends": friendsInfo, "name": userName, "email": userEmail, "profile_link": personalLink, "profile_available": auxprofile_available, "isPremium": auxisPremium, "remainingEvents": auxremainingEvents, "recentEvents": auxrecentEvents, "packageName": auxpackageName] as [String : Any]
                        ref.reference(withPath: "users").child(user.uid).setValue(info)
                        
                    })
//                    let personalLink = result.value(forKey: "link") as! NSString
//                    let info = ["fb-id" : id, "profile_picture": profileIMGURL, "friends": friendsInfo, "name": userName, "email": userEmail, "profile_link": personalLink, "profile_available": NSNumber(value: false as Bool), "isPremium": NSNumber(value: false as Bool), "remainingEvents": NSNumber(value: 0 as Int32), "recentEvents": ["","",""], "packageName": "Nenhum"] as! NSDictionary
//                    ref.reference(withPath: "users").child(user.uid).setValue(info)
                    
//                    ref.reference(withPath: "users").child(user.uid).setValue(["profile_picture": profileIMGURL])
//                    ref.reference(withPath: "users").child(user.uid).setValue(["friends": friendsInfo])
//                    ref.reference(withPath: "users").child(user.uid).setValue(["name": userName])
//                    ref.reference(withPath: "users").child(user.uid).setValue(["email": userEmail])
//                    ref.reference(withPath: "users").child(user.uid).setValue(["profile_link": personalLink])
//                    ref.reference(withPath: "users").child(user.uid).setValue(["profile_available": NSNumber(value: false as Bool)])
//                    ref.reference(withPath: "users").child(user.uid).setValue(["isPremium": NSNumber(value: false as Bool)])
//                    ref.reference(withPath: "users").child(user.uid).setValue(["remainingEvents": NSNumber(value: 0 as Int32)])
//                    ref.reference(withPath: "users").child(user.uid).setValue(["recentEvents": ["","",""]])
//                    ref.reference(withPath: "users").child(user.uid).setValue(["packageName": "Nenhum"])
                    
//                    //let personalLink = result.value(forKey: "link") as! NSString
//                    //Kinvey.sharedClient.activeUser.setValue(id, forAttribute: "fb-id")
//                    //user?.setValue(<#T##value: Any!##Any!#>, forAttribute: <#T##String!#>)
//                    user.setValue(profileIMGURL, forKey: "profile_picture")
//                    user.setValue(friendsInfo, forKey: "friends")
//                    user.setValue(userName, forKey: "name")
//                    user.setValue(userEmail, forKey: "email")
//                    user.setValue(personalLink, forKey: "profile_link")
//                    user.setValue(NSNumber(value: false as Bool), forKey: "profile_available")
//                    user.setValue(NSNumber(value: false as Bool), forKey: "isPremium")
//                    user.setValue(NSNumber(value: 0 as Int32), forKey: "remainingEvents")
//                    user.setValue(["","",""], forKey: "recentEvents")
//                    user.setValue("Nenhum", forKey: "packageName")

                }
                //print(friendsInfo)
//                let user = KCSUser.active()! //Kinvey.sharedClient.activeUser!
//                let personalLink = result.value(forKey: "link") as! NSString
//                //Kinvey.sharedClient.activeUser.setValue(id, forAttribute: "fb-id")
//                //user?.setValue(<#T##value: Any!##Any!#>, forAttribute: <#T##String!#>)
//                user.setValue(profileIMGURL, forAttribute: "profile_picture")
//                user.setValue(friendsInfo, forAttribute: "friends")
//                user.setValue(userName, forAttribute: "name")
//                user.setValue(userEmail, forAttribute: "email")
//                user.setValue(personalLink, forAttribute: "profile_link")
//                user.setValue(NSNumber(value: false as Bool), forAttribute: "profile_available")
//                user.setValue(NSNumber(value: false as Bool), forAttribute: "isPremium")
//                user.setValue(NSNumber(value: 0 as Int32), forAttribute: "remainingEvents")
//                user.setValue(["","",""], forAttribute: "recentEvents")
//                user.setValue("Nenhum", forAttribute: "packageName")
//                user.save() { user, error in
//                    if error != nil {
//                        print(error.debugDescription)
//                    } else {
//                        //self.loginView.hidden = true
//                        let navigationHomeController = self.storyboard?.instantiateViewController(withIdentifier: "TTTabBar") as! TTTabBar
//                        self.present(navigationHomeController, animated: false, completion: nil)
//                    }
//                }
                
//                KCSUser.active().save(completionBlock: { (result, error) in
//                    if error != nil {
//                        print(error.debugDescription)
//                    } else {
//                        //self.loginView.hidden = true
//                        let navigationHomeController = self.storyboard?.instantiateViewController(withIdentifier: "TTTabBar") as! TTTabBar
//                        self.present(navigationHomeController, animated: false, completion: nil)
//                    }
//                })
            }
        })
    }
    
    func setFriendsArray() {
        
        // getting already set information
        
//        userName = KCSUser.active().getValueForAttribute("name") as? String
//        isPremium = KCSUser.active().getValueForAttribute("isPremium") as? Bool
//        is_profile_available = KCSUser.active().getValueForAttribute("profile_available") as? Bool
//        recentEventsName = KCSUser.active().getValueForAttribute("recentEvents") as? [String]
        
        
        let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "friends, picture"])
        graphRequest?.start { (connection, result, error) in
            if let result = result as? NSDictionary {
                if error != nil {
                    print(error)
                } else {
                    if let id = result.value(forKey: "id") as? NSString {
                        let profileIMGURL = self.returnUserProfileImage(id)
                        if let user = FIRAuth.auth()?.currentUser {
                            ref.reference(withPath: "users").child(user.uid).setValue(["profile_picture": profileIMGURL])
                        }
                        //user.setValue(profileIMGURL, forKey: "profile_picture")
                        //KCSUser.active().setValue(profileIMGURL, forAttribute: "profile_picture")
                    }
                    var friendsInfo = [[String: String]]()
                    let userFriends = result.value(forKey: "friends") as! [String : AnyObject]
                    let friendsIds = userFriends["data"] as! NSArray
                    for friendId in friendsIds {
                        let friend = friendId as! [String : AnyObject]
                        let idF = friend["id"] as! String
                        let nameF = friend["name"] as! String
                        let pictureF = self.returnUserProfileImage(idF as NSString) //friend["picture"] as! String
                        let friendInfo = ["id" : idF, "name" : nameF, "picture" : pictureF]
                        friendsInfo.append(friendInfo)
                    }
                    print(friendsInfo)
                    if let user = FIRAuth.auth()?.currentUser {
                        ref.reference(withPath: "users").child(user.uid).child("friends").updateChildValues(["": friendsInfo])
                    }
//                    KCSUser.active().setValue(friendsInfo, forAttribute: "friends")
//                    KCSUser.active().save(completionBlock: { (user, error) in
//                        if error != nil {
//                            print(error.debugDescription)
//                        }
//                    })
                }
            }
            
            //let result = result as! NSDictionary
            
        }
    }
    
    func returnUserProfileImage(_ accessToken: NSString) -> String
    {
        let userID = accessToken as NSString
        var facebookProfileUrl = URL(string: "http://graph.facebook.com/\(userID)/picture?type=large")
        let facebookUrlString = "http://graph.facebook.com/\(userID)/picture?type=large"
        return facebookUrlString
        
    }

}

