//
//  TTTabBarItem.swift
//  goOUTApp
//
//  Created by Sandor ferreira da silva on 01/03/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

open class TTTabBarItem: UIButton {
    
    var offsetY: CGFloat = 0 //Offset from Top Y
    var offsetBottom: CGFloat = 0 //Offset from Bottom, like margin
    
    var image: UIImage?
    var selectedImage: UIImage?
    var viewController: UIViewController?
    
    //If is a button, set to yes
    var isButton = false
    
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    open override func draw(_ rect: CGRect) {
        // Drawing code
        super.draw(rect)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
    }
    
    //Init with default size
    init(image: UIImage?, selected: UIImage?) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 44))
        self.setImage(image, for: UIControlState())
        self.image = image
        self.selectedImage = selected
    }
    
    init(viewController: UIViewController) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 44))
        self.viewController = viewController
    }
    
}
