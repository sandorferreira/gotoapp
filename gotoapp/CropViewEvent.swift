//
//  CropViewEvent.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 24/09/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit
import ImageCropView

class CropViewEvent: UIViewController {

    @IBOutlet weak var imageCropView: ImageCropView!
    var imageToCrop: UIImage?
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    @IBAction func endCroppingImage(_ sender: AnyObject) {
        let croppedImage = imageCropView.croppedImage()
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationDismissImagePicker"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationCroppedImage"), object: nil, userInfo: ["image" : croppedImage])
        }
    }
    
    @IBAction func pickOtherImage(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarColorToTransparent()
        
        imageCropView.setup(imageToCrop!, tapDelegate: self)
        imageCropView.clipsToBounds = true
        imageCropView.coverImageView.clipsToBounds = true
        imageCropView.coverImageView.autoresizingMask = [.flexibleWidth,.flexibleHeight, .flexibleTopMargin, .flexibleLeftMargin /*, .flexibleRightMargin, .flexibleBottomMargin*/]
        //imageCropView.autoresizingMask = [.flexibleWidth,.flexibleHeight, .flexibleTopMargin, .flexibleLeftMargin, .flexibleRightMargin, .flexibleBottomMargin]
        imageCropView.contentMode = .scaleAspectFill
        imageCropView.coverImageView.contentMode = .scaleAspectFill
        //imageCropView.coverImageView.clipsToBounds = true
        
        //imageCropView.zoom(to: imageCropView.frame, animated: false)
        //imageCropView.setCrop(imageCropView.frame)
        imageCropView.display()
        //imageCropView.editable = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupNavigationBarColorToTransparent() {
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.backgroundColor = UIColor.clear
        
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.titleTextAttributes = ([NSFontAttributeName: UIFont(name: "Helvetica Neue", size: 17)!,
                                                             NSForegroundColorAttributeName: UIColor.white])
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CropViewEvent: ImageCropViewTapProtocol {
    
    func onImageCropViewTapped(_ imageCropView: ImageCropView) {
        print("ta")
    }

}
