//
//  KCSUserCheckInCell.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 14/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

class KCSUserCheckInCell: UITableViewCell {

    @IBOutlet weak var profilePicture: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var profile_availableIV: UIImageView!
}
