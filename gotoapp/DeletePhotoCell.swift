//
//  DeletePhotoCell.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 24/07/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

class DeletePhotoCell: UITableViewCell {
    
    @IBOutlet weak var photoTaken: UIImageView!
    
    @IBOutlet weak var deleteButton: UIButton!
    
//    @IBOutlet weak var photoTaken: UIImageView!
//    
//    @IBOutlet weak var deleteButton: UIButton!
}
