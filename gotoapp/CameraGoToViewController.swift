//
//  CameraGoToViewController.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 18/10/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit
import AVFoundation

class CameraGoToViewController: UIViewController {
    
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var flashBtn: UIButton!
    @IBOutlet weak var usarBtn: UIButton!
    var captureSession = AVCaptureSession()
    let stillImageOutput = AVCaptureStillImageOutput()
    var previewLayer = AVCaptureVideoPreviewLayer()
    
    @IBOutlet weak var previewImageview: UIImageView!
    var captureDevice : AVCaptureDevice?
    var tookPhoto = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        captureSession.sessionPreset = AVCaptureSessionPresetHigh
        //previewImageview.isHidden = true
        //previewLayer.frame = cameraView.bounds
        
//        tookPhoto = false
//        usarBtn.isHidden = true
//        previewImageview.isHidden = true
//        self.navigationController?.isNavigationBarHidden = true
//        
//        captureSession.sessionPreset = AVCaptureSessionPresetHigh
//        
//        if let devices = AVCaptureDevice.devices() as? [AVCaptureDevice] {
//            // Loop through all the capture devices on this phone
//            for device in devices {
//                // Make sure this particular device supports video
//                if (device.hasMediaType(AVMediaTypeVideo)) {
//                    // Finally check the position and confirm we've got the back camera
//                    if(device.position == AVCaptureDevicePosition.front) {
//                        captureDevice = device
//                        //captureDevice?.videoZoomFactor = 1.0
//                        if captureDevice != nil {
//                            print("Capture device found")
//                            beginSession()
//                        }
//                    }
//                }
//            }
//        }

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        tookPhoto = false
//        usarBtn.isHidden = true
//        previewImageview.isHidden = true
//        self.navigationController?.isNavigationBarHidden = true
        
        if let devices = AVCaptureDevice.devices() as? [AVCaptureDevice] {
            // Loop through all the capture devices on this phone
            for device in devices {
                // Make sure this particular device supports video
                if (device.hasMediaType(AVMediaTypeVideo)) {
                    // Finally check the position and confirm we've got the back camera
                    if(device.position == AVCaptureDevicePosition.front) {
                        captureDevice = device
                        //captureDevice?.videoZoomFactor = 1.0
                        if captureDevice != nil {
                            print("Capture device found")
                            beginSession()
                        }
                    } else {
                        print("taaaa")
                    }
                }
            }
        }
    }
    
    func beginSession() {
        
        do {
            try captureSession.addInput(AVCaptureDeviceInput(device: captureDevice))
            stillImageOutput.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]
            
            if captureSession.canAddOutput(stillImageOutput) {
                captureSession.addOutput(stillImageOutput)
            }
            
        }
        catch {
            print("error: \(error.localizedDescription)")
        }
        
        DispatchQueue.main.async(execute: { () -> Void in
            guard let previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession) else {
                print("no preview layer")
                return
            }
            previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            previewLayer.frame = self.cameraView.layer.bounds
            previewLayer.masksToBounds = true
            //previewLayer.frame = self.cameraView.frame
            self.cameraView.clipsToBounds = true
            self.cameraView.translatesAutoresizingMaskIntoConstraints = true
            self.cameraView.layer.addSublayer(previewLayer)
            
            self.captureSession.startRunning()
        })
    }
    
    func saveToCamera() {
        
        if let videoConnection = stillImageOutput.connection(withMediaType: AVMediaTypeVideo) {
            
            stillImageOutput.captureStillImageAsynchronously(from: videoConnection, completionHandler: { (CMSampleBuffer, Error) in
                if let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(CMSampleBuffer) {
                    
                    if let cameraImage = UIImage(data: imageData) {
                        
                        UIImageWriteToSavedPhotosAlbum(cameraImage, nil, nil, nil)
                    }
                }
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func closeCamera(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func takePhoto(_ sender: AnyObject) {
        if tookPhoto == false {
            tookPhoto = true
            if let videoConnection = stillImageOutput.connection(withMediaType: AVMediaTypeVideo) {
                stillImageOutput.captureStillImageAsynchronously(from: videoConnection, completionHandler: { (buffer, error) in
                    if error != nil {
                        print("ih carai")
                    }
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)
                    let squareImage = self.cropToBounds(image: UIImage(data: imageData!)!, width: Double(self.cameraView.frame.width), height: Double(self.cameraView.frame.height))
                    self.previewImageview.isHidden = false
                    self.usarBtn.isHidden = false
                    self.previewImageview.contentMode = .scaleAspectFill
                    self.previewImageview.image = squareImage
                    //UIImageWriteToSavedPhotosAlbum(squareImage, nil, nil, nil)
                })
            }
        } else {
            tookPhoto = false
            self.previewImageview.isHidden = true
            self.usarBtn.isHidden = true
        }

    }
    
    @IBAction func usarImagem(_ sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PictureTakenPreview") as! SharePhotoViewController
        controller.pictureTaken = self.previewImageview.image
        self.present(controller, animated: false, completion: nil)
        //self.show(controller, sender: self)
        //self.navigationController?.show(controller, sender: self)
        //self.navigationController //pushViewController(controller, animated: true)
    }
    
    
    
    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
        
        let contextImage: UIImage = UIImage(cgImage: image.cgImage!)
        
        let contextSize: CGSize = contextImage.size
        
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRect(origin: CGPoint(x: posX,y: posY), size: CGSize(width: cgwidth, height: cgheight))//CGRectMake(posX, posY, cgwidth, cgheight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImage = contextImage.cgImage!.cropping(to: rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }

}
