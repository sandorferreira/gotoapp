//
//  PickStartTimeWindowController.swift
//  gotoapp
//
//  Created by Sandor ferreira da silva on 28/06/16.
//  Copyright © 2016 AppsCat. All rights reserved.
//

import UIKit

class PickStartTimeWindowController: UIViewController {
    
    // -- Outlets from Storyboard -- //
    
    @IBOutlet weak var nowViewBackground: UIView!
    
    @IBOutlet weak var thirtyViewBackground: UIView!
    
    @IBOutlet weak var hourViewBackground: UIView!
    
    @IBOutlet weak var okButton: UIButton!
    
    var option = 1
    let backgroundColor = UIColor(red: 148.0/255.0, green: 64.0/255.0, blue: 185.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap_agora = UITapGestureRecognizer(target: self, action: #selector(PickStartTimeWindowController.setStartTimeToNow(_:)))
        let tap_thirty = UITapGestureRecognizer(target: self, action: #selector(PickStartTimeWindowController.setStartTimeToThirty(_:)))
        let tap_hour = UITapGestureRecognizer(target: self, action: #selector(PickStartTimeWindowController.setStartTimeToHour(_:)))
        let tap_ok = UITapGestureRecognizer(target: self, action: #selector(PickStartTimeWindowController.setTime(_:)))
        self.okButton.addGestureRecognizer(tap_ok)
        
        self.nowViewBackground.addGestureRecognizer(tap_agora)
        self.thirtyViewBackground.addGestureRecognizer(tap_thirty)
        self.hourViewBackground.addGestureRecognizer(tap_hour)
    
    }
    
    func setStartTimeToNow(_ r: UITapGestureRecognizer) {
        self.option = 1
        self.nowViewBackground.backgroundColor = backgroundColor
        self.thirtyViewBackground.backgroundColor = UIColor.white
        self.hourViewBackground.backgroundColor = UIColor.white
    }
    
    func setStartTimeToThirty(_ r: UITapGestureRecognizer) {
        self.option = 2
        self.thirtyViewBackground.backgroundColor = backgroundColor
        self.nowViewBackground.backgroundColor = UIColor.white
        self.hourViewBackground.backgroundColor = UIColor.white
    }
    
    func setStartTimeToHour(_ r: UITapGestureRecognizer) {
        self.option = 3
        self.hourViewBackground.backgroundColor = backgroundColor
        self.thirtyViewBackground.backgroundColor = UIColor.white
        self.nowViewBackground.backgroundColor = UIColor.white
    }
    
    func setTime(_ r: UITapGestureRecognizer) {
    NotificationCenter.default.post(name: Notification.Name(rawValue: "EventStartTime"), object: nil, userInfo: ["startDelay" : self.option])
        self.dismiss(animated: true, completion: nil)
    }
    
    func dismissAll() {
        self.dismiss(animated: true, completion: nil)
    }
}
